<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class doi_tuong extends Model
{ 
    protected $table='doi_tuong';
    protected $fillable=['id', 'hinh_anh', 'ten_doi_tuong'];
}
