<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class like_products extends Model
{
    protected $table='like_products';
    protected $fillable=['ma_san_pham', 'ma_khach_hang'];
}
