<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class danh_gia extends Model
{
    protected $table='danh_gia';
    protected $fillable=['id', 'ma_khach_hang', 'noi_dung', 'created_at','updated_at'];
}
