<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class giam_gia extends Model
{
    protected $table='giam_gia';
    protected $fillable=['ma_san_pham', 'gia_giam', 'ngay_bat_dau', 'ngay_ket_thuc'];
}
