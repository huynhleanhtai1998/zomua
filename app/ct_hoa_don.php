<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ct_hoa_don extends Model
{
    protected $table='ct_hoa_don';
    protected $fillable=['size','so_hoa_don', 'ma_san_pham', 'so_luong', 'don_gia', 'stt'];
}
