<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class khach_hang extends Model
{
    protected $table='khach_hang';
    protected $fillable=['ma_loai_nguoi_dung','ma_khach_hang', 'ten_khach_hang', 'phai', 'ngay_sinh', 'dia_chi', 'dien_thoai', 'email', 'mat_khau', 'ngay_tao'];
}
