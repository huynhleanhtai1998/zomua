<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $id;

    public function __construct($id)
    {
        $this->subject('Test gửi mail');
        $this->id=$id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->From('taideptrainhatapbac@gmail.com','ATShop')->view('mail/hoa_don',['ma_hoa_don'=>$this->id]);
    }
}
