<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\doi_tuong;
use App\loai_san_pham;
use Illuminate\Support\Facades\DB;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        
        if ($this->isHttpException($exception)) {
            if ($exception->getStatusCode() == 404) {
                $dsDoiTuong=doi_tuong::get();
                    foreach($dsDoiTuong as $dt)
                    {
                        $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
                    }
                    return response()->view('error',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
            }
        }

        if(!env('APP_DEBUG', false)){
            $dsDoiTuong=doi_tuong::get();
                    foreach($dsDoiTuong as $dt)
                    {
                        $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
                    }
                    return response()->view('error',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
        } else {
            return parent::render($request, $exception);
        }
        return parent::render($request, $exception);
    }
}
