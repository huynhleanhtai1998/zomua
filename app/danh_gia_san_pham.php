<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class danh_gia_san_pham extends Model
{
    //'ma_san_pham', 'ma_khach_hang', 'noi_dung', 'created_at', 'updated_at', 'tac_gia', 'diem'
    protected $table='danh_gia_san_pham';
    protected $fillable=['ma_san_pham', 'ma_khach_hang', 'noi_dung', 'created_at', 'updated_at', 'tac_gia', 'diem'];
}
