<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    
    protected $table='users';
    protected $fillable=['gioi_tinh','id', 'ho_ten', 'email', 'password', 'created_at', 'updated_at', 'quan_ly_san_pham', 'quan_ly_tin_tuc', 'quan_ly_hoa_don', 'quan_ly_khach_hang', 'quan_ly_user'];
}
