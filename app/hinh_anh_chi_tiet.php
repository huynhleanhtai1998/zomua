<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hinh_anh_chi_tiet extends Model
{
    protected $table='hinh_anh_chi_tiet';
    protected $fillable=['ma_san_pham', 'hinh_anh'];
}
