<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slider extends Model
{
    protected $table='slide';
    protected $fillable=['id', 'tieu_de', 'noi_dung', 'hinh_anh'];
}
