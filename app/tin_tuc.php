<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tin_tuc extends Model
{
    protected $table='tin_tuc';
    protected $fillable=['id', 'tieu_de', 'noi_dung', 'hinh_anh', 'created_at', 'updated_At', 'luot_xem', 'luot_thich', 'binh_luan','tom_tat'];
}
