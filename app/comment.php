<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $table='comment';
    protected $fillable=['ma_tin_tuc', 'ma_khach_hang', 'noi_dung', 'ngay_dang', 'ma_comment'];
}
