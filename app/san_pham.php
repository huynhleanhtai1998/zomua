<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class san_pham extends Model
{
    protected $table='san_pham';
    protected $fillable=['gia_sau_khi_giam','ma_san_pham', 'ten_san_pham', 'ma_loai', 'don_gia', 'hinh', 'luot_mua', 'luot_thich', 'doi_tuong', 'updated_at', 'created_at', 'so_luong', 'mo_ta'];
}
