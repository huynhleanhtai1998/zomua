<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class loai_tin_tuc extends Model
{
    protected $table='loai_tin_tuc';
    protected $fillable=['ma_loai', 'ten_loai'];
}
