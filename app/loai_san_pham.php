<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class loai_san_pham extends Model
{
    protected $table='loai_san_pham';
    protected $fillable=['ma_loai', 'ten_loai', 'mo_ta', 'doi_tuong'];
}
