<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bai_viet extends Model
{
    protected $table='bai_viet';
    protected $fillable=['ma_bai_viet', 'ma_loai_bai_viet', 'ma_nguoi_dung', 'tieu_de', 'noi_dung_tom_tat', 'noi_dung_chi_tiet', 'ngay_gui_bai', 'ngay_xuat_ban', 'ngay_het_han', 'so_lan_xem', 'xuat_ban'];
}
