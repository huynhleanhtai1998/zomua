<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class StoreSanPham extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'frm_ten_san_pham'=>'required',
            'frm_mo_ta'=>'required',
            'frm_don_gia'=>'required',
            'frm_so_luong'=>'required',
            'frm_hinh_anh_chinh'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'frm_hinh_anh_2'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'frm_hinh_anh_3'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'frm_hinh_anh_4'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'frm_hinh_anh_5'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'frm_hinh_anh_6'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'frm_ten_san_pham.required'=>'Chưa nhập tên sản phẩm',
            'frm_mo_ta.required'=>'Chưa nhập mô tả',
            'frm_don_gia.required'=>'Chưa nhập đơn giá',
            'frm_so_luong.required'=>'Chưa nhập số lượng',

            'frm_hinh_anh_chinh.required'=>'Chưa chọn hình ảnh chính cho đối tượng',
            'frm_hinh_anh_chinh.image'=>'Chỉ được chọn file hình ảnh để tải lên',
            'frm_hinh_anh_chinh.mimes'=>'Chỉ được chọn file có đuôi là jpeg,png,jpg,gif,svg',
            'frm_hinh_anh_chinh.max'=>'Chỉ được chọn file < 2MB',

            'frm_hinh_anh_2.image'=>'Chỉ được chọn file hình ảnh để tải lên',
            'frm_hinh_anh_2.mimes'=>'Chỉ được chọn file có đuôi là jpeg,png,jpg,gif,svg',
            'frm_hinh_anh_2.max'=>'Chỉ được chọn file < 2MB',

            'frm_hinh_anh_3.image'=>'Chỉ được chọn file hình ảnh để tải lên',
            'frm_hinh_anh_3.mimes'=>'Chỉ được chọn file có đuôi là jpeg,png,jpg,gif,svg',
            'frm_hinh_anh_3.max'=>'Chỉ được chọn file < 2MB',

            'frm_hinh_anh_4.image'=>'Chỉ được chọn file hình ảnh để tải lên',
            'frm_hinh_anh_4.mimes'=>'Chỉ được chọn file có đuôi là jpeg,png,jpg,gif,svg',
            'frm_hinh_anh_4.max'=>'Chỉ được chọn file < 2MB',

            'frm_hinh_anh_5.image'=>'Chỉ được chọn file hình ảnh để tải lên',
            'frm_hinh_anh_5.mimes'=>'Chỉ được chọn file có đuôi là jpeg,png,jpg,gif,svg',
            'frm_hinh_anh_5.max'=>'Chỉ được chọn file < 2MB',

            'frm_hinh_anh_6.image'=>'Chỉ được chọn file hình ảnh để tải lên',
            'frm_hinh_anh_6.mimes'=>'Chỉ được chọn file có đuôi là jpeg,png,jpg,gif,svg',
            'frm_hinh_anh_6.max'=>'Chỉ được chọn file < 2MB',
        ];
    }
}
