<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLoaiSanPham extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'frm_ten_loai'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'frm_ten_loai.required'=>'Chưa nhập tên loại sản phẩm',
        ];
    }
}
