<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDoiTuong extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'frm_ten_doi_tuong'=>'required',
            'frm_hinh_anh'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'frm_ten_doi_tuong.required'=>'Chưa nhập tên đối tượng',

            'frm_hinh_anh.required'=>'Chưa chọn hình ảnh cho đối tượng',
            'frm_hinh_anh.image'=>'Chỉ được chọn file hình ảnh để tải lên',
            'frm_hinh_anh.mimes'=>'Chỉ được chọn file có đuôi là jpeg,png,jpg,gif,svg',
            'frm_hinh_anh.max'=>'Chỉ được chọn file < 2MB',
        ];
    }
}
