<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTinTuc extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'frm_tieu_de'=>'required',
            'frm_tom_tat'=>'required',
            'frm_noi_dung'=>'required',
            'frm_tac_gia'=>'required',
            'frm_hinh_anh'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'frm_tieu_de.required'=>'Chưa nhập tiêu đề cho tin tức' ,
            'frm_tom_tat.required'=>'Chưa nhập tóm tắt cho tin tức' ,
            'frm_noi_dung.required'=>'Chưa nhập nội dung cho tin tức' ,
            'frm_tac_gia.required'=>'Chưa nhập tac giả cho tin tức' ,

            'frm_hinh_anh.required'=>'Chưa chọn hình ảnh cho tin tức',
            'frm_hinh_anh.image'=>'Chỉ được chọn file hình ảnh để tải lên',
            'frm_hinh_anh.mimes'=>'Chỉ được chọn file có đuôi là jpeg,png,jpg,gif,svg',
            'frm_hinh_anh.max'=>'Chỉ được chọn file < 2MB',
        ];
    }
}
