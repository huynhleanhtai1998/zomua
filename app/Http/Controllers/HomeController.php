<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function ChangeLanguage($language)
    {
        \Session::put('locale',$language);
        return redirect()->back();
    }
}
