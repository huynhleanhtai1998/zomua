<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\DB;
use App\san_pham;
use App\bai_viet;
use App\slider;
use App\doi_tuong;
use App\tin_tuc;
use App\danh_gia;
use App\khac_hang;
use App\loai_san_pham;
use App\giam_gia;
use App\hinh_anh_chi_tiet;
use App\comment;
use App\loai_tin_tuc;
use App\like_news;

use DateTime;
use App\Http\Requests\StoreTinTuc;

class TinTucController extends Controller
{
    public function index()
    {
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        $dsTinTuc=tin_tuc::orderBy('created_at','desc')->paginate(6);
        $dsLoaiTinTuc=loai_tin_tuc::get();
        $dsHot=tin_tuc::orderBy('luot_xem','desc')->limit(5)->get();
        if(session()->has('recently_viewed_news'))
        {
            $xem_gan_day=session()->get('recently_viewed_news');
            foreach ($xem_gan_day as $item) {
                $dsXemGanDay[]=tin_tuc::where('id',$item)->first();
            }
            return view('tin_tuc/index',['dsXemGanDay'=>$dsXemGanDay,'dsHot'=>$dsHot,'dsLoaiTinTuc'=>$dsLoaiTinTuc,'dsTinTuc'=>$dsTinTuc,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
        }
        else
        {
            return view('tin_tuc/index',['dsHot'=>$dsHot,'dsLoaiTinTuc'=>$dsLoaiTinTuc,'dsTinTuc'=>$dsTinTuc,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
        }
    }


    public function loai_tin_tuc($id)
    {
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        $dsTinTuc=tin_tuc::where('ma_loai',$id)->orderBy('created_at','desc')->paginate(6);
        $dsLoaiTinTuc=loai_tin_tuc::get();
        $dsHot=tin_tuc::orderBy('luot_xem','desc')->limit(5)->get();
        if(session()->has('recently_viewed_news'))
        {
            $xem_gan_day=session()->get('recently_viewed_news');
            foreach ($xem_gan_day as $item) {
                $dsXemGanDay[]=tin_tuc::where('id',$item)->first();
            }
            return view('tin_tuc/index',['dsXemGanDay'=>$dsXemGanDay,'dsHot'=>$dsHot,'dsLoaiTinTuc'=>$dsLoaiTinTuc,'dsTinTuc'=>$dsTinTuc,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
        }
        else
        {
            return view('tin_tuc/index',['dsHot'=>$dsHot,'dsLoaiTinTuc'=>$dsLoaiTinTuc,'dsTinTuc'=>$dsTinTuc,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
        }
    }

    

    public function show($id)
    {
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        $dsTinTuc=tin_tuc::orderBy('created_at','desc')->paginate(6);
        $dsLoaiTinTuc=loai_tin_tuc::get();
        $dsHot=tin_tuc::orderBy('luot_xem','desc')->limit(5)->get();
        $tin_tuc=tin_tuc::where('id',$id)->first();
        $loai_tin_tuc=loai_tin_tuc::where('ma_loai',$tin_tuc->ma_loai)->first();
        $dsComment=comment::where('ma_tin_tuc',$id)->paginate(6);

        if(session()->has('recently_viewed_news'))
        {
            $xem_gan_day=session()->get('recently_viewed_news');
            $check=true;
            foreach ($xem_gan_day as $value) {
                if($value==$id)
                {
                    $check=false;
                }
            }
            if($check==true)
            {
                if(count($xem_gan_day)>=6)
                {
                    array_splice($xem_gan_day, 0, count($xem_gan_day)-5);
                }
                $xem_gan_day[]=$id;
            }
        }
        else{
            $xem_gan_day[]=$id;
        }
        session()->put('recently_viewed_news', $xem_gan_day);

        if(session()->has('recently_viewed_news'))
        {
            $xem_gan_day=session()->get('recently_viewed_news');
            foreach ($xem_gan_day as $item) {
                $dsXemGanDay[]=tin_tuc::where('id',$item)->first();
            }

            $like_status=false;
            if(session()->has('khach_hang'))
            {
                $tt=tin_tuc::find($id);
                $tt->luot_xem=$tt->luot_xem+1;
                $tt->save();
                
                $ma_khach_hang=session()->get('khach_hang')->ma_khach_hang;
                $like=like_news::where('ma_khach_hang',$ma_khach_hang)->where('ma_tin_tuc',$id)->first();
                if($like)
                {
                    $like_status=true;
                }
            }
            return view('tin_tuc/chi_tiet',['like_status'=>$like_status,'dsXemGanDay'=>$dsXemGanDay,'dsTinTuc'=>$dsTinTuc,'dsHot'=>$dsHot,'dsLoaiTinTuc'=>$dsLoaiTinTuc,'dsComment'=>$dsComment,'tin_tuc'=>$tin_tuc,'loai_tin_tuc'=>$loai_tin_tuc,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
        }
        else
        {
            $like_status=false;
            if(session()->has('khach_hang'))
            {
                $ma_khach_hang=session()->get('khach_hang')->ma_khach_hang;
                $like=like_news::where('ma_khach_hang',$ma_khach_hang)->where('ma_tin_tuc',$id)->first();
                if($like)
                {
                    $like_status=true;
                }
            }
            return view('tin_tuc/chi_tiet',['like_status'=>$like_status,'dsTinTuc'=>$dsTinTuc,'dsHot'=>$dsHot,'dsLoaiTinTuc'=>$dsLoaiTinTuc,'dsComment'=>$dsComment,'tin_tuc'=>$tin_tuc,'loai_tin_tuc'=>$loai_tin_tuc,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
        }
        

    }

    public function comment(Request $request)
    {
        $noi_dung=$request->comment;
        $ma_khach_hang=$request->session()->get('khach_hang')->ma_khach_hang;
        $tac_gia=$request->session()->get('khach_hang')->ten_khach_hang;
        $ma_tin_tuc=$request->ma_tin_tuc;

        $comment=new comment;
        $comment->ma_tin_tuc=$ma_tin_tuc;
        $comment->ma_khach_hang=$ma_khach_hang;
        $comment->noi_dung=$noi_dung;
        $comment->tac_gia=$tac_gia;
        $comment->save();

        $tin_tuc=tin_tuc::find($ma_tin_tuc);
        $tin_tuc->binh_luan=$tin_tuc->binh_luan+1;
        $tin_tuc->save();

         return redirect('tin_tuc/chi_tiet/'.$ma_tin_tuc); 
    }
    

    public function like(Request $request)
    {
        $ma_tin_tuc=$request->ma_tin_tuc;
        $ma_khach_hang=$request->ma_khach_hang;
        $like_status=$request->like_status;
        if($like_status=='like')
        {
            DB::table('like_news')->where('ma_tin_tuc',$ma_tin_tuc)->where('ma_khach_hang',$ma_khach_hang)->delete();
            $tin_tuc=tin_tuc::find($ma_tin_tuc);
            $tin_tuc->luot_thich=$tin_tuc->luot_thich-1;
            $tin_tuc->save();
            echo json_encode((array('n'=>0)));
        }
        else
        {
            $like=new like_news;
            $like->ma_khach_hang=$ma_khach_hang;
            $like->ma_tin_tuc=$ma_tin_tuc;
            $like->save();

            $tin_tuc=tin_tuc::find($ma_tin_tuc);
            $tin_tuc->luot_thich=$tin_tuc->luot_thich+1;
            $tin_tuc->save();

            echo json_encode((array('n'=>1)));
        }
        
    }

    
    public function tin_tuc_yeu_thich()
    {
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        
        
        if(session()->has('khach_hang'))
        {
            $dsLoaiTinTuc=loai_tin_tuc::get();
            $dsHot=tin_tuc::orderBy('luot_xem','desc')->limit(5)->get();
            $dsYeuThich=like_news::where('ma_khach_hang',session()->get('khach_hang')->ma_khach_hang)->get();
            $dsTinTuc=array();
            if(count($dsYeuThich)>0)
            {
                foreach ($dsYeuThich as $like) 
                {
                    $dsTinTuc[]=tin_tuc::where('id',$like->ma_tin_tuc)->first();
                }
            }
            if(session()->has('recently_viewed_news'))
            {
                $xem_gan_day=session()->get('recently_viewed_news');
                foreach ($xem_gan_day as $item) 
                {
                    $dsXemGanDay[]=tin_tuc::where('id',$item)->first();
                }
                return view('tin_tuc/tin_tuc_yeu_thich',['dsXemGanDay'=>$dsXemGanDay,'dsHot'=>$dsHot,'dsLoaiTinTuc'=>$dsLoaiTinTuc,'dsTinTuc'=>$dsTinTuc,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
            }
            else
            {
                return view('tin_tuc/tin_tuc_yeu_thich',['dsHot'=>$dsHot,'dsLoaiTinTuc'=>$dsLoaiTinTuc,'dsTinTuc'=>$dsTinTuc,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
            }
        }
        else 
        {
            return view('error');
        }
        
    }

    public function tim_kiem_tac_gia($id)
    {
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        
        
        if(session()->has('khach_hang'))
        {
            $dsLoaiTinTuc=loai_tin_tuc::get();
            $dsHot=tin_tuc::orderBy('luot_xem','desc')->limit(5)->get();
            $dsYeuThich=like_news::where('ma_khach_hang',session()->get('khach_hang')->ma_khach_hang)->get();
            $tin_tuc=tin_tuc::where('id',$id)->first();
            $tac_gia=$tin_tuc->tac_gia;
            $dsTinTuc=tin_tuc::where('tac_gia',$tac_gia)->get();
            if(session()->has('recently_viewed_news'))
            {
                $xem_gan_day=session()->get('recently_viewed_news');
                foreach ($xem_gan_day as $item) 
                {
                    $dsXemGanDay[]=tin_tuc::where('id',$item)->first();
                }
                return view('tin_tuc/tin_tuc_yeu_thich',['dsXemGanDay'=>$dsXemGanDay,'dsHot'=>$dsHot,'dsLoaiTinTuc'=>$dsLoaiTinTuc,'dsTinTuc'=>$dsTinTuc,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
            }
            else
            {
                return view('tin_tuc/tin_tuc_yeu_thich',['dsHot'=>$dsHot,'dsLoaiTinTuc'=>$dsLoaiTinTuc,'dsTinTuc'=>$dsTinTuc,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
            }
        }
        else 
        {
            return view('error',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
        }
    }

    // -------------Quản lý tin tức

    public function liet_ke(Request $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        $dsTinTuc=tin_tuc::get();
        foreach($dsTinTuc as $tt)
        {
            $dsLTT[$tt->id]=loai_tin_tuc::where('ma_loai',$tt->ma_loai)->first();
        }
        return view('quan_tri/liet_ke_tin_tuc',['dsTinTuc'=>$dsTinTuc,'dsLTT'=>$dsLTT]);
    }


    public function create(Request $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        $dsLoaiTinTuc=loai_tin_tuc::get();
        return view('quan_tri/them_tin_tuc',['dsLoaiTinTuc'=>$dsLoaiTinTuc]);
    }

    public function store(StoreTinTuc $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        $validated=$request->validated;
        $tin_tuc=new tin_tuc;
        $tin_tuc->tieu_de=$request->frm_tieu_de;
        $tin_tuc->tom_tat=$request->frm_tom_tat;
        $tin_tuc->noi_dung=$request->frm_noi_dung;
        $tin_tuc->tac_gia=$request->frm_tac_gia;
        $tin_tuc->ma_loai=$request->frm_ma_loai;
        $tin_tuc->luot_xem=0;
        $tin_tuc->luot_thich=0;
        $tin_tuc->binh_luan=0;
        $n=0;

        $dia_chi_luu_anh='public/source/img/tin_tuc/';
        if($request->hasFile('frm_hinh_anh')){
            $file = $request->frm_hinh_anh;
            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $tin_tuc->hinh_anh=$file->getClientOriginalName();
            $n=$tin_tuc->save();
        }
        if($n==1)
            return redirect('tin_tuc/them')->with('alert','Thêm thành công');
        else {
            return redirect('tin_tuc/them')->with('alert','ERROR');
        }
    }


    public function sua(Request $request,$id)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        $dsLoaiTinTuc=loai_tin_tuc::get();
        $tin_tuc=tin_tuc::where('id',$id)->first();
        return view('quan_tri/sua_tin_tuc',['tin_tuc'=>$tin_tuc,'dsLoaiTinTuc'=>$dsLoaiTinTuc]);
    }

    public function edit(StoreTinTuc $request,$id)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        $validated=$request->validated;
        $tin_tuc=tin_tuc::find($id);
        $tin_tuc->tieu_de=$request->frm_tieu_de;
        $tin_tuc->tom_tat=$request->frm_tom_tat;
        $tin_tuc->noi_dung=$request->frm_noi_dung;
        $tin_tuc->tac_gia=$request->frm_tac_gia;
        $tin_tuc->ma_loai=$request->frm_ma_loai;
        $n=0;

        $dia_chi_luu_anh='public/source/img/tin_tuc/';
        if($request->hasFile('frm_hinh_anh')){
            $file = $request->frm_hinh_anh;
            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $tin_tuc->hinh_anh=$file->getClientOriginalName();
            $n=$tin_tuc->save();
        }
        if($n==1)
            return redirect('tin_tuc/liet_ke');
        else {
            return redirect('tin_tuc/sua/'.$id)->with('alert','ERROR');
        }
    }


    public function xoa($id)
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        DB::table('tin_tuc')->where('id',$id)->delete();
        DB::table('like_news')->where('ma_tin_tuc',$id)->delete();
        return redirect('tin_tuc/liet_ke');
    }
}
