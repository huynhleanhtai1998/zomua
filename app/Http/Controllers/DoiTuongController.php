<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\DB;
use App\san_pham;
use App\bai_viet;
use App\slider;
use App\doi_tuong;
use App\loai_san_pham;
use App\tin_tuc;
use App\danh_gia;
use App\khac_hang;
use App\giam_gia;
use App\hinh_anh_chi_tiet;
use DateTime;
use App\Http\Requests\StoreDoiTuong;

class DoiTuongController extends Controller
{
    public function them(Request $request)
    {

        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham==0)
        {
            return view('error');
        }
        return view('quan_tri/them_doi_tuong');
    }

    public function store(StoreDoiTuong $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $validated=$request->validated;
        $doi_tuong=new doi_tuong;
        $doi_tuong->ten_doi_tuong=$request->frm_ten_doi_tuong;
        $n=0;
        $dia_chi_luu_anh='public/source/img/doi_tuong/';
        if($request->hasFile('frm_hinh_anh')){
            $file = $request->frm_hinh_anh;
            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $doi_tuong->hinh_anh=$file->getClientOriginalName();
            $n=$doi_tuong->save();
        }
        if($n==1)
            return redirect('doi_tuong/them')->with('alert','Thêm thành công');
        else {
            return redirect('doi_tuong/them')->with('alert','Thêm thất bại');
        }
    }
    public function liet_ke(Request $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        return view('quan_tri/liet_ke_doi_tuong',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
    }

    public function sua(Request $request,$id)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $doi_tuong=doi_tuong::where('id',$id)->first();
        return view('quan_tri/sua_doi_tuong',['doi_tuong'=>$doi_tuong]);
    }

    public function edit(StoreDoiTuong $request,$id)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $validated=$request->validated;

        $doi_tuong=doi_tuong::find($id);
        $doi_tuong->ten_doi_tuong=$request->frm_ten_doi_tuong;
        $n=0;
        $dia_chi_luu_anh='public/source/img/doi_tuong/';
        if($request->hasFile('frm_hinh_anh')){
            $file = $request->frm_hinh_anh;
            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $doi_tuong->hinh_anh=$file->getClientOriginalName();
            $n=$doi_tuong->save();
        }
        if($n==1)
            return redirect('doi_tuong/liet_ke')->with('alert','Sửa thành công');
        else {
            return redirect('doi_tuong/sua/'.$id)->with('alert','Sửa thất bại');
        }
    }

    public function xoa($id)
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        DB::table('doi_tuong')->where('id',$id)->delete();
        return redirect('doi_tuong/liet_ke');
    }
}
