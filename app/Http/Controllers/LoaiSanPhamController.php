<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\DB;
use App\san_pham;
use App\bai_viet;
use App\slider;
use App\doi_tuong;
use App\tin_tuc;
use App\danh_gia;
use App\khac_hang;
use App\loai_san_pham;
use App\giam_gia;
use App\hinh_anh_chi_tiet;
use DateTime;
use App\Http\Requests\StoreLoaiSanPham;

class LoaiSanPHamController extends Controller
{
    public function them(Request $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        return view('quan_tri/them_loai_san_pham',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
    }

    public function store(StoreLoaiSanPham $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $validated=$request->validated;
        $loai_san_pham=new loai_san_pham;
        $loai_san_pham->ten_loai=$request->frm_ten_loai;
        $loai_san_pham->doi_tuong=$request->frm_doi_tuong;
        $n=$loai_san_pham->save();
        if($n==1)
            return redirect('loai_san_pham/them')->with('alert','Thêm thành công');
        else {
            return redirect('loai_san_pham/them')->with('alert','ERROR');
        }
    }

    public function liet_ke(Request $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        return view('quan_tri/liet_ke_loai_san_pham',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
    }

    public function sua(Request $request,$id)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        $loai_san_pham=loai_san_pham::where('ma_loai',$id)->first();
        return view('quan_tri/sua_loai_san_pham',['loai_san_pham'=>$loai_san_pham,'dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
    }

    public function edit(StoreLoaiSanPham $request,$id)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $validated=$request->validated;
        $n=DB::table('loai_san_pham')
            ->where('ma_loai', $id)
            ->update(['ten_loai' => $request->frm_ten_loai,'doi_tuong'=>$request->frm_doi_tuong]);
        if($n==1)
            return redirect('loai_san_pham/liet_ke');
        else {
            return redirect('loai_san_pham/sua/'.$id)->with('alert','ERROR');
        }
    }

    public function xoa($id)
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        DB::table('loai_san_pham')->where('ma_loai',$id)->delete();
        return redirect('loai_san_pham/liet_ke');
    }

}
