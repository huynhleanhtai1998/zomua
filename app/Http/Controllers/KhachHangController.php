<?php

namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Mail\SendEmail;
use App\san_pham;
use App\bai_viet;
use App\slider;
use App\doi_tuong;
use App\tin_tuc;
use App\danh_gia;
use App\khach_hang;
use App\loai_san_pham;


class KhachHangController extends Controller
{
    public function xu_ly_dang_nhap(Request $request)
    {
        $email=$request->frm_email;
        $password=$request->frm_password;
        $khach_hang=khach_hang::where('email',$email)->where('mat_khau',$password)->first();
        if($khach_hang)
        {
            $request->session()->put('khach_hang',$khach_hang);
            
            echo json_encode((array('n'=>1)));
            if($request->frm_remember==1)
            {
                $response=new Response();
                $response->withCookie('login_email',$email,4500)->withCookie('login_pass',$password,4500);
                return $response;
            }
        }
    }

    public function xu_ly_dang_ky(Request $request)
    {
        $email=$request->email;
        $khach_hang=khach_hang::where('email',$email)->first();
        if($khach_hang)
        {
            echo json_encode((array('n'=>0)));
        }
        else
        {
            
            $khach_hang=new khach_hang;
            $khach_hang->email=$email;
            $khach_hang->mat_khau=$request->mat_khau;
            $khach_hang->ten_khach_hang=$request->ho_ten;
            $khach_hang->dien_thoai=$request->dien_thoai;
            $khach_hang->dia_chi=$request->dia_chi;
            $khach_hang->ngay_sinh=$request->ngay_sinh;
            $khach_hang->phai=$request->gioi_tinh;
            $khach_hang->ma_loai_nguoi_dung=0;
            $n=$khach_hang->save();
            $request->session()->put('khach_hang',$khach_hang);
            echo json_encode((array('n'=>$n)));

        }
    }


    public function cap_nhat(Request $request)
    {  
        $email=$request->email;
        $ma_khach_hang= $request->session()->get('khach_hang')->ma_khach_hang;
        $n=DB::table('khach_hang')
        ->where('ma_khach_hang',$ma_khach_hang)
        ->update(['email' => $email,'mat_khau'=>$request->mat_khau,'ten_khach_hang'=>$request->ho_ten,'dien_thoai'=>$request->dien_thoai,'dia_chi'=>$request->dia_chi,'ngay_sinh'=>$request->ngay_sinh,'phai'=>$request->gioi_tinh]);
        $khach_hang=khach_hang::where('ma_khach_hang',$ma_khach_hang)->first();
        $request->session()->put('khach_hang',$khach_hang);
        echo json_encode((array('n'=>$n)));
    }

    public function dang_xuat(Request $request)
    {
        $request->session()->flush();
        return redirect('san_pham');
    }

    public function liet_ke()
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_khach_hang!=1)
        {
            return view('error');
        }
        $dsKhachHang=khach_hang::paginate(20);
        return view('quan_tri/quan_ly_khach_hang',['dsKhachHang'=>$dsKhachHang]);
    }


    public function danh_gia(Request $request)
    {
        $ma_khach_hang=$request->session()->get('khach_hang')->ma_khach_hang;
        $noi_dung=$request->noi_dung;
        $danh_gia=new danh_gia;
        $danh_gia->ma_khach_hang=$ma_khach_hang;
        $danh_gia->noi_dung=$noi_dung;
        $danh_gia->save();
        return redirect('/');
    }
}
