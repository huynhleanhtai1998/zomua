<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\DB;
use App\san_pham;
use App\bai_viet;
use App\slider;
use App\doi_tuong;
use App\tin_tuc;
use App\danh_gia;
use App\khac_hang;
use App\loai_san_pham;

class LienHeController extends Controller
{
    public function index()
    {
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        return view('lien_he/index',['dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
    }


    public function loai_san_pham()
    {
        return view('san_pham/loai_san_pham');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('san_pham/chi_tiet');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
