<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\DB;
use App\san_pham;
use App\bai_viet;
use App\slider;
use App\doi_tuong;
use App\tin_tuc;
use App\danh_gia;
use App\khac_hang;
use App\loai_san_pham;
use App\danh_gia_san_pham;
use App\giam_gia;
use App\hinh_anh_chi_tiet;
use App\like_products;
use DateTime;

use App\Http\Requests\StoreSanPham;

class SanPhamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dsSlider=slider::get();
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
            $dsSanPham[$dt->id]=san_pham::where('doi_tuong',$dt->id)->orderBy('created_at','dest')->limit(8)->get();
        }
        $dsHot=san_pham::orderBy('luot_mua','desc')->limit(8)->get();
        $dsNew=san_pham::orderBy('created_at','desc')->limit(8)->get();
        $dsSaleOff=san_pham::where('gia_sau_khi_giam',"!=",0)->orderBy('luot_thich','desc')->limit(8)->get();
        $dsTinTuc=tin_tuc::orderBy('created_at','desc')->limit(3)->get();
        $dsDanhGia=danh_gia::join('khach_hang', 'danh_gia.ma_khach_hang', '=', 'khach_hang.ma_khach_hang')->orderBy('danh_gia.created_at','desc')->limit(10)->get();
        //$dsSanPham=san_pham::paginate(6);
        return view('trang_chu/index',['dsNew'=>$dsNew,'dsSaleOff'=>$dsSaleOff,'dsSanPham'=>$dsSanPham,'dsSlider'=>$dsSlider,'dsDoiTuong'=>$dsDoiTuong,'dsHot'=>$dsHot,'dsTinTuc'=>$dsTinTuc,'dsDanhGia'=>$dsDanhGia,'dsLoaiSanPham'=>$dsLoaiSanPham]);
    }


    public function doi_tuong($id)
    {
        $dsDoiTuong=doi_tuong::get();
        $dsHot=san_pham::orderBy('luot_mua','desc')->limit(4)->get();
        $check=false;
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
            if($dt->id==$id)
            {
                $check=true;
            }
        }
        if($check==false)
        {
            return view('error',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
        }
        $ma_loai=$id;
        //$dsLoaiSanPham=loai_san_pham::where('doi_tuong',$id)->get();
        $dsSanPham=san_pham::where('doi_tuong',$id)->paginate(12);
        if(session()->has('recently_viewed_product'))
        {
            $xem_gan_day=session()->get('recently_viewed_product');
            foreach ($xem_gan_day as $item) {
                $dsXemGanDay[]=san_pham::where('ma_san_pham',$item)->first();
            }
            return view('san_pham/loai_san_pham',['dsXemGanDay'=>$dsXemGanDay,'dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham,'ma_loai'=>$ma_loai]);
        }
        else
        {
            return view('san_pham/loai_san_pham',['dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham,'ma_loai'=>$ma_loai]);
        }
        
    }

    public function doi_tuong_sort(Request $request,$id)
    {
        $dsDoiTuong=doi_tuong::get();
        $dsHot=san_pham::orderBy('luot_mua','desc')->limit(4)->get();
        $check=false;
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
            if($dt->id==$id)
            {
                $check=true;
            }
        }
        if($check==false)
        {
            return view('error',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
        }
        $ma_loai=$id;
        //$dsLoaiSanPham=loai_san_pham::where('doi_tuong',$id)->get();
        $dsSanPham=san_pham::where('doi_tuong',$id)->paginate(12);
        if(session()->has('recently_viewed_product'))
        {
            $xem_gan_day=session()->get('recently_viewed_product');
            foreach ($xem_gan_day as $item) {
                $dsXemGanDay[]=san_pham::where('ma_san_pham',$item)->first();
            }
            return view('san_pham/loai_san_pham',['dsXemGanDay'=>$dsXemGanDay,'dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham,'ma_loai'=>$ma_loai]);
        }
        else
        {
            return view('san_pham/loai_san_pham',['dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham,'ma_loai'=>$ma_loai]);
        }
        
    }

    public function loai_san_pham($id)
    {
        $dsDoiTuong=doi_tuong::get();
        $dsHot=san_pham::orderBy('luot_mua','desc')->limit(4)->get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        $loai_san_pham=loai_san_pham::where('ma_loai',$id)->first();
        $ma_loai=$loai_san_pham->doi_tuong;
        $dsSanPham=san_pham::where('ma_loai',$id)->paginate(12);
            if(session()->has('recently_viewed_product'))
            {
                $xem_gan_day=session()->get('recently_viewed_product');
                foreach ($xem_gan_day as $item) {
                    $dsXemGanDay[]=san_pham::where('ma_san_pham',$item)->first();
                }
                return view('san_pham/loai_san_pham',['dsXemGanDay'=>$dsXemGanDay,'loai_san_pham'=>$loai_san_pham,'dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham,'ma_loai'=>$ma_loai]);
            }
            else
            {
                return view('san_pham/loai_san_pham',['loai_san_pham'=>$loai_san_pham,'dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham,'ma_loai'=>$ma_loai]);
            }
    }


    
    

    public function loai_san_pham_sort(Request $request,$id)
    {
        $dsDoiTuong=doi_tuong::get();
        $dsHot=san_pham::orderBy('luot_mua','desc')->limit(4)->get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        $loai_san_pham=loai_san_pham::where('ma_loai',$id)->first();
        $ma_loai=$loai_san_pham->doi_tuong;
        //$dsLoaiSanPham=loai_san_pham::where('doi_tuong',$id)->get();
        if($request->frm_sort==1)
        {
            $dsSanPham=san_pham::where('ma_loai',$id)->orderBy('don_gia','asc')->paginate(12);
            if(session()->has('recently_viewed_product'))
            {
                $xem_gan_day=session()->get('recently_viewed_product');
                foreach ($xem_gan_day as $item) {
                    $dsXemGanDay[]=san_pham::where('ma_san_pham',$item)->first();
                }
                return view('san_pham/loai_san_pham',['dsXemGanDay'=>$dsXemGanDay,'loai_san_pham'=>$loai_san_pham,'dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham,'ma_loai'=>$ma_loai]);
            }
            else
            {
                return view('san_pham/loai_san_pham',['loai_san_pham'=>$loai_san_pham,'dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham,'ma_loai'=>$ma_loai]);
            }
            
        }
        else{
            $dsSanPham=san_pham::where('ma_loai',$id)->orderBy('don_gia','desc')->paginate(12);
            return view('san_pham/loai_san_pham',['loai_san_pham'=>$loai_san_pham,'dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham,'ma_loai'=>$ma_loai]);

        }
        
        
    }

    

    public function show($id)
    {
        $dsDoiTuong=doi_tuong::get();
        $san_pham=san_pham::where('ma_san_pham',$id)->first();
        $dsHinh=hinh_anh_chi_tiet::where('ma_san_pham',$id)->get();
        $loai_san_pham=loai_san_pham::where('ma_loai',$san_pham->ma_loai)->first();
        $san_pham_lien_quan=san_pham::where('ma_loai',$san_pham->ma_loai)->get();
        $dsDanhGia=danh_gia_san_pham::where('ma_san_pham',$id)->orderBy('created_at','desc')->get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        if(session()->has('recently_viewed_product'))
        {
            $xem_gan_day=session()->get('recently_viewed_product');
            $check=true;
            foreach ($xem_gan_day as $value) {
                if($value==$id)
                {
                    $check=false;
                }
            }
            if($check==true)
            {
                if(count($xem_gan_day)>=6)
                {
                    array_splice($xem_gan_day, 0, count($xem_gan_day)-5);
                }
                $xem_gan_day[]=$id;
            }
 
        }
        else{
            $xem_gan_day[]=$id;
        }
        session()->put('recently_viewed_product', $xem_gan_day);

        $like_status=false;
        if(session()->has('khach_hang'))
        {
            $ma_khach_hang=session()->get('khach_hang')->ma_khach_hang;
            $like=like_products::where('ma_khach_hang',$ma_khach_hang)->where('ma_san_pham',$id)->first();
            if($like)
            {
                $like_status=true;
            }
        }

        return view('san_pham/chi_tiet',['dsDanhGia'=>$dsDanhGia,'like_status'=>$like_status,'dsHinh'=>$dsHinh,'loai_san_pham'=>$loai_san_pham,'san_pham_lien_quan'=>$san_pham_lien_quan,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham,'san_pham'=>$san_pham]);
    }


    public function like(Request $request)
    {
        
        $ma_san_pham=$request->ma_san_pham;
        $ma_khach_hang=$request->ma_khach_hang;
        $like_status=$request->like_status;
        $san_pham=san_pham::where('ma_san_pham',$ma_san_pham)->first();
        
        if($like_status=='like')
        {
            DB::table('like_products')->where('ma_san_pham',$ma_san_pham)->where('ma_khach_hang',$ma_khach_hang)->delete();
            DB::table('san_pham')
            ->where('ma_san_pham', $ma_san_pham)
            ->update(['luot_thich' => $san_pham->luot_thich-1]);
            echo json_encode((array('n'=>0)));
        }
        else
        {
            
            $like=new like_products;          
            $like->ma_san_pham=$ma_san_pham;
            $like->ma_khach_hang=$ma_khach_hang;
            $like->save();

            
            DB::table('san_pham')
            ->where('ma_san_pham', $ma_san_pham)
            ->update(['luot_thich' => $san_pham->luot_thich+1]);

            echo json_encode((array('n'=>1)));
        }
        
    }


    public function san_pham_yeu_thich(Request $request)
    {
        $dsDoiTuong=doi_tuong::get();
        $dsHot=san_pham::orderBy('luot_mua','desc')->limit(4)->get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        
        if($request->session()->has('khach_hang'))
        {
            $dsYeuThich=like_products::where('ma_khach_hang',$request->session()->get('khach_hang')->ma_khach_hang)->get();
            $dsSanPham=array();
            if(count($dsYeuThich)>0)
            {
                foreach ($dsYeuThich as $like) {
                    $dsSanPham[]=san_pham::where('ma_san_pham',$like->ma_san_pham)->first();
                }
            }

            if(session()->has('recently_viewed_product'))
                {
                    $xem_gan_day=session()->get('recently_viewed_product');
                    foreach ($xem_gan_day as $item) {
                        $dsXemGanDay[]=san_pham::where('ma_san_pham',$item)->first();
                    }
                    return view('san_pham/san_pham_yeu_thich',['dsXemGanDay'=>$dsXemGanDay,'dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham]);
            }
            else
            {
                return view('san_pham/san_pham_yeu_thich',['dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham]);
            }
        }
        else {
            return view('error',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
        }
    }

    public function san_pham_giam_gia()
    {
        $dsDoiTuong=doi_tuong::get();
        $dsHot=san_pham::orderBy('luot_mua','desc')->limit(4)->get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        
        $dsSanPham=san_pham::where('gia_sau_khi_giam',"!=",0)->get();
            $giam_gia=1;

            if(session()->has('recently_viewed_product'))
                {
                    $xem_gan_day=session()->get('recently_viewed_product');
                    foreach ($xem_gan_day as $item) {
                        $dsXemGanDay[]=san_pham::where('ma_san_pham',$item)->first();
                    }
                    return view('san_pham/san_pham_yeu_thich',['giam_gia'=>$giam_gia,'dsXemGanDay'=>$dsXemGanDay,'dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham]);
            }
            else
            {
                return view('san_pham/san_pham_yeu_thich',['giam_gia'=>$giam_gia,'dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham]);
            }
    }


    public function tim_kiem(Request $request)
    {
        $dsDoiTuong=doi_tuong::get();
        $dsHot=san_pham::orderBy('luot_mua','desc')->limit(4)->get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        
        if($request->session()->has('khach_hang'))
        {
            $dsSanPham=DB::table('san_pham')->where('ten_san_pham', 'LIKE', '%' . $request->tu_khoa . '%')->get();
            

            if(session()->has('recently_viewed_product'))
                {
                    $xem_gan_day=session()->get('recently_viewed_product');
                    foreach ($xem_gan_day as $item) {
                        $dsXemGanDay[]=san_pham::where('ma_san_pham',$item)->first();
                    }
                    return view('san_pham/san_pham_yeu_thich',['dsXemGanDay'=>$dsXemGanDay,'dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham]);
            }
            else
            {
                return view('san_pham/san_pham_yeu_thich',['dsHot'=>$dsHot,'dsDoiTuong'=>$dsDoiTuong,'dsSanPham'=>$dsSanPham,'dsLoaiSanPham'=>$dsLoaiSanPham]);
            }
        }
        else {
            return view('error',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
        }
    }


    public function danh_gia(Request $request,$id)
    {
        $danh_gia_san_pham=new danh_gia_san_pham;
        $danh_gia_san_pham->ma_san_pham=$request->ma_san_pham;
        $danh_gia_san_pham->diem=$request->rating;
        $danh_gia_san_pham->noi_dung=$request->danh_gia;
        $danh_gia_san_pham->ma_khach_hang=$request->session()->get('khach_hang')->ma_khach_hang;
        $danh_gia_san_pham->tac_gia=$request->session()->get('khach_hang')->ten_khach_hang;
        $danh_gia_san_pham->save();
        return redirect('san_pham/chi_tiet/'.$id);
    }

    // ---------------------Quản lý sản phẩm

    public function create(Request $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        return view('quan_tri/them_san_pham',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
    }

    public function liet_ke(Request $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsSanPham=san_pham::paginate(10);
        foreach($dsSanPham as $sp)
        {
            $dsLSP[$sp->ma_san_pham]=loai_san_pham::where('ma_loai',$sp->ma_loai)->first();
        }
        return view('quan_tri/liet_ke_san_pham',['dsSanPham'=>$dsSanPham,'dsLSP'=>$dsLSP]);
    }

    public function liet_ke_het_hang(Request $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsSanPham=san_pham::where('so_luong','<=',0)->paginate(10);
        foreach($dsSanPham as $sp)
        {
            $dsLSP[$sp->ma_san_pham]=loai_san_pham::where('ma_loai',$sp->ma_loai)->first();
        }
        return view('quan_tri/liet_ke_san_pham',['dsSanPham'=>$dsSanPham,'dsLSP'=>$dsLSP]);
    }

    public function sua(Request $request,$id)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        $san_pham=san_pham::where('ma_san_pham',$id)->first();
        return view('quan_tri/sua_san_pham',['san_pham'=>$san_pham,'dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSanPham $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $validated=$request->validated;
        $san_pham_co_ma_cao_nhat=san_pham::orderBy('ma_san_pham','desc')->first();
        $ma_san_pham=$san_pham_co_ma_cao_nhat->ma_san_pham + 1;
        $loai_san_pham=loai_san_pham::where('ma_loai',$request->frm_loai_san_pham)->first();
        $now = new DateTime();

        $san_pham=new san_pham;
        $san_pham->$ma_san_pham;
        $san_pham->ten_san_pham=$request->frm_ten_san_pham;
        $san_pham->ma_loai=$request->frm_loai_san_pham;
		$san_pham->mo_ta=$request->frm_mo_ta;
        $san_pham->don_gia=$request->frm_don_gia;
        $san_pham->gia_sau_khi_giam=$request->frm_khuyen_mai;
		$san_pham->so_luong=$request->frm_so_luong;   
        $san_pham->doi_tuong=$loai_san_pham->doi_tuong;
        $san_pham->luot_mua=0;
        $san_pham->created_at=$now;
        $san_pham->updated_at=$now;

        $n=0;

        $dia_chi_luu_anh='public/source/img/san_pham/'.$request->frm_loai_san_pham.'/'.$ma_san_pham;
        if($request->hasFile('frm_hinh_anh_chinh')){
            $file = $request->frm_hinh_anh_chinh;
            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $san_pham->hinh=$file->getClientOriginalName();
            $n=$san_pham->save();

            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$ma_san_pham;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($request->hasFile('frm_hinh_anh_2')){
            $file = $request->frm_hinh_anh_2;

            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());

            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$ma_san_pham;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($request->hasFile('frm_hinh_anh_3')){
            $file = $request->frm_hinh_anh_3;

            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$ma_san_pham;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($request->hasFile('frm_hinh_anh_4')){
            $file = $request->frm_hinh_anh_4;

            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$ma_san_pham;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($request->hasFile('frm_hinh_anh_5')){
            $file = $request->frm_hinh_anh_5;

            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$ma_san_pham;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($request->hasFile('frm_hinh_anh_6')){
            $file = $request->frm_hinh_anh_6;

            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$ma_san_pham;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($n==1)
            return redirect('san_pham/them')->with('alert','Thêm thành công');
        else {
            return redirect('san_pham/them')->with('alert','ERROR');
        }
    }

    public function giam_gia($id)
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        $san_pham=san_pham::where('ma_san_pham',$id)->first();
        return view('san_pham/giam_gia',['san_pham'=>$san_pham,'dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
    }

    public function store_giam_gia(Request $request,$id)
    {     
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $giam_gia=$request->frm_gia_khuyen_mai;
        DB::table('san_pham')->where('ma_san_pham',$id)->update(['gia_sau_khi_giam'=>$giam_gia]);
        return redirect('san_pham/liet_ke');
    }

    public function edit(StoreSanPham $request,$id)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $validated=$request->validated;
        
        $loai_san_pham=loai_san_pham::where('ma_loai',$request->frm_loai_san_pham)->first(); 
        $n=0;

        $dia_chi_luu_anh='public/source/img/san_pham/'.$request->frm_loai_san_pham.'/'.$id;
        DB::table('hinh_anh_chi_tiet')->where('ma_san_pham', $id)->delete();
        if($request->hasFile('frm_hinh_anh_chinh')){
            $file = $request->frm_hinh_anh_chinh;
            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());

            $n=DB::table('san_pham')
            ->where('ma_san_pham', $id)
            ->update(['ten_san_pham' => $request->frm_ten_san_pham,'ma_loai'=>$request->frm_loai_san_pham,'mo_ta'=>$request->frm_mo_ta,'don_gia'=>$request->frm_don_gia,'gia_sau_khi_giam'=>$request->frm_khuyen_mai,'so_luong'=>$request->frm_so_luong,'doi_tuong'=>$loai_san_pham->doi_tuong,'hinh'=>$file->getClientOriginalName()]);

            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$id;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($request->hasFile('frm_hinh_anh_2')){
            $file = $request->frm_hinh_anh_2;

            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());

            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$id;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($request->hasFile('frm_hinh_anh_3')){
            $file = $request->frm_hinh_anh_3;

            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$id;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($request->hasFile('frm_hinh_anh_4')){
            $file = $request->frm_hinh_anh_4;

            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$id;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($request->hasFile('frm_hinh_anh_5')){
            $file = $request->frm_hinh_anh_5;

            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$id;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($request->hasFile('frm_hinh_anh_6')){
            $file = $request->frm_hinh_anh_6;

            $file->move($dia_chi_luu_anh, $file->getClientOriginalName());
            $hinh_anh_chi_tiet=new hinh_anh_chi_tiet;
            $hinh_anh_chi_tiet->ma_san_pham=$id;
            $hinh_anh_chi_tiet->hinh_anh=$file->getClientOriginalName();
            $hinh_anh_chi_tiet->save();
        }
        if($n==1)
            return redirect('san_pham/liet_ke');
        else {
            return redirect('san_pham/sua/'.$id)->with('alert','ERROR');
        }
    }


    public function xoa($id)
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        DB::table('san_pham')->where('ma_san_pham',$id)->delete();
        return redirect('san_pham/liet_ke');
    }

}
