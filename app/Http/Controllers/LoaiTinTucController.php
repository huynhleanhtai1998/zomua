<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\DB;
use App\san_pham;
use App\bai_viet;
use App\slider;
use App\doi_tuong;
use App\tin_tuc;
use App\danh_gia;
use App\khac_hang;
use App\loai_san_pham;
use App\giam_gia;
use App\hinh_anh_chi_tiet;
use App\loai_tin_tuc;
use DateTime;
use App\Http\Requests\StoreLoaiTinTuc;

class LoaiTinTucController extends Controller
{
    public function them(Request $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        return view('quan_tri/them_loai_tin_tuc');
    }

    public function store(StoreLoaiTinTuc $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        $validated=$request->validated;
        $loai_tin_tuc=new loai_tin_tuc;
        $loai_tin_tuc->ten_loai=$request->frm_ten_loai;
        $n=$loai_tin_tuc->save();
        if($n==1)
            return redirect('loai_tin_tuc/them')->with('alert','Thêm thành công');
        else {
            return redirect('loai_tin_tuc/them')->with('alert','ERROR');
        }
    }
    public function liet_ke(Request $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        $dsLoaiTinTuc=loai_tin_tuc::get();
        return view('quan_tri/liet_ke_loai_tin_tuc',['dsLoaiTinTuc'=>$dsLoaiTinTuc]);
    }
    public function sua(Request $request,$id)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        $loai_tin_tuc=loai_tin_tuc::where('ma_loai',$id)->first();
        return view('quan_tri/sua_loai_tin_tuc',['loai_tin_tuc'=>$loai_tin_tuc]);
    }

    public function edit(StoreLoaiTinTuc $request,$id)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        $validated=$request->validated;
        $n=DB::table('loai_tin_tuc')
            ->where('ma_loai', $id)
            ->update(['ten_loai' => $request->frm_ten_loai]);
        if($n==1)
            return redirect('loai_tin_tuc/liet_ke');
        else {
            return redirect('loai_tin_tuc/sua/'.$id)->with('alert','Sửa không thành công');
        }
    }

    public function xoa($id)
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->get();
        }
        DB::table('loai_tin_tuc')->where('ma_loai',$id)->delete();
        return redirect('loai_tin_tuc/liet_ke');
    }
}
