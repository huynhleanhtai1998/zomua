<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;
use App\san_pham;
use App\bai_viet;
use App\slider;
use App\doi_tuong;
use App\tin_tuc;
use App\danh_gia;
use App\khac_hang;
use App\loai_san_pham;
use App\giam_gia;
use App\khach_hang;
use App\hoa_don;
use App\ct_hoa_don;
use DateTime;
use Cart;

class GioHangController extends Controller
{
    public function index()
    {
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        return view('gio_hang/index',['dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
    }

    public function cap_nhat(Request $request)
    {

        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }

        $rowID=$request->frm_rowID;
        $so_luong=$request->frm_so_luong;
        Cart::update($rowID,$so_luong);

        return view('gio_hang/index',['dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
    }

    public function xoa($id)
    {
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        Cart::remove($id);
        return view('gio_hang/index',['dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
    }


    public function thanh_toan()
    {
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        return view('gio_hang/thanh_toan',['dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
    }

    public function loai_san_pham()
    {
        return view('san_pham/loai_san_pham');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dia_chi_giao_hang=$request->frm_dia_chi;
        $hinh_thuc_thanh_toan=$request->frm_hinh_thuc;
        $tri_gia=Cart::total();
        $tri_gia= str_replace(",","",$tri_gia);

        $ma_khach_hang=$request->session()->get('khach_hang')->ma_khach_hang;
        $id = DB::table('hoa_don')->insertGetId(
            ['hinh_thuc_thanh_toan'=>$hinh_thuc_thanh_toan,'dia_chi_giao_hang' => $dia_chi_giao_hang, 'tri_gia' => (int)$tri_gia, 'ma_khach_hang' => $ma_khach_hang, 'ngay_hd' => date('Y-m-d H:m:s'),'created_at'=>date('Y-m-d H:m:s'),'updated_at'=>date('Y-m-d H:m:s')]
        );


        foreach(Cart::content() as $row)
        {
            $ct_hoa_don=new ct_hoa_don;
            $ct_hoa_don->so_hoa_don=$id;
            $ct_hoa_don->ma_san_pham=$row->id;
            $ct_hoa_don->so_luong=$row->qty;
            $ct_hoa_don->don_gia=$row->price;
            $ct_hoa_don->size=$row->options->size;
            $ct_hoa_don->thanh_tien=$row->price*$row->qty;
            $ct_hoa_don->save();

            $san_pham=san_pham::where('ma_san_pham',$row->id)->first();
            $so_luong_san_pham_con_lai=$san_pham->so_luong-$row->qty;
            DB::table('san_pham')->where('ma_san_pham',$row->id)->update(['so_luong'=>$so_luong_san_pham_con_lai]);
        }
        $email=$request->session()->get('khach_hang')->email;
        $result=Mail::to($email)->send(new SendEmail($id));

        Cart::destroy();
        return redirect('/');
    }

    public function them(Request $request,$id)
    {
        $so_luong=$request->so_luong;
        $size=$request->size;
        $sp=san_pham::where('ma_san_pham',$id)->first();
        $gia=$sp->don_gia;
        if($sp->gia_sau_khi_giam!=0)
        {
            $gia=$sp->gia_sau_khi_giam;
        }
        if(empty($sp))
            echo json_encode((array('n'=>0)));
        else{
            Cart::add($sp->ma_san_pham,$sp->ten_san_pham,$so_luong,$gia,['ma_loai'=>$sp->ma_loai,'hinh'=>$sp->hinh,'size'=>$size]);
            echo json_encode((array('n'=>1)));
        }
    }

    public function tong_hop_hoa_don()
    {
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        if(session()->has('khach_hang'))
        {
            $dsHoaDon=array();
            $dsHoaDon=hoa_don::where('ma_khach_hang',session()->get('khach_hang')->ma_khach_hang)->get();
            return view('gio_hang/tong_hop_hoa_don',['dsHoaDon'=>$dsHoaDon,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
        }
        else 
        {
            return view('error',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
        }
    }

    public function chi_tiet_hoa_don($id)
    {
        $dsDoiTuong=doi_tuong::get();
        foreach($dsDoiTuong as $dt)
        {
            $dsLoaiSanPham[$dt->id]=loai_san_pham::where('doi_tuong',$dt->id)->paginate(4);
        }
        if(session()->has('khach_hang'))
        {
            $hoa_don=hoa_don::where('so_hoa_don',$id)->first();
            $dsCTHD=ct_hoa_don::where('so_hoa_don',$id)->get();
            foreach($dsCTHD as $cthd)
            {
                $dsSanPham[]=san_pham::where('ma_san_pham',$cthd->ma_san_pham)->first();
            }
            return view('gio_hang/chi_tiet_hoa_don',['hoa_don'=>$hoa_don,'dsSanPham'=>$dsSanPham,'dsCTHD'=>$dsCTHD,'dsDoiTuong'=>$dsDoiTuong,'dsLoaiSanPham'=>$dsLoaiSanPham]);
        }
        else 
        {
            return view('error',['dsLoaiSanPham'=>$dsLoaiSanPham,'dsDoiTuong'=>$dsDoiTuong]);
        }
    }

    public function liet_ke_hoa_don_chua_giao()
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_hoa_don!=1)
        {
            return view('error');
        }
        $dsHoaDon=DB::table('hoa_don')
        ->join('khach_hang','khach_hang.ma_khach_hang','=','hoa_don.ma_khach_hang')
        ->select(DB::raw('hoa_don.so_hoa_don,hoa_don.ma_khach_hang,hoa_don.giao_hang,hoa_don.tri_gia,hoa_don.nhan_hang,hoa_don.hinh_thuc_thanh_toan,
        hoa_don.thanh_toan,khach_hang.ten_khach_hang'))
        ->where('hoa_don.giao_hang',0)
        ->paginate(20);
        return view('quan_tri/liet_ke_hoa_don',['dsHoaDon'=>$dsHoaDon]);
    }

    public function liet_ke_hoa_don_dang_giao()
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_hoa_don!=1)
        {
            return view('error');
        }
        $dsHoaDon=DB::table('hoa_don')
        ->join('khach_hang','khach_hang.ma_khach_hang','=','hoa_don.ma_khach_hang')
        ->select(DB::raw('hoa_don.so_hoa_don,hoa_don.ma_khach_hang,hoa_don.giao_hang,hoa_don.tri_gia,hoa_don.nhan_hang,hoa_don.hinh_thuc_thanh_toan,
        hoa_don.thanh_toan,khach_hang.ten_khach_hang'))
        ->where('hoa_don.giao_hang',1)->where('hoa_don.nhan_hang',0)
        ->paginate(20);
        return view('quan_tri/liet_ke_hoa_don',['dsHoaDon'=>$dsHoaDon]);
    }

    public function liet_ke_hoa_don_da_giao()
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_hoa_don!=1)
        {
            return view('error');
        }
        $dsHoaDon=DB::table('hoa_don')
        ->join('khach_hang','khach_hang.ma_khach_hang','=','hoa_don.ma_khach_hang')
        ->select(DB::raw('hoa_don.so_hoa_don,hoa_don.ma_khach_hang,hoa_don.giao_hang,hoa_don.tri_gia,hoa_don.nhan_hang,hoa_don.hinh_thuc_thanh_toan,
        hoa_don.thanh_toan,khach_hang.ten_khach_hang'))
        ->where('hoa_don.nhan_hang',1)
        ->paginate(20);
        return view('quan_tri/liet_ke_hoa_don',['dsHoaDon'=>$dsHoaDon]);
    }

    public function liet_ke_hoa_don_chua_chuyen_khoan()
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_hoa_don!=1)
        {
            return view('error');
        }
        $dsHoaDon=DB::table('hoa_don')
        ->join('khach_hang','khach_hang.ma_khach_hang','=','hoa_don.ma_khach_hang')
        ->select(DB::raw('hoa_don.so_hoa_don,hoa_don.ma_khach_hang,hoa_don.giao_hang,hoa_don.tri_gia,hoa_don.nhan_hang,hoa_don.hinh_thuc_thanh_toan,
        hoa_don.thanh_toan,khach_hang.ten_khach_hang'))
        ->where('hoa_don.hinh_thuc_thanh_toan',1)->where('hoa_don.thanh_toan',0)
        ->paginate(20);
        return view('quan_tri/liet_ke_hoa_don',['dsHoaDon'=>$dsHoaDon]);
    }

    public function xoa_don_hang($id)
    {
        if(session()->has('user')==false || session()->get('user')->quan_ly_hoa_don!=1)
        {
            return view('error');
        }
        $hoa_don=hoa_don::find($id);
        $hoa_don->delete();

        DB::table('ct_hoa_don')->where('so_hoa_don', $id)->delete();
        return redirect()->back();
    }
}
