<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\san_pham;
use App\bai_viet;
use App\slider;
use App\doi_tuong;
use App\tin_tuc;
use App\danh_gia;
use App\khach_hang;
use App\loai_san_pham;
use App\giam_gia;
use App\hoa_don;
use App\users;
use App\hinh_anh_chi_tiet;
use DateTime;

class QuanTriController extends Controller
{
    public function index(Request $request)
    {
        if($request->session()->has('user')==false)
        {
            return view('error');
        }
        $so_khach_hang=khach_hang::count();
        $so_san_pham=san_pham::count();
        $so_tin_tuc=tin_tuc::count();
        $so_don_hang=hoa_don::count();
        $ThongKeSanPhamBanChay=DB::table('ct_hoa_don')
        ->select(DB::raw("ma_san_pham,sum(thanh_tien) as thanh_tien"))
        ->limit(10)
        ->where(DB::raw("MONTH(created_at)"),date('m'))
        ->groupBy(DB::raw("ma_san_pham"))
        ->orderBy('thanh_tien','desc')
        ->get();

        $ThongKeSanPhamYeuThich=DB::table('san_pham')
        ->select(DB::raw("ma_san_pham, luot_thich"))
        ->limit(10)
        ->where(DB::raw("MONTH(created_at)"),date('m'))
        ->orderBy('luot_thich','desc')
        ->get();

        $ThongKeTinTucXemNhieu=DB::table('tin_tuc')
        ->select(DB::raw("id as ma_tin_tuc, luot_xem"))
        ->limit(10)
        ->where(DB::raw("MONTH(created_at)"),date('m'))
        ->orderBy('luot_xem','desc')
        ->get();

        $ThongKeTinTucYeuThich=DB::table('tin_tuc')
        ->select(DB::raw("id as ma_tin_tuc, luot_thich"))
        ->limit(10)
        ->where(DB::raw("MONTH(created_at)"),date('m'))
        ->orderBy('luot_thich','desc')
        ->get();
        return view('quan_tri/index',['ThongKeTinTucYeuThich'=>$ThongKeTinTucYeuThich,'ThongKeTinTucXemNhieu'=>$ThongKeTinTucXemNhieu,'ThongKeSanPhamYeuThich'=>$ThongKeSanPhamYeuThich,'ThongKeSanPhamBanChay'=>$ThongKeSanPhamBanChay,'so_khach_hang'=>$so_khach_hang,'so_san_pham'=>$so_san_pham,'so_tin_tuc'=>$so_tin_tuc,'so_don_hang'=>$so_don_hang]);
    }

    public function thong_ke_san_pham(Request $request)
    {
        if($request->session()->has('user')==false||$request->session()->get('user')->quan_ly_san_pham!=1)
        {
            return view('error');
        }
        $ThongKeSanPhamBanChay=DB::table('ct_hoa_don')
        ->select(DB::raw("ma_san_pham,sum(thanh_tien) as thanh_tien"))
        ->limit(10)
        ->where(DB::raw("MONTH(created_at)"),date('m'))
        ->groupBy(DB::raw("ma_san_pham"))
        ->orderBy('thanh_tien','desc')
        ->get();

        $ThongKeSanPhamYeuThich=DB::table('san_pham')
        ->select(DB::raw("ma_san_pham, luot_thich"))
        ->limit(10)
        ->where(DB::raw("MONTH(created_at)"),date('m'))
        ->orderBy('luot_thich','desc')
        ->get();

        $ThongKeSanPhamTheoLoai=DB::table('loai_san_pham')
        ->join('san_pham','san_pham.ma_loai','=','loai_san_pham.ma_loai')
        ->select(DB::raw('san_pham.ma_loai,ten_loai,count(san_pham.ma_san_pham) as so_luong'))
        ->groupBy('san_pham.ma_loai','ten_loai')
        ->get();

        $ThongKeDoanhThu=DB::table('hoa_don')
        ->select(DB::raw("MONTH(created_at) as thang,sum(tri_gia) as doanh_thu"))
        ->groupBy(DB::raw("MONTH(created_at)"))
        ->get();
        return view('quan_tri/thong_ke_san_pham',['ThongKeDoanhThu'=>$ThongKeDoanhThu,'ThongKeSanPhamTheoLoai'=>$ThongKeSanPhamTheoLoai,'ThongKeSanPhamYeuThich'=>$ThongKeSanPhamYeuThich,'ThongKeSanPhamBanChay'=>$ThongKeSanPhamBanChay]);
    }

    public function thong_ke_tin_tuc(Request $request)
    {
        if($request->session()->has('user')==false||$request->session()->get('user')->quan_ly_tin_tuc!=1)
        {
            return view('error');
        }
        $ThongKeTinTucXemNhieu=DB::table('tin_tuc')
        ->select(DB::raw("id as ma_tin_tuc, luot_xem"))
        ->limit(10)
        ->where(DB::raw("MONTH(created_at)"),date('m'))
        ->orderBy('luot_xem','desc')
        ->get();

        $ThongKeTinTucYeuThich=DB::table('tin_tuc')
        ->select(DB::raw("id as ma_tin_tuc, luot_thich"))
        ->limit(10)
        ->where(DB::raw("MONTH(created_at)"),date('m'))
        ->orderBy('luot_thich','desc')
        ->get();
        $ThongKeTinTucNhieuBinhLuan=DB::table('tin_tuc')
        ->select(DB::raw("id as ma_tin_tuc, binh_luan"))
        ->limit(10)
        ->where(DB::raw("MONTH(created_at)"),date('m'))
        ->orderBy('binh_luan','desc')
        ->get();

        return view('quan_tri/thong_ke_tin_tuc',['ThongKeTinTucXemNhieu'=>$ThongKeTinTucXemNhieu,'ThongKeTinTucYeuThich'=>$ThongKeTinTucYeuThich,'ThongKeTinTucNhieuBinhLuan'=>$ThongKeTinTucNhieuBinhLuan]);
    }

    public function dang_nhap()
    {
        return view('quan_tri/dang_nhap');
    }

    public function dang_xuat()
    {
        session()->flush();
        return redirect('quan_tri/');
    }

    public function xu_ly_dang_nhap(Request $request)
    {
        $email=$request->email;
        $pass=$request->pass;
        $user=users::where('email',$email)->where('password',$pass)->first();
        if($user)
        {
            $request->session()->put('user',$user);
            if(isset($request->remember))
            {
                return redirect('quan_tri/index')->withCookie('user_email',$email,4500)->withCookie('user_pass',$pass,4500);
            }
            return redirect('quan_tri/index');
        }
    }

    public function liet_ke_user()
    {
        $dsUser=users::get();
        return view('quan_tri/liet_ke_user',['dsUser'=>$dsUser]);
    }

    public function xoa_user($id)
    {
        $user=users::find($id);
        $user->delete();
        return redirect('quan_tri/liet_ke_user');
    }


    public function them_user()
    {
        return view('quan_tri/them_user');
    }

    public function luu_user(Request $request)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_user!=1)
        {
            return view('error');
        }
        $user=new users;
        $user->ho_ten=$request->frm_ho_ten;
        $user->email=$request->frm_email;
        $user->password=$request->frm_password;
        $user->gioi_tinh=$request->frm_gioi_tinh;
        $user->quan_ly_san_pham=(isset($request->frm_quan_ly_san_pham))?1:0;
        $user->quan_ly_tin_tuc=(isset($request->frm_quan_ly_tin_tuc))?1:0;
        $user->quan_ly_khach_hang=(isset($request->frm_quan_ly_khach_hang))?1:0;
        $user->quan_ly_hoa_don=(isset($request->frm_quan_ly_hoa_don))?1:0;
        $user->quan_ly_user=(isset($request->frm_quan_ly_user))?1:0;
        $n=$user->save();
        if($n==1)
            return redirect('quan_tri/them_user')->with('alert','Thêm thành công');
        else {
            return redirect('quan_tri/them_user')->with('alert','Thêm thất bại');
        }
    }

    public function sua_user($id)
    {
        $user=users::where('id',$id)->first();
        return view('quan_tri/sua_user',['user'=>$user]);
    }

    public function luu_sua_user(Request $request,$id)
    {
        if($request->session()->has('user')==false || $request->session()->get('user')->quan_ly_user!=1)
        {
            return view('error');
        }
        $user=users::find($id);
        $user->ho_ten=$request->frm_ho_ten;
        $user->email=$request->frm_email;
        $user->password=$request->frm_password;
        $user->gioi_tinh=$request->frm_gioi_tinh;
        $user->quan_ly_san_pham=(isset($request->frm_quan_ly_san_pham))?1:0;
        $user->quan_ly_tin_tuc=(isset($request->frm_quan_ly_tin_tuc))?1:0;
        $user->quan_ly_khach_hang=(isset($request->frm_quan_ly_khach_hang))?1:0;
        $user->quan_ly_hoa_don=(isset($request->frm_quan_ly_hoa_don))?1:0;
        $user->quan_ly_user=(isset($request->frm_quan_ly_user))?1:0;
        $n=$user->save();
        if($n==1)
            return redirect('quan_tri/user/sua/'.$id)->with('alert','Thêm thành công');
        else {
            return redirect('quan_tri/user/sua/'.$id)->with('alert','Thêm thất bại');
        }
    }
}
