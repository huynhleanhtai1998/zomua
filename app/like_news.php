<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class like_news extends Model
{
    protected $table='like_news';
    protected $fillable=['ma_tin_tuc', 'ma_khach_hang'];
}
