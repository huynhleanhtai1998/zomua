<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class loai_bai_viet extends Model
{
    protected $table='loai_bai_viet';
    protected $fillable=['ma_loai_bai_viet', 'ten_loai_bai_viet', 'mo_ta', 'ma_loai_cha'];
}
