<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hoa_don extends Model
{
    protected $table='hoa_don';
    protected $primaryKey = 'so_hoa_don';
    protected $fillable=['so_hoa_don', 'ngay_hd', 'ma_khach_hang', 'tri_gia', 'dia_chi_giao_hang', 'created_at', 'updated_at', 'hinh_thuc_thanh_toan', 'nhan_hang', 'thanh_toan_tien', 'giao_hang'];
}
