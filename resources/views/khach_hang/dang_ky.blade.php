@extends('master')
@section('content')


 <!-- Cart view section -->
 <section id="aa-myaccount">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
         <div class="aa-myaccount-area">         
             <div class="row">
               <div class="col-md-12">
                 <div class="aa-myaccount-login">
                 <h4>Đăng ký</h4>

                  <form method="POST" class="aa-login-form">
                    @csrf
                    <label for="">Họ và tên<span>*</span></label>
                    <input name="ho_ten" type="text" placeholder="Nhập họ tên">
                    <label for="">Địa chỉ email<span>*</span></label>
                    <input name="email" type="text" placeholder="Nhập email">
                    <label for="">Điện thoại<span>*</span></label>
                    <input name="dien_thoai" type="number" placeholder="Nhập số điện thoại"><br>
                    <label for="">Ngày sinh<span>*</span></label>
                    <input name="ngay_sinh" type="date" placeholder="Nhập ngày sinh"><br>
                    <label for="">Giới tính<span>*</span></label>
                            <input name="frm_gioi_tinh" type="radio" value="0"> Nam
                            <input name="frm_gioi_tinh" type="radio" value="1"> Nữ<br>
                    <label for="">Địa chỉ<span>*</span></label>
                    <input name="dia_chi" type="text" placeholder="Nhập địa chỉ">
                    <label for="">Mật khẩu<span>*</span></label>
                    <input id="mat_khau" name="mat_khau" type="password" placeholder="Password">
                    <label for="">Nhập lại mật khẩu<span>*</span></label>
                    <input id="frm_nhap_lai_password" type="password" placeholder="Password">
                    <button id="frm_submit" name="frm_submit" type="submit" class="aa-browse-btn">Tạo tài khoản</button>
                   </form>

                 </div>
               </div>
             </div>          
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Cart view section --

@endsection