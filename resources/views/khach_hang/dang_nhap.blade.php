@extends('master')
@section('content')


 <!-- Cart view section -->
 <section id="aa-myaccount">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
         <div class="aa-myaccount-area">         
             <div class="row">
               <div class="col-md-12">
                 <div class="aa-myaccount-login">
                 <h4>Đăng nhập</h4>

                  <form method="POST" class="aa-login-form">
                    @csrf
                   <label for="">Địa chỉ email<span>*</span></label>
                    <input name="frm_email" type="text" placeholder="Nhập email">
                    <label for="">Mật khẩu<span>*</span></label>
                     <input name="frm_password" type="password" placeholder="Password">
                     <button name="frm_submit" type="submit" class="aa-browse-btn">Đăng nhập</button>
                     <label name="frm_ghi_nho" class="rememberme" for="rememberme"><input type="checkbox" id="rememberme"> Ghi nhớ </label>
                  <p class="aa-lost-password"><a href="{{URL('khach_hang/dang_ky')}}">Đăng ký</a></p>
                   </form>

                 </div>
               </div>
             </div>          
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Cart view section -->
  
@endsection