@extends('master')
@section('content')
<div class="content" style="width:80%; margin: 10px auto">
        <h1 style="color: red; text-align:center">Quản lý khách hàng</h1>
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Mã khách hàng</th>
                            <th scope="col">Tên khách hàng</th>
                            <th scope="col">Email</th>
                            <th scope="col">Giới tính</th>
                            <th scope="col">Địa chỉ</th>
                            <th scope="col">Nâng cấp thành admin</th>
                          </tr>
                        </thead>
                        <tbody>
                         
                           @foreach ($dsKhachHang as $kh)
                            <tr>
                                <th scope="col">{{$kh->ma_khach_hang}}</th>
                                <th scope="col">{{$kh->ten_khach_hang}}</th>
                                <th scope="col">{{$kh->email}}</th>
                                <th scope="col">@if($kh->phai==0)Nam @else Nữ @endif</th>
                                <th scope="col">{{$kh->dia_chi}}</th>
                                @if($kh->ma_loai_nguoi_dung==1)
                                <th scope="col">Admin</th>
                                @else
                                <th scope="col"><a href="{{URL('khach_hang/nang_quyen_admin/'.$kh->ma_khach_hang)}}" style="color: blue">Nâng quyền admin</a></th>
                                @endif
                            </tr>  
                           @endforeach
                         
                        </tbody>
                      </table>
</div>
@endsection