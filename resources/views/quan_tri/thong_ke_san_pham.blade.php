@include('quan_tri/head')
@include('quan_tri/side_bar')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          
          <!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-6 connectedSortable">
              <!-- Custom tabs (Charts with tabs)-->
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left header"><i class="fa fa-inbox"></i>10 sản phẩm doanh thu cao nhất tháng</li>
                </ul>
                <div class="tab-content no-padding">
                    <canvas id="chart_doanh_thu_cao_nhat" style="position: relative; height: 300px;"></canvas>
                </div>
              </div>
              <!-- /.nav-tabs-custom -->
    
              <div class="nav-tabs-custom">
                  <!-- Tabs within a box -->
                  <ul class="nav nav-tabs pull-right">
                    <li class="pull-left header"><i class="fa fa-inbox"></i>10 sản phẩm có lượt yêu thích cao nhất tháng</li>
                  </ul>
                  <div class="tab-content no-padding">
                    <!-- Morris chart - Sales -->
                    <canvas id="chart_san_pham_yeu_thich" style="position: relative; height: 300px;"></canvas>
                  </div>
                </div>
    
            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-6 connectedSortable">
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-right">
                      <li class="pull-left header"><i class="fa fa-inbox"></i>Số lượng sản phẩm của các loại</li>
                    </ul>
                    <div class="tab-content no-padding">
                      <!-- Morris chart - Sales -->
                      <canvas id="so_luong_san_pham" style="position: relative; height: 300px;"></canvas>
                    </div>
                </div>
    
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-right">
                      <li class="pull-left header"><i class="fa fa-inbox"></i>Doanh thu các tháng</li>
                    </ul>
                    <div class="tab-content no-padding">
                      <!-- Morris chart - Sales -->
                      <canvas id="doanh_thu_theo_thang" style="position: relative; height: 300px;"></canvas>
                    </div>
                </div>
            </section>
            <!-- right col -->
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
      </div>

      
@section('script')
@parent
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
<script>
var ctx = document.getElementById('chart_doanh_thu_cao_nhat').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            @foreach($ThongKeSanPhamBanChay as $sp)
                '{{$sp->ma_san_pham}}',
            @endforeach    
        ],
        datasets: [{
            label: 'Doanh số',
            data: [
                @foreach($ThongKeSanPhamBanChay as $sp)
                '{{$sp->thanh_tien}}',
            @endforeach  
            ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        title: {
            display: true,
            text: '10 sản phẩm có doanh thu cao nhất trong tháng'
        },
        legend: {
    display: true
    },
    tooltips: {
    mode: 'label',
    label: 'Doanh thu',
    callbacks: {
        label: function(tooltipItem, data) {
            return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); }, },
    },
    scales: {
        yAxes: [{
            ticks: {
                callback: function(label, index, labels) { return label; },
                beginAtZero:true,
                fontSize: 15,
            },
            gridLines: {
                display: true
            },
            scaleLabel: { 
                display: true,
                fontSize: 15,
            }
        }],
        xAxes: [{
            ticks: {
                beginAtZero: true,
                fontSize: 15
            },
            gridLines: {
                display:true
            },
            scaleLabel: {
                display: true,
                fontSize: 15,
        }
        }]
    }

    }
});

var canvasP = document.getElementById('chart_doanh_thu_cao_nhat')
canvasP.onclick = function(e) {
    var slice = myChart.getElementAtEvent(e);
    if (!slice.length) return; // return if not clicked on slice
    var label = slice[0]._model.label;
    switch (label) {
        @foreach($ThongKeSanPhamBanChay as $sp)
            case '{{$sp->ma_san_pham}}':
            window.open('san_pham/chi_tiet/{{$sp->ma_san_pham}}');
        @endforeach    
    }
}
</script>

<script>
    var ctx = document.getElementById('chart_san_pham_yeu_thich').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                @foreach($ThongKeSanPhamYeuThich as $sp)
                    '{{$sp->ma_san_pham}}',
                @endforeach          
            ],
            datasets: [{
                label: 'Lượt yêu thích',
                data: [
                    @foreach($ThongKeSanPhamYeuThich as $sp)
                    '{{$sp->luot_thich}}',
                @endforeach  
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            title: {
                display: true,
                text: '10 sản phẩm được yêu thích nhiều nhất trong tháng'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    
    var canvasP = document.getElementById('chart_san_pham_yeu_thich')
    canvasP.onclick = function(e) {
        var slice = myChart.getElementAtEvent(e);
        if (!slice.length) return; // return if not clicked on slice
        var label = slice[0]._model.label;
        switch (label) {
            @foreach($ThongKeSanPhamYeuThich as $sp)
                case '{{$sp->ma_san_pham}}':
                window.open('san_pham/chi_tiet/{{$sp->ma_san_pham}}');
            @endforeach    
        }
    }
</script>

<script>
        var ctx = document.getElementById('so_luong_san_pham').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    @foreach($ThongKeSanPhamTheoLoai as $lsp)
                        '{{$lsp->ten_loai}}',
                    @endforeach          
                ],
                datasets: [{
                    label: 'Số lượng',
                    data: [
                        @foreach($ThongKeSanPhamTheoLoai as $lsp)
                        '{{$lsp->so_luong}}',
                    @endforeach  
                    ],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Số lượng sản phẩm theo loại'
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
</script>

<script>
        var ctx = document.getElementById('doanh_thu_theo_thang').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    @foreach($ThongKeDoanhThu as $item)
                        '{{$item->thang}}',
                    @endforeach          
                ],
                datasets: [{
                    data: [
                        @foreach($ThongKeDoanhThu as $item)
                        '{{$item->doanh_thu}}',
                        @endforeach  ],
                        label: 'Doanh thu',
                        borderColor: "#3e95cd",
                        fill: false
                }]
            },
            options: {
                title: {
                display: true,
                text: 'Thống kê doanh thu theo tháng',
                },
            legend: {
            display: true
            },
            tooltips: {
            mode: 'label',
            label: 'Doanh thu',
            callbacks: {
                label: function(tooltipItem, data) {
                    return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); }, },
            },
            scales: {
                yAxes: [{
                    ticks: {
                        callback: function(label, index, labels) { return label; },
                        beginAtZero:true,
                        fontSize: 15,
                    },
                    gridLines: {
                        display: true
                    },
                    scaleLabel: { 
                        display: true,
                        fontSize: 15,
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        fontSize: 15
                    },
                    gridLines: {
                        display:true
                    },
                    scaleLabel: {
                        display: true,
                        fontSize: 15,
                }
                }]
            }

            }
        });
</script>
@endsection
      
@include('quan_tri/footer')