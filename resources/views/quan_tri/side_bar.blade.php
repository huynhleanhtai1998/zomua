<body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        
          <header class="main-header">
            <!-- Logo -->
          <a href="{{URL('/')}}" class="logo">
              <!-- logo for regular state and mobile devices -->
              <span class="logo-lg"><b>ATShop</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
              <!-- Sidebar toggle button-->
              <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
              </a>
            </nav>
          </header>
          <!-- Left side column. contains the logo and sidebar -->
          <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
              <!-- Sidebar user panel -->
              <div class="user-panel">
                <div class="pull-left image">
                  @if(Session::get('user')->gioi_tinh==1)
                  <img src="public/source/img/user/girl.png" class="img-circle" alt="User Image">
                  @else
                  <img src="public/source/img/user/boy.png" class="img-circle" alt="User Image">
                  @endif
                </div>
                <div class="pull-left info">
                  <p>{{Session::get('user')->ho_ten}}</p>
                  <i class="fa fa-circle text-success"></i> Online ||
                  <a href="quan_tri/dang_xuat" style="font-size:15px;color:red"> Đăng xuất</a>
                </div>
              </div>
              
              <!-- sidebar menu: : style can be found in sidebar.less -->
              <ul class="sidebar-menu" data-widget="tree">
                @if(Session::get('user')->quan_ly_san_pham==1)
                <li class="treeview">
                  <a href="#">
                      <i class="fa fa-folder"></i>
                    <span>Quản lý sản phẩm</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="{{URL('san_pham/liet_ke')}}"><i class="fa fa-table"></i></i>Liệt kê sản phẩm</a></li>
                    <li><a href="{{URL('san_pham/them')}}"><i class="fa fa-share"></i> Thêm sản phẩm</a></li>
                    <li><a href="{{URL('san_pham/liet_ke_het_hang')}}"><i class="fa fa-table"></i></i>Liệt kê sản phẩm hết hàng</a></li>
                  </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i>
                      <span>Quản lý loại sản phẩm</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">
                      <li><a href="{{URL('loai_san_pham/liet_ke')}}"><i class="fa fa-table"></i></i>Liệt kê loại sản phẩm</a></li>
                      <li><a href="{{URL('loai_san_pham/them')}}"><i class="fa fa-share"></i> Thêm loại sản phẩm</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i>
                      <span>Quản lý đối tượng sản phẩm</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">
                      <li><a href="{{URL('doi_tuong/liet_ke')}}"><i class="fa fa-table"></i></i>Liệt kê đối tượng sản phẩm</a></li>
                      <li><a href="{{URL('doi_tuong/them')}}"><i class="fa fa-share"></i>Thêm đối tượng sản phẩm</a></li>
                    </ul>
                </li>
              @endif
              @if(Session::get('user')->quan_ly_tin_tuc==1)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-book"></i>
                      <span>Quản lý tin tức</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">
                      <li><a href="{{URL('tin_tuc/liet_ke')}}"><i class="fa fa-table"></i></i>Liệt kê danh sách tin tức</a></li>
                      <li><a href="{{URL('tin_tuc/them')}}"><i class="fa fa-share"></i>Thêm tin tức</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-book"></i>
                      <span>Quản lý loại tin tức</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">
                      <li><a href="{{URL('loai_tin_tuc/liet_ke')}}"><i class="fa fa-table"></i></i>Liệt kê loại tin tức</a></li>
                      <li><a href="{{URL('loai_tin_tuc/them')}}"><i class="fa fa-share"></i>Thêm loại tin tức</a></li>
                    </ul>
                </li>
                @endif

                @if(Session::get('user')->quan_ly_user==1)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-book"></i>
                      <span>Quản lý user</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">
                      <li><a href="{{URL('quan_tri/liet_ke_user')}}"><i class="fa fa-table"></i></i>Liệt kê user</a></li>
                      <li><a href="{{URL('quan_tri/them_user')}}"><i class="fa fa-share"></i>Thêm user</a></li>
                    </ul>
                </li>
                @endif

                @if(Session::get('user')->quan_ly_khach_hang==1)
                <li><a href="{{URL('khach_hang/liet_ke')}}"><i class="fa fa-table"></i></i>Quản lý khách hàng</a></li>
                @endif

                @if(Session::get('user')->quan_ly_hoa_don==1)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-book"></i>
                      <span>Quản lý đơn hàng</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">
                      <li><a href="{{URL('quan_tri/liet_ke_hoa_don_chua_giao')}}"><i class="fa fa-table"></i></i>Đơn hàng chưa giao</a></li>
                      <li><a href="{{URL('quan_tri/liet_ke_hoa_don_dang_giao')}}"><i class="fa fa-table"></i></i>Đơn hàng đang giao</a></li>
                      <li><a href="{{URL('quan_tri/liet_ke_hoa_don_da_giao')}}"><i class="fa fa-share"></i>Đơn hàng đã giao</a></li>
                      <li><a href="{{URL('quan_tri/liet_ke_hoa_don_chua_chuyen_khoan')}}"><i class="fa fa-share"></i>Đơn hàng chưa chuyển khoản</a></li>
                    </ul>
                </li>
                @endif

                @if(Session::get('user')->quan_ly_san_pham==1||Session::get('user')->quan_ly_tin_tuc==1)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i>
                      <span>Thống kê</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(Session::get('user')->quan_ly_san_pham==1)
                      <li><a href="{{URL('quan_tri/thong_ke_san_pham')}}"><i class="fa fa-pie-chart"></i></i>Sản phẩm</a></li>
                      @endif
                      @if(Session::get('user')->quan_ly_tin_tuc==1)
                      <li><a href="{{URL('quan_tri/thong_ke_tin_tuc')}}"><i class="fa fa-pie-chart"></i>Tin tức</a></li>
                      @endif
                    </ul>
                </li>
                @endif
              </ul>
            </section>
            <!-- /.sidebar -->
          </aside>