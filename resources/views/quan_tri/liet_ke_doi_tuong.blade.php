@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row">
                <h1 style="color: red; text-align:center">Quản lý đối tượng</h1>
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Tên đối tượng</th>
                            <th scope="col">Xóa</th>
                            <th scope="col">Sửa</th>
                          </tr>
                        </thead>
                        <tbody>
                         
                           @foreach ($dsDoiTuong as $dt)
                            <tr>
                                <th scope="col">{{$dt->id}}</th>
                                <th scope="col">{{$dt->ten_doi_tuong}}</th>
                                <th scope="col"><a onclick="return xoa_click();" href="{{URL('doi_tuong/xoa/'.$dt->id)}}" style="color: red">Xóa</a></th>
                            <th scope="col"><a href="{{URL('doi_tuong/sua/'.$dt->id)}}" style="color: blue">Sửa</a></th>
                            </tr>  
                           @endforeach
                         
                        </tbody>
                      </table>
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>
    @section('script')
        @parent
        <script>
        function xoa_click()
        {
          if(confirm("Bấm vào nút OK để tiếp tục") == true){
            }else{
                return false; 
            }
        }
        </script>
    @endsection
      
@include('quan_tri/footer')