@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row" style="padding:20px">
                <h1 style="color:blue; text-align:center;">Thêm User</h1>
                <div class="content_message">
                @if(count($errors)>0)
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{$error}}<br>
                    </div>
                @endforeach
                @endif  

                @if(session('alert'))
                    <div class="alert alert-success">
                        {{session('alert')}}<br>
                    </div>
                @endif
            </div>
                <form method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                        <label for="frm_ho_ten">Họ và tên</label>
                        <input required="required" name="frm_ho_ten" type="text" class="form-control" value="{{$user->ho_ten}}" id="frm_ho_ten" placeholder="Nhập họ và tên">
                        </div>

                        <div class="form-group">
                                <label for="frm_email">Email</label>
                                <input required="required" name="frm_email" type="email" class="form-control" value="{{$user->email}}" id="frm_email" placeholder="Nhập email">
                        </div>

                        <div class="form-group">
                                <label for="frm_password">Password</label>
                                <input required="required" name="frm_password" type="password" class="form-control" value="{{$user->password}}" id="frm_password" placeholder="Nhập mật khẩu">
                        </div>

                        <div class="form-group">
                                <label for="frm_re_password">Nhập lại Password</label>
                                <input required="required" name="frm_re_password" type="password" class="form-control" value="{{$user->password}}" id="frm_re_password" placeholder="Nhập lại mật khẩu">
                        </div>

                        <div class="form-group">
                                <label>Giới tính</label><br>
                                <input name="frm_gioi_tinh" type="radio"  @if($user->gioi_tinh==0) checked @endif value="0">Nam<br>
                                <input name="frm_gioi_tinh" type="radio"  @if($user->gioi_tinh==1) checked @endif value="1">Nữ<br>
                        </div>

                        <div class="form-group">
                                <label>Quản lý sản phẩm</label><br>
                                <input name="frm_quan_ly_san_pham" type="checkbox" value="1" @if($user->quan_ly_san_pham==1) checked @endif>
                        </div>

                        <div class="form-group">
                                <label>Quản lý tin tức</label><br>
                                <input name="frm_quan_ly_tin_tuc" type="checkbox" value="1" @if($user->quan_ly_tin_tuc==1) checked @endif>
                        </div>
                        
                        <div class="form-group">
                                <label>Quản lý khách hàng</label><br>
                                <input name="frm_quan_ly_khach_hang" type="checkbox" value="1" @if($user->quan_ly_khach_hang==1) checked @endif>
                        </div>

                        <div class="form-group">
                                <label>Quản lý hóa đơn</label><br>
                                <input name="frm_quan_ly_hoa_don" type="checkbox" value="1" @if($user->quan_ly_hoa_don==1) checked @endif>
                        </div>

                        <div class="form-group">
                                <label>Quản lý user</label><br>
                                <input name="frm_quan_ly_user" type="checkbox" value="1" @if($user->quan_ly_user==1) checked @endif>
                        </div>

                        <input onclick="return submit_click();"  name="frm_submit" type="submit" class="btn btn-primary" value="Submit" >
                    </form>
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>

    @section('script')
        @parent
        <script>
        function submit_click()
        {
            var pass=document.getElementById("frm_password").value;
            var pass1=document.getElementById("frm_re_password").value;
            if(pass!=pass1)
            {
                alert('Mật khẩu không trùng khớp');
                return fasle;
            }
            else
            {
                return true;
            }
        }
        </script>
    @endsection
      
@include('quan_tri/footer')