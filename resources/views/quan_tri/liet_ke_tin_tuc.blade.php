@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row">
                <h1 style="color: red; text-align:center">Quản lý tin tức</h1>
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Mã tin tức</th>
                            <th scope="col">Tiêu đề</th>
                            <th scope="col">Tác giả</th>
                            <th scope="col">Loại tin tức</th>
                            <th scope="col">Xóa</th>
                            <th scope="col">Sửa</th>
                          </tr>
                        </thead>
                        <tbody>
                         
                           @foreach ($dsTinTuc as $tt)
                           <tr>
                            <th scope="col">{{$tt->id}}</th>
                            <th scope="col">{{$tt->tieu_de}}</th>
                            <th scope="col">{{$tt->tac_gia}}</th>
                            <th scope="col">{{$dsLTT[$tt->id]->ten_loai}}</th>
                            <th scope="col"><a onclick="return xoa_click();" href="{{URL('tin_tuc/xoa/'.$tt->id)}}" style="color: red">Xóa</a></th>
                           <th scope="col"><a href="{{URL('tin_tuc/sua/'.$tt->id)}}" style="color: blue">Sửa</a></th>
                           </tr>  
                           @endforeach
                         
                        </tbody>
                      </table>
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>

    @section('script')
        @parent
        <script>
        function xoa_click()
        {
          if(confirm("Bấm vào nút OK để tiếp tục") == true){
            }else{
                return false; 
            }
        }
        </script>
    @endsection
      
@include('quan_tri/footer')