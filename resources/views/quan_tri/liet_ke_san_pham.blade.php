@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row">
                <h1 style="color: red; text-align:center">Quản lý sản phẩm</h1>
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Mã SP</th>
                            <th scope="col">Tên sản phẩm</th>
                            <th scope="col">Số lượng</th>
                            <th scope="col">Đơn giá</th>
                            <th scope="col">Khuyến mãi</th>
                            <th scope="col">Loại sản phẩm</th>
                            <th scope="col">Xóa</th>
                            <th scope="col">Sửa</th>
                          </tr>
                        </thead>
                        <tbody>
                         
                           @foreach ($dsSanPham as $sp)
                           <tr>
                            <th scope="col">{{$sp->ma_san_pham}}</th>
                            <th scope="col">{{$sp->ten_san_pham}}</th>
                            <th scope="col">{{$sp->so_luong}}</th>
                            <th scope="col">{{$sp->don_gia}}</th>
                           <th scope="col"><a href="{{URL('san_pham/giam_gia/'.$sp->ma_san_pham)}}" style="color: green">@if($sp->gia_sau_khi_giam!=0){{$sp->gia_sau_khi_giam}} @else Không @endif</a></th>
                            <th scope="col">{{$dsLSP[$sp->ma_san_pham]->ten_loai}}</th>
                            <th scope="col"><a onclick="return xoa_click();" href="{{URL('san_pham/xoa/'.$sp->ma_san_pham)}}" style="color: red">Xóa</a></th>
                           <th scope="col"><a href="{{URL('san_pham/sua/'.$sp->ma_san_pham)}}" style="color: blue">Sửa</a></th>
                           </tr>
                           @endforeach
                        </tbody>
                        
                      </table>
                      <div style="margin-left:48%">
                            {{$dsSanPham->links()}}
                      </div>
                      
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>

    @section('script')
        @parent
        <script>
        function xoa_click()
        {
          if(confirm("Bấm vào nút OK để tiếp tục") == true){
            }else{
                return false; 
            }
        }
        </script>
    @endsection
      
@include('quan_tri/footer')