

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                <h3>{{$so_khach_hang}}</h3>
    
                  <p>Người đăng ký</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
            <a href="{{URL('khach_hang/liet_ke')}}" class="small-box-footer">Xem chi tiết<i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                <h3>{{$so_san_pham}}</h3>
    
                  <p>Sản phẩm</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{URL('san_pham/liet_ke')}}" class="small-box-footer">Xem chi tiết <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                <h3>{{$so_tin_tuc}}</h3>
    
                  <p>Tin tức</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="{{URL('tin_tuc/liet_ke')}}" class="small-box-footer">Xem chi tiết <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                <h3>{{$so_don_hang}}</h3>
    
                  <p>Đơn hàng</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="{{URL('quan_tri/liet_ke_hoa_don_chua_giao')}}" class="small-box-footer">Xem chi tiết<i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
          </div>
          <!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-6 connectedSortable">
              <!-- Custom tabs (Charts with tabs)-->
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left header"><i class="fa fa-inbox"></i>10 sản phẩm doanh thu cao nhất tháng</li>
                </ul>
                <div class="tab-content no-padding">
                    <canvas id="chart_doanh_thu_cao_nhat" style="position: relative; height: 300px;"></canvas>
                </div>
              </div>
              <!-- /.nav-tabs-custom -->
    
              <div class="nav-tabs-custom">
                  <!-- Tabs within a box -->
                  <ul class="nav nav-tabs pull-right">
                    <li class="pull-left header"><i class="fa fa-inbox"></i>10 sản phẩm có lượt yêu thích cao nhất tháng</li>
                  </ul>
                  <div class="tab-content no-padding">
                    <!-- Morris chart - Sales -->
                    <canvas id="chart_san_pham_yeu_thich" style="position: relative; height: 300px;"></canvas>
                  </div>
                </div>
    
            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-6 connectedSortable">
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-right">
                      <li class="pull-left header"><i class="fa fa-inbox"></i>10 tin tức lượt xem cao nhất tháng</li>
                    </ul>
                    <div class="tab-content no-padding">
                      <!-- Morris chart - Sales -->
                      <canvas id="chart_tin_tuc_xem_nhieu" style="position: relative; height: 300px;"></canvas>
                    </div>
                </div>
    
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-right">
                      <li class="pull-left header"><i class="fa fa-inbox"></i>10 tin tức lượt thích cao nhất tháng</li>
                    </ul>
                    <div class="tab-content no-padding">
                      <!-- Morris chart - Sales -->
                      <canvas id="chart_tin_tuc_yeu_thich" style="position: relative; height: 300px;"></canvas>
                    </div>
                </div>
            </section>
            <!-- right col -->
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
      </div>

      
@section('script')
@parent
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
<script>
var ctx = document.getElementById('chart_doanh_thu_cao_nhat').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            @foreach($ThongKeSanPhamBanChay as $sp)
                '{{$sp->ma_san_pham}}',
            @endforeach    
        ],
        datasets: [{
            label: 'Doanh số',
            data: [
                @foreach($ThongKeSanPhamBanChay as $sp)
                '{{$sp->thanh_tien}}',
            @endforeach  
            ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        title: {
            display: true,
            text: '10 sản phẩm có doanh thu cao nhất trong tháng'
        },
        legend: {
    display: true
    },
    tooltips: {
    mode: 'label',
    label: 'Doanh thu',
    callbacks: {
        label: function(tooltipItem, data) {
            return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); }, },
    },
    scales: {
        yAxes: [{
            ticks: {
                callback: function(label, index, labels) { return label; },
                beginAtZero:true,
                fontSize: 15,
            },
            gridLines: {
                display: true
            },
            scaleLabel: { 
                display: true,
                fontSize: 15,
            }
        }],
        xAxes: [{
            ticks: {
                beginAtZero: true,
                fontSize: 15
            },
            gridLines: {
                display:true
            },
            scaleLabel: {
                display: true,
                fontSize: 15,
        }
        }]
    }

    }
});

var canvasP = document.getElementById('chart_doanh_thu_cao_nhat')
canvasP.onclick = function(e) {
    var slice = myChart.getElementAtEvent(e);
    if (!slice.length) return; // return if not clicked on slice
    var label = slice[0]._model.label;
    switch (label) {
        @foreach($ThongKeSanPhamBanChay as $sp)
            case '{{$sp->ma_san_pham}}':
            window.open('san_pham/chi_tiet/{{$sp->ma_san_pham}}');
        @endforeach    
    }
}
</script>

<script>
    var ctx = document.getElementById('chart_san_pham_yeu_thich').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                @foreach($ThongKeSanPhamYeuThich as $sp)
                    '{{$sp->ma_san_pham}}',
                @endforeach          
            ],
            datasets: [{
                label: 'Lượt yêu thích',
                data: [
                    @foreach($ThongKeSanPhamYeuThich as $sp)
                    '{{$sp->luot_thich}}',
                @endforeach  
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            title: {
                display: true,
                text: '10 sản phẩm được yêu thích nhiều nhất trong tháng'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    
    var canvasP = document.getElementById('chart_san_pham_yeu_thich')
    canvasP.onclick = function(e) {
        var slice = myChart.getElementAtEvent(e);
        if (!slice.length) return; // return if not clicked on slice
        var label = slice[0]._model.label;
        switch (label) {
            @foreach($ThongKeSanPhamYeuThich as $sp)
                case '{{$sp->ma_san_pham}}':
                window.open('san_pham/chi_tiet/{{$sp->ma_san_pham}}');
            @endforeach    
        }
    }
</script>


@section('script')
        @parent
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
        <script>
        var ctx = document.getElementById('chart_tin_tuc_xem_nhieu').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    @foreach($ThongKeTinTucXemNhieu as $tt)
                        '{{$tt->ma_tin_tuc}}',
                    @endforeach    
                ],
                datasets: [{
                    label: 'Lượt xem',
                    data: [
                        @foreach($ThongKeTinTucXemNhieu as $tt)
                        '{{$tt->luot_xem}}',
                    @endforeach  
                    ],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    display: true,
                    text: '10 tin tức có lượt xem cao nhất tháng'
                },
                legend: {
            display: true
            }
            }
        });

        var canvasP = document.getElementById('chart_tin_tuc_xem_nhieu')
        canvasP.onclick = function(e) {
            var slice = myChart.getElementAtEvent(e);
            if (!slice.length) return; // return if not clicked on slice
            var label = slice[0]._model.label;
            switch (label) {
                @foreach($ThongKeTinTucXemNhieu as $tt)
                    case '{{$tt->ma_tin_tuc}}':
                    window.open('tin_tuc/chi_tiet/{{$tt->ma_tin_tuc}}');
                @endforeach    
            }
        }
</script>

<script>
    var ctx = document.getElementById('chart_tin_tuc_yeu_thich').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                @foreach($ThongKeTinTucYeuThich as $tt)
                    '{{$tt->ma_tin_tuc}}',
                @endforeach    
            ],
            datasets: [{
                label: 'Lượt thích',
                data: [
                    @foreach($ThongKeTinTucYeuThich as $tt)
                    '{{$tt->luot_thich}}',
                @endforeach  
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            title: {
                display: true,
                text: '10 tin tức có lượt thích cao nhất tháng'
            },
            legend: {
        display: true
        }
        }
    });

    var canvasP = document.getElementById('chart_tin_tuc_yeu_thich')
    canvasP.onclick = function(e) {
        var slice = myChart.getElementAtEvent(e);
        if (!slice.length) return; // return if not clicked on slice
        var label = slice[0]._model.label;
        switch (label) {
            @foreach($ThongKeTinTucYeuThich as $tt)
                case '{{$tt->ma_tin_tuc}}':
                window.open('tin_tuc/chi_tiet/{{$tt->ma_tin_tuc}}');
            @endforeach    
        }
    }
</script>
@endsection
      