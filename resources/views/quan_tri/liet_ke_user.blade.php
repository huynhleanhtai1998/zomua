@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row">
                <h1 style="color: red; text-align:center">Quản lý user</h1>
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Họ tên</th>
                            <th scope="col">Email</th>
                            <th scope="col">Giới tính</th>
                            <th scope="col">Ngày tạo</th>
                            <th scope="col">Quản lý sản phẩm</th>
                            <th scope="col">Quản lý tin tức</th>
                            <th scope="col">Quản lý khách hàng</th>
                            <th scope="col">Quản lý hóa đơn</th>
                            <th scope="col">Quản lý user</th>
                            <th scope="col">Sửa</th>
                            <th scope="col">Xóa</th>
                          </tr>
                        </thead>
                        <tbody>
                         
                           @foreach ($dsUser as $user)
                            <tr>
                            <td scope="col">{{$user->id}}</td>
                                    <td scope="col">{{$user->ho_ten}}</td>
                                    <td scope="col">{{$user->email}}</td>
                                    <td scope="col">@if($user->gioi_tinh==0)Nam @else Nữ @endif</td>
                                    <td scope="col">{{$user->created_at}}</td>
                                    <td scope="col">@if($user->quan_ly_san_pham==0)Không @else Có @endif</td>
                                    <td scope="col">@if($user->quan_ly_tin_tuc==0)Không @else Có @endif</td>
                                    <td scope="col">@if($user->quan_ly_khach_hang==0)Không @else Có @endif</td>
                                    <td scope="col">@if($user->quan_ly_hoa_don==0)Không @else Có @endif</td>
                                    <td scope="col">@if($user->quan_ly_user==0)Không @else Có @endif</td>
                                    <td scope="col"><a href="{{URL('quan_tri/user/sua/'.$user->id)}}">Sửa</a></td>
                                    <td scope="col"><a onclick="return xoa_click();"  href="{{URL('quan_tri/user/xoa/'.$user->id)}}">Xóa</a></td>
                            </tr>  
                           @endforeach
                         
                        </tbody>
                      </table>
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>
    @section('script')
        @parent
        <script>
        function xoa_click()
        {
          if(confirm("Bấm vào nút OK để tiếp tục") == true){
            }else{
                return false; 
            }
        }
        </script>
    @endsection
      
@include('quan_tri/footer')