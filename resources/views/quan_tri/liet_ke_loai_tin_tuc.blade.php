@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row">
                <h1 style="color: red; text-align:center">Quản lý tin tức</h1>
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Mã loại</th>
                            <th scope="col">Tên loại</th>

                            <th scope="col">Xóa</th>
                            <th scope="col">Sửa</th>
                          </tr>
                        </thead>
                        <tbody>
                         
                           @foreach ($dsLoaiTinTuc as $ltt)
                           <tr>
                            <th scope="col">{{$ltt->ma_loai}}</th>
                            <th scope="col">{{$ltt->ten_loai}}</th>
                            <th scope="col"><a onclick="return xoa_click();" href="{{URL('loai_tin_tuc/xoa/'.$ltt->ma_loai)}}" style="color: red">Xóa</a></th>
                           <th scope="col"><a href="{{URL('loai_tin_tuc/sua/'.$ltt->ma_loai)}}" style="color: blue">Sửa</a></th>
                           </tr>  
                           @endforeach
                         
                        </tbody>
                      </table>
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>

    @section('script')
        @parent
        <script>
        function xoa_click()
        {
          if(confirm("Bấm vào nút OK để tiếp tục") == true){
            }else{
                return false; 
            }
        }
        </script>
    @endsection
      
@include('quan_tri/footer')