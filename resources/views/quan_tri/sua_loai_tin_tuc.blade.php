@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row" style="padding:20px">
                <h1 style="color:blue; text-align:center;">Sửa thông tin loại tin tức</h1>
                <div class="content_message">
                @if(count($errors)>0)
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{$error}}<br>
                    </div>
                @endforeach
                @endif  

                @if(session('alert'))
                    <div class="alert alert-success">
                        {{session('alert')}}<br>
                    </div>
                @endif
            </div>
                <form method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                        <label for="frm_ten_loai">Tên loại tin tức</label>
                        <input name="frm_ten_loai" type="text" class="form-control" value="{{$loai_tin_tuc->ten_loai}}" id="frm_ten_loai" placeholder="Nhập tên loại tin tức">
                        </div>
                        <button name="frm_submit" type="submit" class="btn btn-primary">Submit</button>
                    </form>
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>
      
@include('quan_tri/footer')