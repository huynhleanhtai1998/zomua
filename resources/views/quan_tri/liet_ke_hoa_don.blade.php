@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row">
              <h1 style="color: red; text-align:center">Danh sách hóa đơn</h1>
              <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">Số hóa đơn</th>
                          <th scope="col">Mã khách hàng</th>
                          <th scope="col">Tên khách hàng</th>
                          <th scope="col">Giá trị</th>
                          <th scope="col">Hình thức thanh toán</th>
                          <th scope="col">Thanh toán</th>
                          <th scope="col">Trạng thái</th>
                          <th scope="col">Chi tiết</th>
                          <th scope="col">Xóa</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                         @foreach ($dsHoaDon as $hd)
                          <tr>
                              <th scope="col">{{$hd->so_hoa_don}}</th>
                              <th scope="col">{{$hd->ma_khach_hang}}</th>
                              <th scope="col">{{$hd->ten_khach_hang}}</th>
                              <th scope="col">{{number_format($hd->tri_gia)}}</th>
                              <th scope="col">@if($hd->hinh_thuc_thanh_toan==0)Tiền mặt @else Chuyển khoản @endif</th>
                              <th scope="col">@if($hd->thanh_toan==0)Chưa thanh toán @else Đã thanh toán @endif</th>
                              <th scope="col">@if($hd->nhan_hang==1)Đã giao @elseif($hd->giao_hang==1)Đang giao @else Chưa giao @endif</th>
                              <th><a href="{{URL('khach_hang/hoa_don/'.$hd->so_hoa_don)}}" style="color:green">Chi tiết</a></th>
                              <th scope="col"><a onclick="return xoa_click();" href="{{URL('hoa_don/xoa/'.$hd->so_hoa_don)}}" style="color: red">Xóa</a></th>
                          </tr>  
                         @endforeach
                       
                      </tbody>
                    </table>
                    <div style="margin-left:48%">
                        {{$dsHoaDon->links()}}
                  </div>
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>

    @section('script')
        @parent
        <script>
        function xoa_click()
        {
          if(confirm("Bấm vào nút OK để tiếp tục") == true){
            }else{
                return false; 
            }
        }
        </script>
    @endsection
      
@include('quan_tri/footer')