@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row" style="padding:20px">
                <h1 style="color:blue; text-align:center;">Sửa thông tin đối tượng</h1>
                <div class="content_message">
                    @if(count($errors)>0)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{$error}}<br>
                        </div>
                    @endforeach
                    @endif  

                    @if(session('alert'))
                        <div class="alert alert-success">
                            {{session('alert')}}<br>
                        </div>
                    @endif
                </div>
                <form method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                        <label for="frm_ten_doi_tuong">Tên đối tượng</label>
                        <input name="frm_ten_doi_tuong" type="text" class="form-control" id="frm_ten_doi_tuong" value="{{$doi_tuong->ten_doi_tuong}}" placeholder="Nhập tên đối tượng">
                        </div>
                    
                        <div class="form-group">
                                <label for="frm_hinh_anh">Hình ảnh</label>
                                <input type="file" name="frm_hinh_anh" class="form-control" id="frm_hinh_anh">
                        </div>
                        <button name="frm_submit" type="submit" class="btn btn-primary">Submit</button>
                    </form>
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>
      
@include('quan_tri/footer')