@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row">
                <h1 style="color:blue; text-align:center;">Giảm giá</h1>
                <div class="content_message">
                    @if(count($errors)>0)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                          {{$error}}<br>
                        </div>
                    @endforeach
                  @endif  
            
                  @if(session('alert'))
                        <div class="alert alert-success">
                          {{session('alert')}}<br>
                        </div>
                  @endif
              </div>
                <form method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                          <label for="don_gia">Giá không khuyến mãi</label>
                          <input name="frm_don_gia" value="{{$san_pham->don_gia}}" type="number" class="form-control" id="don_gia" disabled="disabled">
                        </div>
                        <div class="form-group">
                            <label for="gia_khuyen_mai">Giá khuyến mãi</label>
                            <input name="frm_gia_khuyen_mai" value="{{$san_pham->gia_sau_khi_giam}}" type="number" class="form-control" id="gia_khuyen_mai">
                          </div>
                        <button name="frm_submit" type="submit" class="btn btn-primary">Submit</button>
            </form>     
          </div>
          <!-- /.row (main row) -->
        </section>
        <!-- /.content -->
    </div>
      
@include('quan_tri/footer')