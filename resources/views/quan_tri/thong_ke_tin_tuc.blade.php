@include('quan_tri/head')
@include('quan_tri/side_bar')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          
          <!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-6 connectedSortable">
              <!-- Custom tabs (Charts with tabs)-->
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left header"><i class="fa fa-inbox"></i>10 tin tức xem nhiều nhất tháng</li>
                </ul>
                <div class="tab-content no-padding">
                    <canvas id="tin_tuc_xem_nhieu" style="position: relative; height: 300px;"></canvas>
                </div>
              </div>
              <!-- /.nav-tabs-custom -->
    
              <div class="nav-tabs-custom">
                  <!-- Tabs within a box -->
                  <ul class="nav nav-tabs pull-right">
                    <li class="pull-left header"><i class="fa fa-inbox"></i>10 tin tức có lượt yêu thích cao nhất tháng</li>
                  </ul>
                  <div class="tab-content no-padding">
                    <!-- Morris chart - Sales -->
                    <canvas id="tin_tuc_yeu_thich" style="position: relative; height: 300px;"></canvas>
                  </div>
                </div>
    
            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-6 connectedSortable">
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-right">
                      <li class="pull-left header"><i class="fa fa-inbox"></i>10 tin tức nhiều bình luận nhất tháng</li>
                    </ul>
                    <div class="tab-content no-padding">
                      <!-- Morris chart - Sales -->
                      <canvas id="tin_tuc_nhieu_binh_luan" style="position: relative; height: 300px;"></canvas>
                    </div>
                </div>

            </section>
            <!-- right col -->
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
      </div>

      
@section('script')
@parent
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
<script>
        var ctx = document.getElementById('tin_tuc_nhieu_binh_luan').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    @foreach($ThongKeTinTucNhieuBinhLuan as $tt)
                        '{{$tt->ma_tin_tuc}}',
                    @endforeach    
                ],
                datasets: [{
                    label: 'Lượt bình luận',
                    data: [
                        @foreach($ThongKeTinTucNhieuBinhLuan as $tt)
                        '{{$tt->binh_luan}}',
                    @endforeach  
                    ],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    display: true,
                    text: '10 tin tức có lượt bình luận cao nhất tháng'
                },
                legend: {
            display: true
            }
            }
        });

        var canvasP = document.getElementById('tin_tuc_nhieu_binh_luan')
        canvasP.onclick = function(e) {
            var slice = myChart.getElementAtEvent(e);
            if (!slice.length) return; // return if not clicked on slice
            var label = slice[0]._model.label;
            switch (label) {
                @foreach($ThongKeTinTucNhieuBinhLuan as $tt)
                    case '{{$tt->ma_tin_tuc}}':
                    window.open('tin_tuc/chi_tiet/{{$tt->ma_tin_tuc}}');
                @endforeach    
            }
        }
</script>

<script>
        var ctx = document.getElementById('tin_tuc_xem_nhieu').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    @foreach($ThongKeTinTucXemNhieu as $tt)
                        '{{$tt->ma_tin_tuc}}',
                    @endforeach    
                ],
                datasets: [{
                    label: 'Lượt xem',
                    data: [
                        @foreach($ThongKeTinTucXemNhieu as $tt)
                        '{{$tt->luot_xem}}',
                    @endforeach  
                    ],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    display: true,
                    text: '10 tin tức có lượt xem cao nhất tháng'
                },
                legend: {
            display: true
            }
            }
        });

        var canvasP = document.getElementById('tin_tuc_xem_nhieu')
        canvasP.onclick = function(e) {
            var slice = myChart.getElementAtEvent(e);
            if (!slice.length) return; // return if not clicked on slice
            var label = slice[0]._model.label;
            switch (label) {
                @foreach($ThongKeTinTucXemNhieu as $tt)
                    case '{{$tt->ma_tin_tuc}}':
                    window.open('tin_tuc/chi_tiet/{{$tt->ma_tin_tuc}}');
                @endforeach    
            }
        }
</script>
<script>
        var ctx = document.getElementById('tin_tuc_yeu_thich').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    @foreach($ThongKeTinTucYeuThich as $tt)
                        '{{$tt->ma_tin_tuc}}',
                    @endforeach    
                ],
                datasets: [{
                    label: 'Lượt thích',
                    data: [
                        @foreach($ThongKeTinTucYeuThich as $tt)
                        '{{$tt->luot_thich}}',
                    @endforeach  
                    ],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
                    display: true,
                    text: '10 tin tức có lượt thích cao nhất tháng'
                },
                legend: {
            display: true
            }
            }
        });

        var canvasP = document.getElementById('tin_tuc_yeu_thich')
        canvasP.onclick = function(e) {
            var slice = myChart.getElementAtEvent(e);
            if (!slice.length) return; // return if not clicked on slice
            var label = slice[0]._model.label;
            switch (label) {
                @foreach($ThongKeTinTucYeuThich as $tt)
                    case '{{$tt->ma_tin_tuc}}':
                    window.open('tin_tuc/chi_tiet/{{$tt->ma_tin_tuc}}');
                @endforeach    
            }
        }
</script>
@endsection
      
@include('quan_tri/footer')