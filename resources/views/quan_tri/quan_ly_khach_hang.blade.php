@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row" style="padding: 20px">
                <h1 style="color: red; text-align:center">Quản lý khách hàng</h1>
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Mã khách hàng</th>
                            <th scope="col">Tên khách hàng</th>
                            <th scope="col">Email</th>
                            <th scope="col">Giới tính</th>
                            <th scope="col">Địa chỉ</th>
                          </tr>
                        </thead>
                        <tbody>
                         
                           @foreach ($dsKhachHang as $kh)
                            <tr>
                                <th scope="col">{{$kh->ma_khach_hang}}</th>
                                <th scope="col">{{$kh->ten_khach_hang}}</th>
                                <th scope="col">{{$kh->email}}</th>
                                <th scope="col">@if($kh->phai==0)Nam @else Nữ @endif</th>
                                <th scope="col">{{$kh->dia_chi}}</th>
                            </tr>  
                           @endforeach
                         
                        </tbody>
                      </table>
                      <div style="margin-left:48%">
                          {{$dsKhachHang->links()}}
                    </div>
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>

    @section('script')
        @parent
        <script>
        function xoa_click()
        {
          if(confirm("Bấm vào nút OK để tiếp tục") == true){
            }else{
                return false; 
            }
        }
        </script>
    @endsection
      
@include('quan_tri/footer')