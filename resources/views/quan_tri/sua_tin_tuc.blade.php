@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row" style="padding:20px">
                <h1 style="color:blue; text-align:center;">Sửa nội dung tin tức</h1>
    <div class="content_message">
        @if(count($errors)>0)
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">
              {{$error}}<br>
            </div>
        @endforeach
      @endif  

      @if(session('alert'))
            <div class="alert alert-success">
              {{session('alert')}}<br>
            </div>
      @endif
  </div>
    <form method="post" enctype="multipart/form-data">
        @csrf
            <div class="form-group">
              <label for="frm_tieu_de">Tiêu đề</label>
            <input name="frm_tieu_de" value="{{$tin_tuc->tieu_de}}" type="text" class="form-control" id="frm_tieu_de" placeholder="Nhập tiêu để của tin tức">
            </div>
            <div class="form-group">
                <label for="frm_tom_tat">Tóm tắt</label>
                <input name="frm_tom_tat" value="{{$tin_tuc->tom_tat}}" type="text" class="form-control" id="frm_tom_tat" placeholder="Tóm tắt">
            </div>
            <div class="form-group">
                    <label for="frm_noi_dung">Nội dung</label>
                    <textarea class="ckeditor" id="frm_noi_dung" rows="10" name="frm_noi_dung">value="{{$tin_tuc->noi_dung}}"</textarea>
            </div>
            <div class="form-group">
                    <label for="frm_hinh_anh">Hình ảnh</label>
                    <input type="file" name="frm_hinh_anh" class="form-control" id="frm_hinh_anh">
            </div>
            <div class="form-group">
                <label>Loại tin tức</label>
                <select name="frm_ma_loai">
                    @foreach ($dsLoaiTinTuc as $ltt)
                    <option @if($tin_tuc->ma_loai==$ltt->ma_loai) selected="selected" @endif value="{{$ltt->ma_loai}}">{{$ltt->ten_loai}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                    <label for="frm_tac_gia">Tác giả</label>
                    <input type="text" name="frm_tac_gia" value="{{$tin_tuc->tac_gia}}" class="form-control" id="frm_tac_gia" placeholder="Nhập đơn giá sản phẩm">
            </div>

            <button name="frm_submit" type="submit" class="btn btn-primary">Submit</button>
          </form>
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>
      
@include('quan_tri/footer')