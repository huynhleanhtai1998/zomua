@include('quan_tri/head')
@include('quan_tri/side_bar')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Quản lý website
          </h1>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row" style="padding:20px">
                <h1 style="color:blue; text-align:center;">Thêm sản phẩm mới</h1>
                <div class="content_message">
                    @if(count($errors)>0)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                        {{$error}}<br>
                        </div>
                    @endforeach
                @endif  

                @if(session('alert'))
                        <div class="alert alert-success">
                        {{session('alert')}}<br>
                        </div>
                @endif
            </div>
                <form method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                        <label for="frm_ten_san_pham">Tên sản phẩm</label>
                        <input name="frm_ten_san_pham" value="{{old('frm_ten_san_pham')}}" type="text" class="form-control" id="frm_ten_san_pham" placeholder="Nhập tên sản phẩm">
                        </div>
                        <div class="form-group">
                            <label>Loại sản phẩm</label>
                            <select name="frm_loai_san_pham">
                                @foreach ($dsLoaiSanPham as $lsp)
                                    @foreach ($lsp as $item)
                                        <option value="{{$item->ma_loai}}">{{$item->ten_loai}}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                                <label for="frm_mo_ta">Mô tả</label>
                                <textarea class="ckeditor" id="frm_mo_ta_tom_tat" rows="5" name="frm_mo_ta">{{old('frm_mo_ta_tom_tat')}}</textarea>
                        </div>

                        <div class="form-group">
                                <label for="frm_don_gia">Đơn giá</label>
                                <input type="number" name="frm_don_gia" value="{{old('frm_don_gia')}}" class="form-control" id="frm_don_gia" placeholder="Nhập đơn giá sản phẩm">
                        </div>

                        <div class="form-group">
                            <label for="frm_don_gia">Giá khuyến mãi</label>
                            <input type="number" name="frm_khuyen_mai" value="{{old('frm_khuyen_mai')}}" class="form-control" id="frm_don_gia" placeholder="Nhập đơn giá sản phẩm">
                        </div>

                        <div class="form-group">
                            <label for="frm_don_gia">Số lượng</label>
                            <input type="number" name="frm_so_luong" value="{{old('frm_so_luongs')}}" class="form-control" id="frm_so_luong" placeholder="Nhập đơn giá sản phẩm">
                    </div>
                        <div class="form-group">
                                <label for="frm_hinh_anh">Hình ảnh chính</label>
                                <input type="file" name="frm_hinh_anh_chinh" class="form-control" id="frm_hinh_anh_chinh">
                        </div>
                        <div class="form-group">
                            <label for="frm_hinh_anh">Hình ảnh 2</label>
                            <input type="file" name="frm_hinh_anh_2" class="form-control" id="frm_hinh_anh_2">
                        </div>
                        <div class="form-group">
                            <label for="frm_hinh_anh">Hình ảnh 3</label>
                            <input type="file" name="frm_hinh_anh_3" class="form-control" id="frm_hinh_anh_3">
                        </div>
                        <div class="form-group">
                            <label for="frm_hinh_anh">Hình ảnh 4</label>
                            <input type="file" name="frm_hinh_anh_4" class="form-control" id="frm_hinh_anh_4">
                        </div>
                        <div class="form-group">
                            <label for="frm_hinh_anh">Hình ảnh 5</label>
                            <input type="file" name="frm_hinh_anh_5" class="form-control" id="frm_hinh_anh_5">
                        </div>
                        <div class="form-group">
                            <label for="frm_hinh_anh">Hình ảnh 6</label>
                            <input type="file" name="frm_hinh_anh_6" class="form-control" id="frm_hinh_anh_6">
                        </div>
                        <button name="frm_submit" type="submit" class="btn btn-primary">Submit</button>
                    </form>
          </div>
          <!-- /.row (main row) -->
    
        </section>
        <!-- /.content -->
    </div>
      
@include('quan_tri/footer')