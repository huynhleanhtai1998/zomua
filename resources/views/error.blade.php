<html>
@include('head')
<body>

<section id="aa-error">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-error-area">
            <h2>404</h2>
            <span>Xin lỗi, chúng tôi không tìm thấy trang bạn yêu cầu</span>
            <p>Vui lòng hãy kiểm tra lại đường dẫn bạn đã nhập, chúng tôi rất xin lỗi về bất tiện này!!!</p>
          <a href="{{URL('/')}}"> Quay về trang chủ</a>
          </div>
        </div>
      </div>
    </div>
  </section>

</body>
</html>
