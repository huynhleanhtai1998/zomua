<!-- footer -->
<footer id="aa-footer">
    <!-- footer bottom -->
    <div class="aa-footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-footer-top-area">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="aa-footer-widget">
                                    <h3>Mục lục</h3>
                                    <ul class="aa-footer-nav">
                                    <li><a href="{{URL('/')}}">Trang chủ</a></li>
                                        <li><a href="{{URL('san_pham/0')}}">Sản phẩm nam</a></li>
                                        <li><a href="{{URL('san_pham/1')}}">Sản phẩm nữ</a></li>
                                        <li><a href="{{URL('tin_tuc/')}}">Tin thời trang</a></li>
                                        <li><a href="{{URL('lien_he/')}}">Liên hệ</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="aa-footer-widget">
                                    <div class="aa-footer-widget">
                                        <h3>Tìm hiểu thêm</h3>
                                        <ul class="aa-footer-nav">
                                            <li><a href="" data-toggle="modal" data-target="#shipping-modal">Giao hàng</a></li>
                                            <li><a href="" data-toggle="modal" data-target="#doi_tra-modal">Đổi trả</a></li>
                                        <li><a href="{{URL('san_pham/giam_gia')}}">Giảm giá</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4 col-sm-6">
                                <div class="aa-footer-widget">
                                    <div class="aa-footer-widget">
                                        <h3>Liên hệ</h3>
                                        <address>
                      <p>137E-Nguyễn Chí Thanh-Q.5-TP.HCM</p>
                      <p><span class="fa fa-phone"></span>0345098590</p>
                      <p><span class="fa fa-envelope"></span>16521046@gm.uit.edu.vn</p>
                    </address>
                                        <div class="aa-footer-social">
                                            <a href="" data-toggle="modal" data-target="#facebook"><span class="fa fa-facebook"></span></a>
                                            <a href="#"><span class="fa fa-twitter"></span></a>
                                            <a href="#"><span class="fa fa-google-plus"></span></a>
                                            <a href="https://www.youtube.com/watch?v=6L-pwD21yb4"><span class="fa fa-youtube"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="facebook" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">                      
                    <div class="modal-body">
                            <div class="fb-page" data-href="https://www.facebook.com/Trungtamtinhockhtn/" data-tabs="timeline" data-width="450px" data-height="300px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Trungtamtinhockhtn/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Trungtamtinhockhtn/">Trung Tâm Tin Học - ĐH Khoa Học Tự Nhiên TPHCM</a></blockquote></div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
        </div>

    <div class="modal fade" id="shipping-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">                      
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2>Giao hàng miễn phí toàn quốc</h2>  
                    <p>ATShop cung cấp đến các quý vị khách hàng dịch vụ giao hàng miễn phí trên toàn quốc, với thời gian nhận hàng tối đa 3 ngày.</p>   
                    <p>Quý khách vui lòng cung cấp địa chỉ nhận hàng và số điện thoại chính xác trong lúc đặt hàng.</p> 
                    <p style="color:green">Quý khách vui lòng kiểm tra kỹ sản phẩm nhận được khi giao hàng.</p>
                    <p style="color:red">Rất cảm ơn quý khách đã quan tâm đến ATShop</p>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>

    <div class="modal fade" id="doi_tra-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">                      
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2>Hỗ trợ đổi trả sản phẩm</h2>  
                    <p>ATShop hỗ trợ quý khách hàng đổi trả sản phẩm tận nhà, hoàn tiền 100% nếu sản phẩm có sai lệch so với thông tin trên website.</p>   
                    <p>Quý khách lưu ý chỉ được đổi trả sản phẩm với trạng thái còn tem, không bị thiệt hại so với lúc nhận hàng (Không tính đến lỗi do nhà sản xuất).</p> 
                    <p style="color:green">Xin quý khách vui lòng kiểm tra kỹ sản phẩm khi nhận hàng.</p>
                    <p style="color:red">Rất cảm ơn quý khách đã quan tâm đến ATShop</p>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
    {{-- <!-- footer-bottom -->
    <div class="aa-footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-footer-bottom-area">
                        <p>Designed by <a href="http://www.markups.io/">MarkUps.io</a></p>
                        <div class="aa-footer-payment">
                            <span class="fa fa-cc-mastercard"></span>
                            <span class="fa fa-cc-visa"></span>
                            <span class="fa fa-paypal"></span>
                            <span class="fa fa-cc-discover"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- / footer --> --}}

@section('script')
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="public/source/js/bootstrap.js"></script>
<!-- SmartMenus jQuery plugin -->
<script type="text/javascript" src="public/source/js/jquery.smartmenus.js"></script>
<!-- SmartMenus jQuery Bootstrap Addon -->
<script type="text/javascript" src="public/source/js/jquery.smartmenus.bootstrap.js"></script>
<!-- To Slider JS -->
<script src="public/source/js/sequence.js"></script>
<script src="public/source/js/sequence-theme.modern-slide-in.js"></script>
<!-- Product view slider -->
<script type="text/javascript" src="public/source/js/jquery.simpleGallery.js"></script>
<script type="text/javascript" src="public/source/js/jquery.simpleLens.js"></script>
<!-- slick slider -->
<script type="text/javascript" src="public/source/js/slick.js"></script>
<!-- Price picker slider -->
<script type="text/javascript" src="public/source/js/nouislider.js"></script>
<!-- Custom js -->
<script src="public/source/js/custom.js"></script>
{{-- CKediter --}}
<script src="//cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0"></script>
@show

</body>

</html>