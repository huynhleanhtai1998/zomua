<!-- Testimonial -->
<section id="aa-testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-testimonial-area">
                    <ul class="aa-testimonial-slider">
                        @foreach ($dsDanhGia as $danh_gia)
                            <!-- single slide -->
                            <li>
                                <div class="aa-testimonial-single">
                                    <img class="aa-testimonial-img" src="public/source/img/user/@if($danh_gia->phai==0)boy.png @elseif($danh_gia->phai==1)girl.png @endif" alt="testimonial img">
                                    <span class="fa fa-quote-left aa-testimonial-quote"><p>{{$danh_gia->noi_dung}}</p></span>
                                
                                    <div class="aa-testimonial-info">
                                    <p>{{$danh_gia->ten_khach_hang}}</p>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Testimonial -->