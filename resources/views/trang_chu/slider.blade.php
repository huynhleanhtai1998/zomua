<!-- Start slider -->
<section id="aa-slider">
    <div class="aa-slider-area">
        <div id="sequence" class="seq">
            <div class="seq-screen">
                <ul class="seq-canvas">
                    @foreach ($dsSlider as $slider)
                        <!-- single slide item -->
                        <li>
                            <div class="seq-model">
                            <img data-seq src="public/source/img/slider/{{$slider->hinh_anh}}" alt="{{$slider->tieu_de}}" />
                            </div>
                            <div class="seq-title">
                                {{-- <span data-seq>Save Up to 75% Off</span> --}}
                            <h2 data-seq>{{$slider->tieu_de}}</h2>
                            <span data-seq>{{$slider->noi_dung}}</span>
                                <a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn">MUA NGAY</a>
                            </div>
                        </li>
                    @endforeach                    
                </ul>
            </div>
            <!-- slider navigation btn -->
            <fieldset class="seq-nav" aria-controls="sequence" aria-label="Slider buttons">
                <a type="button" class="seq-prev" aria-label="Previous"><span class="fa fa-angle-left"></span></a>
                <a type="button" class="seq-next" aria-label="Next"><span class="fa fa-angle-right"></span></a>
            </fieldset>
        </div>
    </div>
</section>
<!-- / slider -->