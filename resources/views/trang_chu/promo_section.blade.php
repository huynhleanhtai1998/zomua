<!-- Start Promo section -->
<section id="aa-promo">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-promo-area">
                    <div class="row">
                        <!-- promo left -->
                        <div class="col-md-5 no-padding">
                            
                                <div class="aa-promo-left">
                                    <div class="aa-promo-banner">
                                        <img src="public/source/img/doi_tuong/{{$dsDoiTuong[0]->hinh_anh}}" alt="img" style="width:100%;">
                                        <div class="aa-prom-content">
                                            <h4><a href="{{URL('san_pham/'.$dsDoiTuong[0]->id)}}"><span style="background-color: black !important; color: white !important">{{$dsDoiTuong[0]->ten_doi_tuong}}</span></a></h4>
                                        </div>
                                    </div>
                                </div>
                
                        </div>
                            <!-- promo right -->
                            <div class="col-md-7 no-padding">
                                <div class="aa-promo-right">
                                    @foreach ($dsDoiTuong as$key => $doi_tuong)
                                    @if($key!=0)                           
                                        <div class="aa-single-promo-right">
                                            <div class="aa-promo-banner">
                                                <img src="public/source/img/doi_tuong/{{$doi_tuong->hinh_anh}}" alt="img" width="100%;">
                                                <div class="aa-prom-content">
                                                    <h4 ><a href="{{URL('san_pham/'.$doi_tuong->id)}}"><span style="background-color: black !important; color: white !important">{{$doi_tuong->ten_doi_tuong}}</span></a></h4>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Promo section -->