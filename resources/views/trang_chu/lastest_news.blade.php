<!-- Latest Blog -->
<section id="aa-latest-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-latest-blog-area">
                    <h2>TIN TỨC MỚI NHẤT</h2>
                    <div class="row">

                        @foreach ($dsTinTuc as $tin_tuc)
                            <!-- single latest blog -->
                            <div class="col-md-4 col-sm-4">
                                <div class="aa-latest-blog-single">
                                    <figure class="aa-blog-img">
                                    <a href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><img src="public/source/img/tin_tuc/{{$tin_tuc->hinh_anh}}" alt="{{$tin_tuc->tieu_de}}"></a>
                                        <figcaption class="aa-blog-img-caption">
                                        <span href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><i class="fa fa-eye"></i>{{$tin_tuc->luot_xem}}</span>
                                        <a href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><i class="fa fa-thumbs-o-up"></i>{{$tin_tuc->luot_thich}}</a>
                                        <a href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><i class="fa fa-comment-o"></i>{{$tin_tuc->binh_luan}}</a>
                                        <span href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><i class="fa fa-clock-o"></i>{{$tin_tuc->ngay_cap_nhat}}</span>
                                        </figcaption>
                                    </figure>
                                    <div class="aa-blog-info">
                                        <h3 class="aa-blog-title"><a href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}">{{$tin_tuc->tieu_de}}</a></h3>
                                        <p>
                                            {{$tin_tuc->tom_tat}}
                                        </p>
                                        <a href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}" class="aa-read-mor-btn">Read more <span class="fa fa-long-arrow-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Latest Blog -->