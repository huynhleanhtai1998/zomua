<!-- popular section -->
<section id="aa-popular-category">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="aa-popular-category-area">
                        <!-- start prduct navigation -->
                        <ul class="nav nav-tabs aa-products-tab">
                            <li class="active"><a href="#popular" data-toggle="tab">Sản phẩm HOT</a></li>
                            <li><a href="#featured" data-toggle="tab">Mới nhất</a></li>
                            <li><a href="#latest" data-toggle="tab">Giảm giá</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- Start men popular category -->
                            <div class="tab-pane fade in active" id="popular">
                                <ul class="aa-product-catg aa-popular-slider">
                                    @foreach ($dsHot as $sp)
                                    <!-- start single product item -->
                                    <li>
                                        <figure>
                                        <a class="aa-product-img" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><img src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}" alt="{{$sp->ten_san_pham}}" style="width:100%; height:300px;"></a>
                                            <a class="aa-add-card-btn" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><span class="fa fa-shopping-cart"></span>Xem chi tiết</a>
                                            <figcaption>
                                                <h4 class="aa-product-title"><a href="index_detail.php">{{$sp->ten_san_pham}}</a></h4>
                                                @if($sp->gia_sau_khi_giam!=0)
                                                <span class="aa-product-price">{{number_format($sp->gia_sau_khi_giam)}}</span><span class="aa-product-price"><del>{{number_format($sp->don_gia)}}</del></span>
                                                @else
                                                <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                                                @endif
                                            </figcaption>
                                        </figure>
                                        
                                        <!-- product badge -->
                                        @if($sp->so_luong<=0)
                                        <span class="aa-badge aa-sold-out" href="#">Hết hàng</span>
                                        @elseif($sp->like>10)
                                        <span class="aa-badge aa-hot" href="#">HOT</span>
                                        @else
                                        <span class="aa-badge aa-sale" href="#">Còn hàng</span>
                                        @endif
                                    </li>
                                    @endforeach
                                </ul>
                                <a class="aa-browse-btn" href="#">Xem thêm<span class="fa fa-long-arrow-right"></span></a>
                            </div>
                            <!-- / popular product category -->

                            <!-- start featured product category -->
                            <div class="tab-pane fade" id="featured">
                                <ul class="aa-product-catg aa-featured-slider">
                                    <!-- start single product item -->
                                        @foreach ($dsNew as $sp)
                                    <!-- start single product item -->
                                    <li>
                                        <figure>
                                        <a class="aa-product-img" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><img src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}" alt="{{$sp->ten_san_pham}}" style="width:100%; height:300px;"></a>
                                            <a class="aa-add-card-btn" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><span class="fa fa-shopping-cart"></span>Xem chi tiết</a>
                                            <figcaption>
                                                <h4 class="aa-product-title"><a href="index_detail.php">{{$sp->ten_san_pham}}</a></h4>
                                                @if($sp->gia_sau_khi_giam!=0)
                                                <span class="aa-product-price">{{number_format($sp->gia_sau_khi_giam)}}</span><span class="aa-product-price"><del>{{number_format($sp->don_gia)}}</del></span>
                                                @else
                                                <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                                                @endif                                            </figcaption>
                                        </figure>
                                        
                                        <!-- product badge -->
                                        @if($sp->so_luong<=0)
                                        <span class="aa-badge aa-sold-out" href="#">Hết hàng</span>
                                        @elseif($sp->like>10)
                                        <span class="aa-badge aa-hot" href="#">HOT</span>
                                        @else
                                        <span class="aa-badge aa-sale" href="#">Còn hàng</span>
                                        @endif
                                    </li>
                                    @endforeach
                                    </li>
                                    
                                </ul>
                                <a class="aa-browse-btn" href="#">Xem thêm<span class="fa fa-long-arrow-right"></span></a>
                            </div>
                            <!-- / featured product category -->

                            <!-- start latest product category -->
                            <div class="tab-pane fade" id="latest">
                                <ul class="aa-product-catg aa-latest-slider">
                                    <!-- start single product item -->
                                    @foreach ($dsSaleOff as $sp)
                                    <!-- start single product item -->
                                    <li>
                                        <figure>
                                        <a class="aa-product-img" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><img src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}" alt="{{$sp->ten_san_pham}}" style="width:100%; height:300px;"></a>
                                            <a class="aa-add-card-btn" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><span class="fa fa-shopping-cart"></span>Xem chi tiết</a>
                                            <figcaption>
                                                <h4 class="aa-product-title"><a href="index_detail.php">{{$sp->ten_san_pham}}</a></h4>
                                                @if($sp->gia_sau_khi_giam!=0)
                                                <span class="aa-product-price">{{number_format($sp->gia_sau_khi_giam)}}</span><span class="aa-product-price"><del>{{number_format($sp->don_gia)}}</del></span>
                                                @else
                                                <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                                                @endif                                            </figcaption>
                                        </figure>
                                        
                                        <!-- product badge -->
                                        @if($sp->so_luong<=0)
                                        <span class="aa-badge aa-sold-out" href="#">Hết hàng</span>
                                        @elseif($sp->like>10)
                                        <span class="aa-badge aa-hot" href="#">HOT</span>
                                        @else
                                        <span class="aa-badge aa-sale" href="#">Còn hàng</span>
                                        @endif
                                    </li>
                                    @endforeach
                                </ul>
                                <a class="aa-browse-btn" href="#">Xem thêm<span class="fa fa-long-arrow-right"></span></a>
                            </div>
                            <!-- / latest product category -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / popular section -->