<!-- Products section -->
<section id="aa-product">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="aa-product-area">
                        <div class="aa-product-inner">
                            <!-- start prduct navigation -->
                            <ul class="nav nav-tabs aa-products-tab">
                                <li class="active"><a href="#men" data-toggle="tab">Nam</a></li>
                                <li><a href="#women" data-toggle="tab">Nữ</a></li>
                                <li><a href="#sports" data-toggle="tab">Trẻ em</a></li>
                                <li><a href="#electronics" data-toggle="tab">Thể thao</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- Start men product category -->
                                <div class="tab-pane fade in active" id="men">
                                    <ul class="aa-product-catg">
                                        @foreach ($dsSanPham[0] as $sp)
                                        <!-- start single product item -->
                                        <li class="col-md-3" style="margin-left: 4%">
                                            <figure>
                                            <a class="aa-product-img" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><img src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}" alt="{{$sp->ten_san_pham}}" style="width:250px; height:300px"></a>
                                                <a class="aa-add-card-btn" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><span class="fa fa-shopping-cart"></span>Xem chi tiết</a>
                                                <figcaption>
                                                <h4 class="aa-product-title"><a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}">{{$sp->ten_san_pham}}</a></h4>
                                                <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                                                </figcaption>
                                            </figure>
                                            
                                            <!-- product badge -->
                                            @if($sp->so_luong<=0)
                                            <span class="aa-badge aa-sold-out" href="#">Hết hàng</span>
                                            @elseif($sp->like>10)
                                            <span class="aa-badge aa-hot" href="#">HOT</span>
                                            @else
                                            <span class="aa-badge aa-sale" href="#">Còn hàng</span>
                                            @endif
                                        </li>

                                        @endforeach
                                    </ul>
                                    <a class="aa-browse-btn" href="{{URL('san_pham/'.$dsDoiTuong[0]->id)}}" style="margin-left:48%">Xem thêm <span class="fa fa-long-arrow-right"></span></a>
                                </div>
                                <!-- / men product category -->


                                <!-- start women product category -->
                                <div class="tab-pane fade" id="women">
                                        <ul class="aa-product-catg">
                                                @foreach ($dsSanPham[1] as $sp)
                                                <!-- start single product item -->
                                                <li class="col-md-3" style="margin-left: 4%">
                                                    <figure>
                                                    <a class="aa-product-img" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><img src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}" alt="{{$sp->ten_san_pham}}" style="width:250px; height:300px"></a>
                                                        <a class="aa-add-card-btn" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><span class="fa fa-shopping-cart"></span>Xem chi tiết</a>
                                                        <figcaption>
                                                        <h4 class="aa-product-title"><a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}">{{$sp->ten_san_pham}}</a></h4>
                                                        <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                                                        </figcaption>
                                                    </figure>
                                                    
                                                    <!-- product badge -->
                                                    @if($sp->so_luong<=0)
                                                    <span class="aa-badge aa-sold-out" href="#">Hết hàng</span>
                                                    @elseif($sp->like>10)
                                                    <span class="aa-badge aa-hot" href="#">HOT</span>
                                                    @else
                                                    <span class="aa-badge aa-sale" href="#">Còn hàng</span>
                                                    @endif
                                                </li>
        
                                                @endforeach
                                            </ul>
                                    <a class="aa-browse-btn" href="{{URL('san_pham/'.$dsDoiTuong[1]->id)}}">Xem thêm <span class="fa fa-long-arrow-right"></span></a>
                                </div>
                                <!-- / women product category -->
                                <!-- start sports product category -->

                                <div class="tab-pane fade" id="sports">
                                        <ul class="aa-product-catg">
                                            @foreach ($dsSanPham[2] as $sp)
                                            <!-- start single product item -->
                                            <li class="col-md-3" style="margin-left: 4%">
                                                <figure>
                                                <a class="aa-product-img" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><img src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}" alt="{{$sp->ten_san_pham}}" style="width:250px; height:300px"></a>
                                                    <a class="aa-add-card-btn" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><span class="fa fa-shopping-cart"></span>Xem chi tiết</a>
                                                    <figcaption>
                                                    <h4 class="aa-product-title"><a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}">{{$sp->ten_san_pham}}</a></h4>
                                                    <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                                                    </figcaption>
                                                </figure>
                                                
                                                <!-- product badge -->
                                                @if($sp->so_luong<=0)
                                                <span class="aa-badge aa-sold-out" href="#">Hết hàng</span>
                                                @elseif($sp->like>10)
                                                <span class="aa-badge aa-hot" href="#">HOT</span>
                                                @else
                                                <span class="aa-badge aa-sale" href="#">Còn hàng</span>
                                                @endif
                                            </li>
    
                                            @endforeach
                                        </ul>
                                        <a class="aa-browse-btn" href="{{URL('san_pham/'.$dsDoiTuong[2]->id)}}"style="margin-left:48%">Xem thêm <span class="fa fa-long-arrow-right"></span></a>
                                    </div>
                                <!-- / sports product category -->
                                <!-- start electronic product category -->

                                <div class="tab-pane fade" id="electronics">
                                        <ul class="aa-product-catg">
                                            @foreach ($dsSanPham[3] as $sp)
                                            <!-- start single product item -->
                                            <li class="col-md-3" style="margin-left: 4%">
                                                <figure>
                                                <a class="aa-product-img" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><img src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}" alt="{{$sp->ten_san_pham}}" style="width:250px; height:300px"></a>
                                                    <a class="aa-add-card-btn" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><span class="fa fa-shopping-cart"></span>Xem chi tiết</a>
                                                    <figcaption>
                                                    <h4 class="aa-product-title"><a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}">{{$sp->ten_san_pham}}</a></h4>
                                                    <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                                                    </figcaption>
                                                </figure>
                                                
                                                <!-- product badge -->
                                                @if($sp->so_luong<=0)
                                                <span class="aa-badge aa-sold-out" href="#">Hết hàng</span>
                                                @elseif($sp->like>10)
                                                <span class="aa-badge aa-hot" href="#">HOT</span>
                                                @else
                                                <span class="aa-badge aa-sale" href="#">Còn hàng</span>
                                                @endif
                                            </li>
    
                                            @endforeach
                                        </ul>
                                        <a class="aa-browse-btn" href="{{URL('san_pham/'.$dsDoiTuong[3]->id)}}">Xem thêm <span class="fa fa-long-arrow-right"></span></a>
                                    </div>
                                <!-- / electronic product category -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Products section -->