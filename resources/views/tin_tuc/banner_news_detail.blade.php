<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="public/source/img/banner_product.jpg" alt="fashion img">
    <div class="aa-catg-head-banner-area">
      <div class="container">
       <div class="aa-catg-head-banner-content">
         <ol class="breadcrumb">
           <li><a href="{{URL('/')}}">Trang chủ</a></li>   
           <li><a href="{{URL('tin_tuc/')}}">Tin tức</a></li>     
           <li><a href="{{URL('tin_tuc/loai/'.$loai_tin_tuc->ma_loai)}}">{{$loai_tin_tuc->ten_loai}}</a></li>   
           <li class="active">{{$tin_tuc->tieu_de}}</li>
         
         </ol>
       </div>
      </div>
    </div>
   </section>
  <!-- / catg header banner section -->