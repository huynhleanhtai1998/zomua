@extends('master')
@section('content')
<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="public/source/img/banner_product.jpg" alt="fashion img">
    <div class="aa-catg-head-banner-area">
      <div class="container">
       <div class="aa-catg-head-banner-content">
         <ol class="breadcrumb">
           <li><a href="{{URL('/')}}">Trang chủ</a></li>   
           <li><a href="{{URL('khach_hang/tin_tuc_yeu_thich')}}">Tin tức yêu thích</a></li>     
         </ol>
       </div>
      </div>
    </div>
   </section>
  <!-- / catg header banner section -->

  <!-- Blog Archive -->
  <section id="aa-blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-blog-archive-area aa-blog-archive-2">
            <div class="row">
              <div class="col-md-9">
                <div class="aa-blog-content">
                  <div class="row">

                    @foreach ($dsTinTuc as $tin_tuc)
                    <div class="col-md-4 col-sm-4">
                      <article class="aa-latest-blog-single">
                        <figure class="aa-blog-img">                    
                        <a href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><img alt="img" src="public/source/img/tin_tuc/{{$tin_tuc->hinh_anh}}"></a>  
                            <figcaption class="aa-blog-img-caption">
                            <span href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><i class="fa fa-eye"></i>{{$tin_tuc->luot_xem}}</span>
                            <a href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><i class="fa fa-thumbs-o-up"></i>{{$tin_tuc->luot_thich}}</a>
                            <a href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><i class="fa fa-comment-o"></i>{{$tin_tuc->binh_luan}}</a>
                            <span href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><i class="fa fa-clock-o"></i>{{$tin_tuc->ngay_dang}}</span>
                          </figcaption>                          
                        </figure>
                        <div class="aa-blog-info">
                        <h3 class="aa-blog-title"><a href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}">{{$tin_tuc->tieu_de}}</a></h3>
                        <p>{{$tin_tuc->tom_tat}}</p> 
                          <a class="aa-read-mor-btn" href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}">Xem thêm<span class="fa fa-long-arrow-right"></span></a>
                        </div>
                      </article>
                    </div>
                    @endforeach

                  </div>
                </div>
                
              </div>
              @include('tin_tuc/sidebar')
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Blog Archive -->

@endsection