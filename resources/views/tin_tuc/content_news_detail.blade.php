<!-- Blog Archive -->
<section id="aa-blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-blog-archive-area">
            <div class="row">
              <div class="col-md-9">
                <!-- Blog details -->
                <div class="aa-blog-content aa-blog-details">
                  <article class="aa-blog-content-single">                        
                  <h2>{{$tin_tuc->tieu_de}}</h2>
                     <div class="aa-article-bottom">
                      <div class="aa-post-author">
                      Tác giả <a href="{{URL('tin_tuc/tac_gia/'.$tin_tuc->id)}}">{{$tin_tuc->tac_gia}}</a>
                      </div>
                      <div class="aa-post-date">
                        {{$tin_tuc->created_at}}
                      </div>
                    </div>
                    <figure class="aa-blog-img">
                    <img style="width:100%" src="public/source/img/tin_tuc/{{$tin_tuc->hinh_anh}}" alt="fashion img">
                    </figure>
                    <div class="tin_tuc">
                    <?php
                      echo htmlspecialchars_decode($tin_tuc->noi_dung);
                    ?>
                    </div>
                    <div class="blog-single-bottom">
                      <div class="row">
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <div class="blog-single-tag">
                            <span>Thể loại:</span>
                            <a href="#">{{$loai_tin_tuc->ten_loai}}</a>
                          </div>
                          <div class="blog-single-tag">
                            @if(Session::has('khach_hang'))
                              @if($like_status==false)
                                <span id="thong_bao">Thích</span>
                                <input id="ma_khach_hang" type="hidden" value="{{Session::get('khach_hang')->ma_khach_hang}}">
                                <input id="like_status"  type="hidden" value="dislike">
                                <button name="{{$tin_tuc->id}}" id="btn_like"><img id="img_like_status" src="public/source/img/like.png"></button>
                              @else
                                <span id="thong_bao">Bỏ thích</span>
                                <input id="ma_khach_hang" type="hidden" value="{{Session::get('khach_hang')->ma_khach_hang}}">
                                <input id="like_status"  type="hidden" value="like">
                                <button name="{{$tin_tuc->id}}" id="btn_like"><img id="img_like_status" src="public/source/img/dislike.png"></button>
                              @endif
                            @else
                            <a href="" data-toggle="modal" data-target="#login-modal">Đăng nhập để thích</a>
                            @endif
                          </div>
                        </div>
                        
                      </div>
                    </div>
                   
                  </article>
                  {{-- <!-- blog navigation -->
                  <div class="aa-blog-navigation">
                    <a class="aa-blog-prev" href="#"><span class="fa fa-arrow-left"></span>Prev Post</a>
                    <a class="aa-blog-next" href="#">Next Post<span class="fa fa-arrow-right"></span></a>
                  </div> --}}
                  <!-- Blog Comment threats -->
                  <div class="aa-blog-comment-threat">
                    <h3>Comments ({{$dsComment->count()}})</h3>
                    <div class="comments">
                      <ul class="commentlist">
                        @foreach ($dsComment as $comment)
                          <li>
                            <div class="media">
                              
                              <div class="media-body">
                              <h4 class="author-name">{{$comment->tac_gia}}</h4>
                               <span class="comments-date">{{$comment->created_at}}</span>
                              <p>{{$comment->noi_dung}}</p>
                              </div>
                            </div>
                          </li> 
                        @endforeach
                        
                        
                        
                      </ul>
                    </div>
                    <div class="aa-blog-archive-pagination">
                      
                    </div>
                  </div>
                  <!-- blog comments form -->
                  <div id="respond">
                    <h3 class="reply-title">Nhập cảm nghĩ của bạn về bài viết</h3>
                    @if(Session::has('khach_hang'))
                    <form id="commentform" method="post" action="tin_tuc/comment">
                      @csrf
                    <input name="ma_tin_tuc" type="hidden" value="{{$tin_tuc->id}}">
                        <p class="comment-form-comment">
                          <label for="comment">Comment</label>
                          <textarea name="comment" cols="45" rows="8" aria-required="true" required="required"></textarea>
                        </p>
                        <p class="form-submit">
                          <input type="submit" name="submit" class="aa-browse-btn" value="Post Comment">
                        </p>        
                    </form>
                    @else
                    <a href="{{URL('khach_hang/dang_nhap')}}">Đăng nhập để comment</a>
                    @endif
                  </div>
                </div>
              </div>
              @include('tin_tuc/sidebar')
            </div>
          </div>
        </div>
      </div>
    </div>
</section>

@section('script')
        @parent
        <script type="text/javascript">
        $("#btn_like").click(function(){
            var ma_tin_tuc=$("#btn_like").attr('name');
            var ma_khach_hang=$("#ma_khach_hang").val();
            var like_status=document.getElementById("like_status").value;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url:'{{URL('tin_tuc/like')}}',
                data:{_token: '<?php echo csrf_token() ?>',ma_tin_tuc:ma_tin_tuc,ma_khach_hang:ma_khach_hang,like_status:like_status},
                success: function(data){
                    if(data.n==0)
                    {
                      document.getElementById("img_like_status").src = "public/source/img/like.png";
                      document.getElementById("like_status").value = "dislike";
                      document.getElementById("thong_bao").innerHTML="Thích";
                      
                    }
                    else    
                    {
                      document.getElementById("img_like_status").src = "public/source/img/dislike.png";
                      document.getElementById("like_status").value = "like";
                      document.getElementById("thong_bao").innerHTML="Đã thích";

                    }    
                },
                error: function(xhr,status,error){
                    alert(error);
                }

            });
        });
        </script>
@endsection