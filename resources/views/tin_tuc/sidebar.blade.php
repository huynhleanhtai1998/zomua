<div class="col-md-3">
    <aside class="aa-blog-sidebar">
      <div class="aa-sidebar-widget">
        <h3>Thể loại</h3>
        <ul class="aa-catg-nav">
          @foreach ($dsLoaiTinTuc as $loai_tin_tuc)
        <li><a href="{{URL('tin_tuc/loai/'.$loai_tin_tuc->ma_loai)}}">{{$loai_tin_tuc->ten_loai}}</a></li>
          @endforeach
        </ul>
      </div>
      {{-- <div class="aa-sidebar-widget">
        <h3>Tags</h3>
        <div class="tag-cloud">
          <a href="#">Fashion</a>
          <a href="#">Ecommerce</a>
          <a href="#">Shop</a>
          <a href="#">Hand Bag</a>
          <a href="#">Laptop</a>
          <a href="#">Head Phone</a>
          <a href="#">Pen Drive</a>
        </div>
      </div> --}}
      <div class="aa-sidebar-widget">
        <h3>Xem nhiều nhất</h3>
        <div class="aa-recently-views">
          <ul>
            @foreach ($dsHot as $tin_tuc)
              <li>
              <a class="aa-cartbox-img" href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><img src="public/source/img/tin_tuc/{{$tin_tuc->hinh_anh}}" alt="img"></a>
                <div class="aa-cartbox-info">
                <h4><a href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}">{{$tin_tuc->tieu_de}}</a></h4>
                  <p>{{$tin_tuc->ngay_dang}}</p>
                </div>                    
              </li>  
            @endforeach                       
          </ul>
        </div>                            
      </div>

      @if(Session::has('recently_viewed_news'))
      <div class="aa-sidebar-widget">
          <h3>Xem gần đây</h3>
          <div class="aa-recently-views">
            <ul>
              @foreach ($dsXemGanDay as $tin_tuc)
                <li>
                <a class="aa-cartbox-img" href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}"><img src="public/source/img/tin_tuc/{{$tin_tuc->hinh_anh}}" alt="img"></a>
                  <div class="aa-cartbox-info">
                  <h4><a href="{{URL('tin_tuc/chi_tiet/'.$tin_tuc->id)}}">{{$tin_tuc->tieu_de}}</a></h4>
                    <p>{{$tin_tuc->ngay_dang}}</p>
                  </div>                    
                </li>  
              @endforeach                       
            </ul>
          </div>                            
        </div>
        @endif
    </aside>
  </div>
</div>