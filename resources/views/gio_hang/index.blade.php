@extends('master')
@section('content')
<!-- Cart view section -->
<section id="cart-view">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="cart-view-area">
            <div class="cart-view-table">
            <form action='gio_hang/cap_nhat' method="POST">
              {{ method_field('PUT') }}
              @csrf
                <div class="table-responsive">
                   <table class="table">
                     <thead>
                       <tr>
                         <th></th>
                         <th></th>
                         <th>Sản phẩm</th>
                         <th>Size</th>
                         <th>Đơn giá</th>
                         <th>Số lượng</th>
                         <th>Thành tiền</th>
                       </tr>
                     </thead>
                     <tbody>
                       @foreach (Cart::content() as $row)
                     <input type="hidden" name="frm_rowID" value="{{$row->rowId}}"/>
                        <tr>
                        <td><a class="remove" href="{{URL('gio_hang/xoa/'.$row->rowId)}}"><fa class="fa fa-close"></fa></a></td>
                          <td><a href="#"><img src="public/source/img/san_pham/{{$row->options->ma_loai}}/{{$row->id}}/{{$row->options->hinh}}" alt="img"></a></td>
                        <td><a class="aa-cart-title" href="#">{{$row->name}}</a></td>
                        <td>{{$row->options->size}}</td>
                        <td>{{number_format($row->price)}}</td>
                        <td><input class="aa-cart-quantity" name="frm_so_luong" type="number" value="{{$row->qty}}"></td>
                        <td>{{number_format($row->price*$row->qty)}}</td>
                        </tr>
                       @endforeach
                       <tr>
                          <td colspan="7" class="aa-cart-view-bottom">
                            <input name="frm_cap_nhat" class="aa-cart-view-btn" type="submit" value="Cập nhật giỏ hàng">
                          </td>
                        </tr>
                       </tbody>
                   </table>
                 </div>
              </form>
              <!-- Cart Total view -->
              <div class="cart-view-total">
                <h4>Tổng kết</h4>
                <table class="aa-totals-table">
                  <tbody>
                    <tr>
                      <th>Tổng tiền</th>
                    <td>{{Cart::total()}}</td>
                    </tr>
                  </tbody>
                </table>
                @if(Session::has('khach_hang'))
                  <a href="{{URL('gio_hang/thanh_toan')}}" class="aa-cart-view-btn">Tiến hành thanh toán</a>       
                @else
                  <a href="" class="aa-cart-view-btn" data-toggle="modal" data-target="#login-modal">Đăng nhập để thanh toán</a>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Cart view section -->
 
@endsection