@extends('master')
@section('content')
<!-- Cart view section -->
<section id="cart-view">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="cart-view-area">
            <div class="cart-view-table">
            <form action='gio_hang/cap_nhat' method="POST">
              {{ method_field('PUT') }}
              @csrf
                <div class="table-responsive">
                   <table class="table">
                     <thead>
                       <tr>
                         <th></th>
                         <th>Sản phẩm</th>
                         <th>Size</th>
                         <th>Đơn giá</th>
                         <th>Số lượng</th>
                         <th>Thành tiền</th>
                       </tr>
                     </thead>
                     <tbody>
                       @foreach ($dsCTHD as $key => $cthd)
                        <tr>
                          <td><a href="#"><img src="public/source/img/san_pham/{{$dsSanPham[$key]->ma_loai}}/{{$dsSanPham[$key]->ma_san_pham}}/{{$dsSanPham[$key]->hinh}}" alt="img"></a></td>
                        <td><a class="aa-cart-title" href="#">{{$dsSanPham[$key]->ten_san_pham}}</a></td>
                        <td>{{$cthd->size}}</td>
                        <td>{{number_format($cthd->don_gia)}}</td>
                        <td>{{$cthd->so_luong}}</td>
                        <td>{{number_format($cthd->thanh_tien)}}</td>
                        </tr>
                       @endforeach
                       </tbody>
                   </table>
                 </div>
              </form>
              <!-- Cart Total view -->
              <div class="cart-view-total">
                <h4>Tổng kết</h4>
                <table class="aa-totals-table">
                  <tbody>
                    <tr>
                      <th>Tổng tiền</th>
                    <td>{{number_format($hoa_don->tri_gia)}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Cart view section -->
 
@endsection