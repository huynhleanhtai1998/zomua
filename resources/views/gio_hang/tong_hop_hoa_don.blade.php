@extends('master')
@section('content')
<!-- Cart view section -->
<section id="cart-view">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="cart-view-area">
            <div class="cart-view-table">
                <div class="table-responsive">
                   <table class="table">
                     <thead>
                       <tr>
                          <th>Số hóa đơn</th>
                         <th>Ngày</th>
                         <th>Trị giá</th>
                         <th>Hình thức thanh toán</th>
                         <th>Thanh toán</th>
                         <th>Trạng thái</th>
                         <th>Địa chỉ giao hàng</th>
                         <th style="color:red">Xóa</th>
                         <th></th>
                       </tr>
                     </thead>
                     <tbody>
                       @foreach ($dsHoaDon as $hoa_don)
                        <tr>
                        <td>{{$hoa_don->so_hoa_don}}</td>
                        <td>{{date_format($hoa_don->created_at,"d/m/Y")}}</td>
                        <td>@if($hoa_don->hinh_thuc_thanh_toan==0)Tiền mặt @else Chuyển khoản @endif</td>
                        <td>@if($hoa_don->thanh_toan==0)Chưa thanh toán @else Đã thanh toán @endif</td>
                        <td>@if($hoa_don->giao_hang==0)Chưa giao hàng @elseif($hoa_don->nhan_hang==0) Đang giao hàng @else Đã giao hàng @endif</td>
                        <td>{{$hoa_don->tri_gia}}</td>
                        <td>{{$hoa_don->dia_chi_giao_hang}}</td>
                        <td>@if($hoa_don->giao_hang==0)<a onclick="return xoa_click();" href="{{URL('hoa_don/xoa/'.$hoa_don->so_hoa_don)}}" style="color:red">Xóa</a>@endif</td>
                        <td><a href="{{URL('khach_hang/hoa_don/'.$hoa_don->so_hoa_don)}}">Chi tiết</a></td>
                        </tr>
                       @endforeach
                       </tbody>
                   </table>
                 </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Cart view section -->
  @section('script')
        @parent
        <script>
        function xoa_click()
        {
          if(confirm("Bấm vào nút OK để tiếp tục") == true){
            }else{
                return false; 
            }
        }
        </script>
    @endsection
 
@endsection