@extends('master')
@section('content')
<!-- Cart view section -->
<section id="checkout">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
             <div class="checkout-area">
               <form action='gio_hang/luu_hoa_don' method="POST">
                @csrf
                 <div class="row">
                   <div class="col-md-8">
                     <div class="checkout-left">
                       <div class="panel-group" id="accordion">

                         <div class="panel panel-default aa-checkout-billaddress">
                           <div class="panel-heading">
                             <h4 class="panel-title">
                               <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                 Thông tin đặt hàng
                               </a>
                             </h4>
                           </div>
                           <div id="collapseFour" class="panel-collapse collapse">
                             <div class="panel-body">
                              <div class="row">
                                 <div class="col-md-12">
                                   <div class="aa-checkout-single-bill">
                                     <label for="">Họ tên</label>
                                     <input disabled type="text" placeholder="Họ tên người nhận hàng " value="{{Session::get('khach_hang')->ten_khach_hang}}">
                                   </div>                             
                                 </div>
                               </div> 
                               
                               <div class="row">
                                 <div class="col-md-6">
                                   <div class="aa-checkout-single-bill">
                                      <label for="">Email</label>
                                     <input disabled type="email" placeholder="Địa chỉ email người nhận hàng" value="{{Session::get('khach_hang')->email}}">
                                   </div>                             
                                 </div>
                                 <div class="col-md-6">
                                   <div class="aa-checkout-single-bill">
                                      <label for="">Số điện thoại</label>
                                     <input disabled type="tel" placeholder="Số điện thoại người nhận hàng" value="{{Session::get('khach_hang')->dien_thoai}}">
                                   </div>
                                 </div>
                               </div> 
                               <div class="row">
                                  <div class="col-md-12">
                                    <div class="aa-checkout-single-bill">
                                        <label for="">Địa chỉ</label>
                                      <input name="frm_dia_chi" type="text" placeholder="Địa chỉ" value="{{Session::get('khach_hang')->dia_chi}}">
                                    </div>                             
                                  </div>
                                </div>  
                        
                             </div>
                           </div>
                         </div>


                         <div class="panel panel-default aa-checkout-billaddress">
                            <div class="panel-heading">
                              <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                  Mã giảm giá
                                </a>
                              </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse">
                              <div class="panel-body">
                               <div class="row">
                                  <div class="col-md-12">
                                    <div class="aa-checkout-single-bill">
                                      <input frm_ma_giam_gia type="text" placeholder="Nhập mã giảm giá">
                                    </div>                             
                                  </div>
                                </div>                          
                              </div>
                            </div>
                          </div>

                       </div>
                     </div>
                   </div>

                   <div class="col-md-4">
                     <div class="checkout-right">
                       <h4>Hóa đơn</h4>
                       <div class="aa-order-summary-area">
                         <table class="table table-responsive">
                           <thead>
                             <tr>
                               <th>Product</th>
                               <th style="width: 10%">Total</th>
                             </tr>
                           </thead>
                           <tbody>
                              @foreach (Cart::content() as $row)
                                <tr>
                                <td>{{$row->name}} - {{$row->options->size}} <strong> x {{$row->qty}}</strong></td>
                                  <td>{{number_format($row->qty * $row->price)}}</td>
                                </tr>
                              @endforeach
                             
                             <tr>
                                <th>Tổng tiền</th>
                                <td>{{Cart::total()}}</td>
                             </tr>
                           </tbody>
                         </table>
                       </div>
                       <h4>Phương thức thanh toán</h4>
                       <div class="aa-payment-method">                    
                         <label for="cashdelivery"><input type="radio" id="cashdelivery" name="frm_hinh_thuc" value="0" checked>Thanh toán tiền mặt khi nhận hàng</label>
                         <label for="paypal"><input type="radio" id="paypal" name="frm_hinh_thuc" value="1" >Chuyển khoảng</label>
                         <img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" border="0" alt="PayPal Acceptance Mark" style="margin-top:10px">    
                         <input name="frm_submit" type="submit" value="Đặt hàng" class="aa-browse-btn">
                        </div>
                     </div>
                   </div>
                 </div>
               </form>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- / Cart view section -->
@endsection