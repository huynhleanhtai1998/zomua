@extends('master')
@section('content')
<div class="content" style="width:80%; margin: 10px auto">
        <h1 style="color: red; text-align:center">Quản lý tin tức</h1>
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Mã loại</th>
                            <th scope="col">Tên loại</th>

                            <th scope="col">Xóa</th>
                            <th scope="col">Sửa</th>
                          </tr>
                        </thead>
                        <tbody>
                         
                           @foreach ($dsLoaiTinTuc as $ltt)
                           <tr>
                            <th scope="col">{{$ltt->ma_loai}}</th>
                            <th scope="col">{{$ltt->ten_loai}}</th>
                            <th scope="col"><a href="{{URL('loai_tin_tuc/xoa/'.$ltt->ma_loai)}}" style="color: red">Xóa</a></th>
                           <th scope="col"><a href="{{URL('loai_tin_tuc/sua/'.$ltt->ma_loai)}}" style="color: blue">Sửa</a></th>
                           </tr>  
                           @endforeach
                         
                        </tbody>
                      </table>
</div>
@endsection