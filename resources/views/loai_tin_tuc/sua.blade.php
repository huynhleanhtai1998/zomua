@extends('master')
@section('content')
<div class="container" style="padding: 20px">
    <h1 style="color:blue; text-align:center;">Sửa thông tin loại tin tức</h1>
    <div class="content_message">
      @if(count($errors)>0)
      @foreach ($errors->all() as $error)
          <div class="alert alert-danger">
            {{$error}}<br>
          </div>
      @endforeach
    @endif  

    @if(session('alert'))
          <div class="alert alert-success">
            {{session('alert')}}<br>
          </div>
    @endif
  </div>
    <form method="post" enctype="multipart/form-data">
        @csrf
            <div class="form-group">
              <label for="frm_ten_loai">Tên loại tin tức</label>
            <input name="frm_ten_loai" type="text" class="form-control" value="{{$loai_tin_tuc->ten_loai}}" id="frm_ten_loai" placeholder="Nhập tên loại tin tức">
            </div>
            <button name="frm_submit" type="submit" class="btn btn-primary">Submit</button>
          </form>
</div>
@endsection