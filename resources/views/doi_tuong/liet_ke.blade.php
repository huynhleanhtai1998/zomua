@extends('master')
@section('content')
<div class="content" style="width:80%; margin: 10px auto">
        <h1 style="color: red; text-align:center">Quản lý đối tượng</h1>
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Tên đối tượng</th>
                            <th scope="col">Xóa</th>
                            <th scope="col">Sửa</th>
                          </tr>
                        </thead>
                        <tbody>
                         
                           @foreach ($dsDoiTuong as $dt)
                            <tr>
                                <th scope="col">{{$dt->id}}</th>
                                <th scope="col">{{$dt->ten_doi_tuong}}</th>
                                <th scope="col"><a href="{{URL('doi_tuong/xoa/'.$dt->id)}}" style="color: red">Xóa</a></th>
                            <th scope="col"><a href="{{URL('doi_tuong/sua/'.$dt->id)}}" style="color: blue">Sửa</a></th>
                            </tr>  
                           @endforeach
                         
                        </tbody>
                      </table>
</div>
@endsection