@include('head')
@include('header')
@include('menu')

@yield('content')

@include('footer')