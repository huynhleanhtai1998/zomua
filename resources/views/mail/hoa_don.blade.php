<!DOCTYPE html>
<html>
<head>
<title>Thông tin hóa đơn</title>
</head>
<body>

    <div class="content" style="width:600px; margin: 10px auto">
        <h1 style="color: blue; text-align:center">Thông tin hóa đơn</h1>
        <h2>Mã hóa đơn: {{$ma_hoa_don}}</h2>
        <table border="1" style="border-collapse: collapse;width: 80%;text-align:center">
            <tr>
                <td style="border: 1px solid black;">Sản phẩm</td>
                <td style="border: 1px solid black;">Size</td>
                <td style="border: 1px solid black;">Số lượng</td>
                <td style="border: 1px solid black;">Đơn giá</td>
                <td style="border: 1px solid black;">Thành tiền</td>
            </tr>

            @foreach (Cart::content() as $row)
                <input type="hidden" name="frm_rowID" value="{{$row->rowId}}"/>
                <tr>
                    <td style="border: 1px solid black;">{{$row->name}}</td>
                    <td style="border: 1px solid black;">{{$row->options->size}}</td>
                    <td style="border: 1px solid black;">{{$row->qty}}</td>
                    <td style="border: 1px solid black;">{{$row->price}}</td>
                    <td style="border: 1px solid black;">{{number_format($row->price*$row->qty)}}</td>
                </tr>
            @endforeach

            <tr>
                <td style="border: 1px solid black;" colspan="2">Tổng cộng</td>
            <td style="border: 1px solid black;" colspan="3">{{Cart::total()}}</td>
            </tr>
        </table>
    </div>

</body>
</html>