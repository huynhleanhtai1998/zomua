@extends('master')
@section('content')
<div class="container" style="padding:20px">
    <h1 style="color:blue; text-align:center;">Thêm loại sản phẩm mới</h1>
    <div class="content_message">
      @if(count($errors)>0)
      @foreach ($errors->all() as $error)
          <div class="alert alert-danger">
            {{$error}}<br>
          </div>
      @endforeach
    @endif  

    @if(session('alert'))
          <div class="alert alert-success">
            {{session('alert')}}<br>
          </div>
    @endif
  </div>
    <form method="post" enctype="multipart/form-data">
        @csrf
            <div class="form-group">
              <label for="frm_ten_loai">Tên loại sản phẩm</label>
              <input name="frm_ten_loai" type="text" class="form-control" id="frm_ten_loai" value="{{old('frm_ten_loai')}}" placeholder="Nhập tên loại sản phẩm">
            </div>
        
            <div class="form-group">
                    <label>Đối tượng</label>
                    <select name="frm_doi_tuong">
                        @foreach ($dsDoiTuong as $dt)
                        <option value="{{$dt->id}}" @if(old('frm_doi_tuong')==$dt->id)selected="selected" @endif>{{$dt->ten_doi_tuong}}</option>
                        @endforeach
                    </select>
                </div>
            <button name="frm_submit" type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
@endsection