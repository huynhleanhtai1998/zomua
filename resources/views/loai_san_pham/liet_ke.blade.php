@extends('master')
@section('content')
<div class="content" style="width:80%; margin: 10px auto">
        <h1 style="color: red; text-align:center">Quản lý loại sản phẩm</h1>
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Mã loại</th>
                            <th scope="col">Tên loại</th>
                            <th scope="col">Đối tượng</th>
                            <th scope="col">Xóa</th>
                            <th scope="col">Sửa</th>
                          </tr>
                        </thead>
                        <tbody>
                         
                           @foreach ($dsDoiTuong as $dt)
                            @foreach ($dsLoaiSanPham[$dt->id] as $lsp)
                            <tr>
                                <th scope="col">{{$lsp->ma_loai}}</th>
                                <th scope="col">{{$lsp->ten_loai}}</th>
                                <th scope="col">{{$dt->ten_doi_tuong}}</th>
                                <th scope="col"><a href="{{URL('loai_san_pham/xoa/'.$lsp->ma_loai)}}" style="color: red">Xóa</a></th>
                            <th scope="col"><a href="{{URL('loai_san_pham/sua/'.$lsp->ma_loai)}}" style="color: blue">Sửa</a></th>
                            </tr>  
                            @endforeach  
                           @endforeach
                         
                        </tbody>
                      </table>
</div>
@endsection