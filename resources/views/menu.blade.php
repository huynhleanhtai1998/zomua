<!-- menu -->
<section id="menu">
    <div class="container">
        <div class="menu-area">
            <!-- Navbar -->
            <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
                </div>
                <div class="navbar-collapse collapse">
                    <!-- Left nav -->
                    <ul class="nav navbar-nav">
                    <li><a href="{{URL('san_pham/')}}">{{ __('home') }}</a></li>
                        <li><a href="{{URL('san_pham/giam_gia')}}">{{ __('sale') }}</a></li>
                        @foreach ($dsDoiTuong as $key => $dt)
                            @if($key<5)
                            <li><a href="{{URL('san_pham/'.$dt->id)}}">{{ __($dt->ten_doi_tuong) }}<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    @foreach ($dsLoaiSanPham[$dt->id] as $lsp)
                                    <li><a href="{{URL('san_pham/loai/'.$lsp->ma_loai)}}">{{$lsp->ten_loai}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            @endif
                        @endforeach

                        @if($dsDoiTuong->count()>5)
                            <li><a>Khác<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    @foreach ($dsDoiTuong as $key => $dt)
                                        @if($key>=5)
                                        <li><a href="{{URL('san_pham/'.$dt->id)}}">{{$dt->ten_doi_tuong}}</span></a>
                                                @foreach ($dsLoaiSanPham[$dt->id] as $lsp)
                                                <li><a href="{{URL('san_pham/loai/'.$lsp->ma_loai)}}">{{$lsp->ten_loai}}</a></li>
                                                @endforeach
                                        </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                        <li><a href="{{URL('tin_tuc/')}}">{{ __('news') }}</a></li>
                        <li><a href="{{URL('lien_he/')}}">{{ __('Liên hệ') }}</a></li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
</section>
<!-- / menu -->