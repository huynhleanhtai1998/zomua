@extends('master')
@section('content')
<!-- catg header banner section -->
       <!-- / catg header banner section -->
     <!-- start contact section -->
      <section id="aa-contact">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="aa-contact-area">
                <div class="aa-contact-top">
                  <h2>Liên hệ với chúng tôi</h2>
                  <p>137E-Nguyễn Chí Thanh-Q.5-TP.HCM</p>
                </div>
                <!-- contact map -->
                <div class="aa-contact-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1589.5872544052718!2d106.66574349967212!3d10.75920730137543!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ee53a91b957%3A0x394068fd61e4fbeb!2zMTM3RSBOZ3V54buFbiBDaMOtIFRoYW5oLCBQaMaw4budbmcgOSwgUXXhuq1uIDUsIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1566311211439!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <!-- Contact address -->
                <div class="aa-contact-address">
                  <div class="row">
                    <div class="col-md-8">
                      <div class="aa-contact-address-left">
                        <form class="comments-form contact-form" action="khach_hang/danh_gia" method="post">
                          @csrf             
                          <h1>Đánh giá ATShop</h1>
                          <div class="form-group">                        
                            <textarea name="noi_dung" class="form-control" rows="3" placeholder="Nội dung đánh giá"></textarea>
                          </div>
                          @if(Session::has('khach_hang'))
                          <button class="aa-secondary-btn">Gửi</button>
                          @else
                          <a href="" data-toggle="modal" data-target="#login-modal">Đăng nhập để đánh giá</a>
                          @endif
                       </form>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="aa-contact-address-right">
                        <address>
                          <h4>ATShop</h4>
                          <p>Trang thương mại điện tử chuyên cung cấp các mặt hàng thời trang nam, nữ, trẻ em...</p>
                          <p><span class="fa fa-home"></span>137E-Nguyễn Chí Thanh-Q.5-TP.HCM</p>
                          <p><span class="fa fa-phone"></span>0345098590</p>
                          <p><span class="fa fa-envelope"></span>Email: 16521046@gm.uit.edu.vn</p>
                        </address>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
     
       
@endsection