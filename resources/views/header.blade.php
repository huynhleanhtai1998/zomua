<body>
    <!-- wpf loader Two -->
    <div id="wpf-loader-two">
        <div class="wpf-loader-two-inner">
            <span>Đang tải</span>
        </div>
    </div>
    <!-- / wpf loader Two -->
    <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
    <!-- END SCROLL TOP BUTTON -->


    <!-- Start header section -->
    <header id="aa-header" >
        <!-- start header top  -->
        <div class="aa-header-top">
            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="aa-header-top-area">
                            <!-- start header top left -->
                            <div class="aa-header-top-left">
                                    <div class="aa-language">
                                            <div class="dropdown">
                                            <a class="btn dropdown-toggle" href="{{URL('change_language/en')}}" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                @if(Lang::locale()=='vi')
                                                <img src="public/source/img/flag/vietnam.png" alt="vietnam flag">VIETNAMESE
                                                @else
                                                <img src="public/source/img/flag/english.jpg" alt="english flag">ENGLISH
                                                @endif
                                              </a>
                                              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                              <li><a href="{{URL('change_language/vi')}}"><img src="public/source/img/flag/vietnam.png" alt="">VIETNAMESE</a></li>
                                                <li><a href="{{URL('change_language/en')}}"><img src="public/source/img/flag/english.jpg" alt="">ENGLISH</a></li>
                                              </ul>
                                            </div>
                                          </div>
                                <!-- start cellphone -->
                                <div class="cellphone hidden-xs">
                                    <p><span class="fa fa-phone"></span>0345098590</p>
                                </div>
                                <!-- / cellphone -->
                                <!-- start email -->
                                <div class="cellphone hidden-xs">
                                    <p><span class="fa"></span>16521046@gm.uit.edu.vn</p>
                                </div>
                                <!-- / email -->
                            </div>
                    
                            <!-- / header top left -->
                            <div class="aa-header-top-right">
                                <ul class="aa-head-top-nav-right">
                                    <li class="hidden-xs"><a href="{{URL('gio_hang/')}}">{{ __('cart') }}</a></li>
                                    @if(Session::has('khach_hang'))
                                    <li class="hidden-xs"><a href="{{URL('gio_hang/thanh_toan')}}">{{ __('check out') }}</a></li>
                                    <li><a href="" data-toggle="modal" data-target="#login-modal">{{Session::get('khach_hang')->ten_khach_hang}}<img src="public/source/img/user/user.png"></a></li>
                                    @else
                                    <li><a href="" data-toggle="modal" data-target="#login-modal">{{ __('login') }}</a></li>
                                    @endif
                                </ul>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / header top  -->

        <!-- start header bottom  -->
        <div class="aa-header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="aa-header-bottom-area">
                            <!-- logo  -->
                            <div class="aa-logo">
                                    <!-- Text based logo -->
                                    <a href="{{URL('san_pham/')}}">
                                        <span class="fa fa-shopping-cart"></span>
                                        <p>AnhTai<strong>Fashion</strong> <span>Thời trang của mọi nhà</span></p>
                                    </a>
                                    <!-- img based logo -->
                                    <!-- <a href="index.html"><img src="img/logo.jpg" alt="logo img"></a> -->
                                </div>
                            <!-- / logo  -->

                            <!-- cart box (BUNG RA KHI RÊ CHUỘT VÀO GIỎ HÀNG) -->
                            <div class="aa-cartbox">

                                <a class="aa-cart-link" href="{{URL('gio_hang/')}}">
                                    <span class="fa fa-shopping-basket"></span>
                                    <span class="aa-cart-title">{{ __('cart') }}</span>
                                <span class="aa-cart-notify">{{Cart::count()}}</span>
                                </a>
                                <div class="aa-cartbox-summary">
                                    <ul>
                                        @foreach (Cart::content() as $row)
                                        <li>
                                                <a class="aa-cartbox-img" href="#"><img src="public/source/img/san_pham/{{$row->options->ma_loai}}/{{$row->id}}/{{$row->options->hinh}}" alt="img"></a>
                                                <div class="aa-cartbox-info">
                                                    <h4><a href="{{URL('san_pham/chi_tiet/'.$row->id)}}">{{$row->name}}</a></h4>
                                                    <p>{{$row->qty}} x {{number_format($row->price)}}</p>
                                                </div>
                                                <a class="aa-remove-product" href="{{URL('gio_hang/xoa/'.$row->rowId)}}"><span class="fa fa-times"></span></a>
                                            </li>
                                        @endforeach
                                        
                                        <li>
                                            <span class="aa-cartbox-total-title">
                        Tổng cộng
                      </span>
                                            <span class="aa-cartbox-total-price">
                        {{Cart::total()}}
                      </span>
                                        </li>
                                    </ul>
                                    @if(Session::has('khach_hang'))
                                    <a class="aa-cartbox-checkout aa-primary-btn" href="{{URL('gio_hang/thanh_toan')}}">{{ __('check out') }}</a>
                                    @else
                                    <a class="aa-cartbox-checkout aa-primary-btn" data-toggle="modal" data-target="#login-modal">Đăng nhập để đặt hàng</a>
                                    @endif
                                    
                                </div>
                            </div>
                            <!-- / cart box -->


                            <!-- search box -->
                            <div class="aa-search-box">
                                <form action="san_pham/tim_kiem" method="GET">
                                    @csrf
                                    <input name="tu_khoa" type="text" placeholder="{{ __('find') }}">
                                    <button type="submit"><span class="fa fa-search"></span></button>
                                </form>
                            </div>
                            <!-- / search box -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / header bottom  -->
        @if(Session::has('khach_hang'))
        <!-- Login Modal -->  
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">                      
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>Xin chào {{Session::get('khach_hang')->ten_khach_hang}}</h4>
                        <div class="aa-register-now">
                            <a href="" data-toggle="modal" data-target="#chinh_sua_thong_tin-modal">Xem và chỉnh sửa thông tin cá nhân </a>
                        </div>
                        <div class="aa-register-now">
                                <a href="{{URL('khach_hang/san_pham_yeu_thich')}}">Xem danh mục sản phẩm yêu thích </a>
                        </div>
                        <div class="aa-register-now">
                                <a href="{{URL('khach_hang/tin_tuc_yeu_thich')}}">Xem danh mục tin tức yêu thích</a>
                        </div>
                        <div class="aa-register-now">
                                <a href="{{URL('khach_hang/tong_hop_hoa_don')}}">Xem danh sách các hóa đơn của bạn</a>
                        </div>
                        <div class="aa-register-now">
                        Bạn muốn <a href="{{URL('khach_hang/dang_xuat')}}">Đăng xuất</a>
                        </div>
                    </div>                        
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="chinh_sua_thong_tin-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">                      
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>Vui lòng điền thông tin đăng ký</h4>
                        <div class="aa-login-form">
                                <label for="">Họ và tên<span>*</span></label>
                        <input id="frm_ho_ten" name="frm_ho_ten" type="text" value="{{Session()->get('khach_hang')->ten_khach_hang}}" placeholder="Nhập họ tên">
                                <label for="">Địa chỉ email<span>*</span></label>
                                <input id="frm_email_dang_ky" name="frm_email" value="{{Session()->get('khach_hang')->email}}" type="text" placeholder="Nhập email">
                                <label for="">Giới tính<span>*</span></label>
                                <input name="frm_gioi_tinh" type="radio" value="0" @if(Session()->get('khach_hang')->phai==0) selected="selected" @endif> Nam
                                <input name="frm_gioi_tinh" type="radio" value="1" @if(Session()->get('khach_hang')->phai==1) selected="selected" @endif> Nữ<br>
                                <label for="">Điện thoại<span>*</span></label>
                                <input id="frm_dien_thoai" name="frm_dien_thoai" type="number" value="{{Session()->get('khach_hang')->dien_thoai}}" placeholder="Nhập số điện thoại"><br>
                                <label for="">Ngày sinh<span>*</span></label>
                                <input id="frm_ngay_sinh" name="frm_ngay_sinh" type="date" value="{{Session()->get('khach_hang')->ngay_sinh}}" placeholder="Nhập ngày sinh"><br>
                                <label for="">Địa chỉ<span>*</span></label>
                                <input id="frm_dia_chi" name="frm_dia_chi" type="text" value="{{Session()->get('khach_hang')->dia_chi}}" placeholder="Nhập địa chỉ">
                                <label for="">Mật khẩu<span>*</span></label>
                                <input id="frm_password_dang_ky" name="frm_password" type="password"  placeholder="Password" value="{{Session()->get('khach_hang')->mat_khau}}">
                                <label for="">Nhập lại mật khẩu<span>*</span></label>
                                <input id="frm_nhap_lai_password" type="password" placeholder="Password" value="{{Session()->get('khach_hang')->mat_khau}}">
                                <button id="frm_cap_nhat_submit" name="frm_submit" type="submit" class="aa-browse-btn">Cập nhật thông tin</button>        
                        </div>
                    </div>                        
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
      @else
      <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">                      
                <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Vui lòng đăng nhập</h4>
                <div class="aa-login-form">
                       <label for="">Địa chỉ email<span>*</span></label>
                        <input id="frm_email" name="frm_email" type="text" placeholder="Nhập email" 
                        @if(\Illuminate\Support\Facades\Cookie::has('login_email'))
                        value="{{\Illuminate\Support\Facades\Cookie::get('login_email')}} " @endif>
                        
                        <label for="">Mật khẩu<span>*</span></label>
                         <input id="frm_password" name="frm_password" type="password" placeholder="Password" 
                         @if(\Illuminate\Support\Facades\Cookie::has('login_pass'))
                         value="{{\Illuminate\Support\Facades\Cookie::get('login_pass')}} " @endif>

                         <button id="frm_submit" name="frm_submit" type="submit" class="aa-browse-btn">Đăng nhập</button>
                         <label class="rememberme" for="rememberme">
                        <input class="frm_ghi_nho" name="frm_ghi_nho"  type="checkbox" id="rememberme" value="1"> Ghi nhớ </label>
                      <p class="aa-lost-password"><a href="" data-toggle="modal" data-target="#dang_ky-modal">Đăng ký</a></p>
                </div>
                </div>                        
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
            </div>

            <div class="modal fade" id="dang_ky-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">                      
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>Vui lòng điền thông tin đăng ký</h4>
                    <div class="aa-login-form">
                            <label for="">Họ và tên<span>*</span></label>
                            <input id="frm_ho_ten" name="frm_ho_ten" type="text" placeholder="Nhập họ tên">
                            <label for="">Địa chỉ email<span>*</span></label>
                            <input id="frm_email_dang_ky" name="frm_email" type="text" placeholder="Nhập email">
                            <label for="">Giới tính<span>*</span></label>
                            <input name="frm_gioi_tinh" type="radio" value="0" selected="selected"> Nam
                            <input name="frm_gioi_tinh" type="radio" value="1"> Nữ<br>
                            <label for="">Điện thoại<span>*</span></label>
                            <input id="frm_dien_thoai" name="frm_dien_thoai" type="number" placeholder="Nhập số điện thoại"><br>
                            <label for="">Ngày sinh<span>*</span></label>
                            <input id="frm_ngay_sinh" name="frm_ngay_sinh" type="date" placeholder="Nhập ngày sinh"><br>
                            <label for="">Địa chỉ<span>*</span></label>
                            <input id="frm_dia_chi" name="frm_dia_chi" type="text" placeholder="Nhập địa chỉ">
                            <label for="">Mật khẩu<span>*</span></label>
                            <input id="frm_password_dang_ky" name="frm_password" type="password" placeholder="Password">
                            <label for="">Nhập lại mật khẩu<span>*</span></label>
                            <input id="frm_nhap_lai_password" type="password" placeholder="Password">
                            <button id="frm_dang_ky_submit" name="frm_submit" type="submit" class="aa-browse-btn">Tạo tài khoản</button>        
                    </div>
                    </div>                        
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
      @endif

    </header>
    @section('script')
        @parent
        <script type="text/javascript">
        $("#frm_submit").click(function(){
            var mail=$("#frm_email").val();
            var pass=$("#frm_password").val();
            var remember = $('.frm_ghi_nho:checked').val();
            if(mail=="")
            {
                alert('Chưa nhập email');
                return false;
            }
            else{
                if(pass=="")
                {
                    alert('Chưa nhập password');
                    return false;
                }
            }
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url:'{{URL('khach_hang/dang_nhap')}}',
                data:{_token: '<?php echo csrf_token() ?>',frm_remember:remember,frm_email:mail,frm_password:pass},
                success: function(data){
                    if(data.n==1)
                    location.reload();
                },
                error: function(xhr,status,error){
                    alert('Đăng nhập không thành công');
                }
            });
        });

        $("#frm_dang_ky_submit").click(function(){
            var pass1=$("#frm_password_dang_ky").val();
            var pass2=$("#frm_nhap_lai_password").val();
            if(pass1!=pass2||pass1=="")
            { 
                alert('Password không trùng khớp');
              document.getElementById("frm_password").value=""; 
              document.getElementById("frm_nhap_lai_password").value=""; 
                document.getElementById("frm_nhap_lai_password").focus(); 
                return false;
            }
            else
            {
                var ho_ten=$("#frm_ho_ten").val();
                var email=$("#frm_email_dang_ky").val();
                
                if(email=="")
                {
                    alert('Chưa nhập email');
                    return false;
                }
                var gioi_tinh= $("input[name='frm_gioi_tinh']").val();
                var dien_thoai=$("#frm_dien_thoai").val();
                var ngay_sinh=$("#frm_ngay_sinh").val();
                var dia_chi=$("#frm_dia_chi").val();
                $.ajax({
                type: 'POST',
                dataType: 'json',
                url:'{{URL('khach_hang/dang_ky')}}',
                data:{_token: '<?php echo csrf_token() ?>',gioi_tinh:gioi_tinh,ho_ten:ho_ten,email:email,dien_thoai:dien_thoai,ngay_sinh:ngay_sinh,dia_chi:dia_chi,mat_khau:pass1},
                success: function(data){
                    if(data.n==1)
                    {
                        location.reload();
                    }
                    if(data.n==0)
                    {
                        alert("Tài khoản đã tồn tài, xin dùng tài khoản email khác");
                        document.getElementById("frm_email_dang_ky").value="";
                        document.getElementById("frm_email_dang_ky").focus(); 
                    }
                },
                error: function(xhr,status,error){
                    alert('Đăng ký không thành công');
                }
            });
            }
        });


        $("#frm_cap_nhat_submit").click(function(){
            var pass1=$("#frm_password_dang_ky").val();
            var pass2=$("#frm_nhap_lai_password").val();
            if(pass1!=pass2||pass1=="")
            { 
                alert('Password không trùng khớp');
              document.getElementById("frm_password").value=""; 
              document.getElementById("frm_nhap_lai_password").value=""; 
                document.getElementById("frm_nhap_lai_password").focus(); 
                return false;
            }
            else
            {
                var ho_ten=$("#frm_ho_ten").val();
                var email=$("#frm_email_dang_ky").val();
                
                if(email=="")
                {
                    alert('Chưa nhập email');
                    return false;
                }
                var gioi_tinh= $("input[name='frm_gioi_tinh']").val();
                var dien_thoai=$("#frm_dien_thoai").val();
                var ngay_sinh=$("#frm_ngay_sinh").val();
                var dia_chi=$("#frm_dia_chi").val();
                $.ajax({
                type: 'POST',
                dataType: 'json',
                url:'{{URL('khach_hang/cap_nhat')}}',
                data:{_token: '<?php echo csrf_token() ?>',gioi_tinh:gioi_tinh,ho_ten:ho_ten,email:email,dien_thoai:dien_thoai,ngay_sinh:ngay_sinh,dia_chi:dia_chi,mat_khau:pass1},
                success: function(data){
                    if(data.n==1)
                    {
                        location.reload();
                    }
                },
                error: function(xhr,status,error){
                    alert('Cập nhật không thành công');
                }
            });
            }
        });
        </script>
    @endsection
    <!-- / header section -->