@extends('master')
@section('content')
<section id="aa-catg-head-banner">
        <img src="public/source/img/banner_product.jpg" alt="fashion img">
        <div class="aa-catg-head-banner-area">
          <div class="container">
           <div class="aa-catg-head-banner-content">
             <ol class="breadcrumb">
               <li><a href="{{URL('/')}}">Trang chủ</a></li>         
               
               @if(isset($giam_gia))
               <li class="active">Sản phẩm giảm giá</li>
               @elseif(isset($tim_kiem))
               <li class="active">Sản phẩm tìm kiếm</li>
               @else
               <li class="active">Sản phẩm yêu thích</li>
               @endif
             
             </ol>
           </div>
          </div>
        </div>
       </section>
 <!-- product category -->
 <section id="aa-product-category">
        <div class="container">
          <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
              <div class="aa-product-catg-content">
                <div class="aa-product-catg-head">
                  
                  <div class="aa-product-catg-head-right">
                    <a id="grid-catg" href="#"><span class="fa fa-th"></span></a>
                    <a id="list-catg" href="#"><span class="fa fa-list"></span></a>
                  </div>
                </div>
                <div class="aa-product-catg-body">
                  <ul class="aa-product-catg">
                    @foreach ($dsSanPham as $sp)
                        <!-- start single product item -->
                      <li style="margin-left: 25px;">
                        <figure>
                          <a class="aa-product-img" href="#"><img style="width:100%;height:300px;" src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}" alt="{{$sp->ten_san_pham}}"></a>
                          <a class="aa-add-card-btn"href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><span class="fa fa-search"></span>Xem chi tiết</a>
                          <figcaption>
                            <h4 class="aa-product-title"><a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}">{{$sp->ten_san_pham}}</a></h4>
                            @if($sp->gia_sau_khi_giam!=0)
                            <span class="aa-product-price">{{number_format($sp->gia_sau_khi_giam)}}</span><span class="aa-product-price"><del>{{number_format($sp->don_gia)}}</del></span>
                            @else
                            <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                            @endif
                          <p class="aa-product-descrip">{{$sp->mo_ta}}</p>
                          </figcaption>
                        </figure>                         
                       
                        <!-- product badge -->
                        @if($sp->so_luong<=0)
                        <span class="aa-badge aa-sold-out" href="#">Hết hàng</span>
                        @elseif($sp->luot_thich>10)
                        <span class="aa-badge aa-hot" href="#">HOT</span>
                        @else
                        <span class="aa-badge aa-sale" href="#">Còn hàng</span>
                        @endif
                      </li>
                    @endforeach             
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9">
                    <aside class="aa-sidebar">
                      
                      @if(isset($dsXemGanDay))
                      <!-- single sidebar -->
                      <div class="aa-sidebar-widget">
                        <h3>Xem gần đây</h3>
                        <div class="aa-recently-views">
                          <ul>
                            @foreach ($dsXemGanDay as $sp)
                              <li>
                                <a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}" class="aa-cartbox-img"><img alt="img" src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}"></a>
                                  <div class="aa-cartbox-info">
                                    <h4><a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}">{{$sp->ten_san_pham}}</a></h4>
                                    <p>{{number_format($sp->don_gia)}}đ</p>
                                  </div>                    
                              </li>
                            @endforeach    
                          </ul>
                        </div>                            
                      </div>
                      @endif
                      <!-- single sidebar -->
                      <div class="aa-sidebar-widget">
                        <h3>Sản phẩm HOT</h3>
                        <div class="aa-recently-views">
                          <ul>
                            @foreach ($dsHot as $sp)
                            <li>
                              <a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}" class="aa-cartbox-img"><img alt="img" src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}"></a>
                                <div class="aa-cartbox-info">
                                  <h4><a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}">{{$sp->ten_san_pham}}</a></h4>
                                  <p>{{number_format($sp->don_gia)}}đ</p>
                                </div>                    
                            </li>
                            @endforeach                                 
                          </ul>
                        </div>                            
                      </div>
                    </aside>
                  </div>
                 
                </div>
              </div>
            </section>
@endsection