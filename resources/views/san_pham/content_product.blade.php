 <!-- product category -->
 <section id="aa-product-category">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
          <div class="aa-product-catg-content">
            <div class="aa-product-catg-head">
              <div class="aa-product-catg-head-left">
                <form method="POST" class="aa-sort-form">
                  @csrf
                  <select style="padding: 5px 0px;" name="frm_sort">
                    <option value="1" selected="Default">Giá thấp đến cao</option>
                    <option value="2">Giá cao đến thấp</option>
                  </select>
                  <button name="submit" style="padding: 2px 10px"> Sắp xếp</button>
                </form>

              </div>
              <div class="aa-product-catg-head-right">
                <a id="grid-catg" href="#"><span class="fa fa-th"></span></a>
                <a id="list-catg" href="#"><span class="fa fa-list"></span></a>
              </div>
            </div>
            <div class="aa-product-catg-body">
              <ul class="aa-product-catg">
                @foreach ($dsSanPham as $sp)
                    <!-- start single product item -->
                  <li style="margin-left: 25px;">
                    <figure>
                      <a class="aa-product-img" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><img style="width:100%;height:300px;" src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}" alt="{{$sp->ten_san_pham}}"></a>
                      <a class="aa-add-card-btn"href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><span class="fa fa-search"></span>Xem chi tiết</a>
                      <figcaption>
                        <h4 class="aa-product-title"><a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}">{{$sp->ten_san_pham}}</a></h4>
                        @if($sp->gia_sau_khi_giam!=0)
                        <span class="aa-product-price">{{number_format($sp->gia_sau_khi_giam)}}</span><span class="aa-product-price"><del>{{number_format($sp->don_gia)}}</del></span>
                        @else
                        <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                        @endif
                      <p class="aa-product-descrip">{{$sp->mo_ta_tom_tat}}</p>
                      </figcaption>
                    </figure>                         
                   
                    <!-- product badge -->
                    @if($sp->so_luong<=0)
                    <span class="aa-badge aa-sold-out" href="#">Hết hàng</span>
                    @elseif($sp->like>10)
                    <span class="aa-badge aa-hot" href="#">HOT</span>
                    @else
                    <span class="aa-badge aa-sale" href="#">Còn hàng</span>
                    @endif
                  </li>
                @endforeach             
              </ul>
            </div>
            <div class="aa-product-catg-pagination">
              {{$dsSanPham->links()}}
            </div>
          </div>
        </div>