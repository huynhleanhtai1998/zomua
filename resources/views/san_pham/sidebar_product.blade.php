<div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9">
          <aside class="aa-sidebar">
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Loại sản phẩm</h3>
              <ul class="aa-catg-nav">
                @foreach ($dsLoaiSanPham[$ma_loai] as $lsp)
              <li><a href="{{URL('san_pham/loai/'.$lsp->ma_loai)}}">{{$lsp->ten_loai}}</a></li>
                @endforeach
              </ul>
            </div>
           
            @if(isset($dsXemGanDay))
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Xem gần đây</h3>
              <div class="aa-recently-views">
                <ul>
                  @foreach ($dsXemGanDay as $sp)
                    <li>
                    <a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}" class="aa-cartbox-img"><img alt="img" src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}"></a>
                        <div class="aa-cartbox-info">
                          <h4><a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}">{{$sp->ten_san_pham}}</a></h4>
                          @if($sp->gia_sau_khi_giam!=0)
                          <span class="aa-product-price">{{number_format($sp->gia_sau_khi_giam)}}</span> <span class="aa-product-price"><del>{{number_format($sp->don_gia)}}</del></span>
                          @else
                          <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                          @endif
                        </div>                    
                    </li>
                  @endforeach    
                </ul>
              </div>                            
            </div>
            @endif
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Sản phẩm HOT</h3>
              <div class="aa-recently-views">
                <ul>
                  @foreach ($dsHot as $sp)
                  <li>
                    <a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}" class="aa-cartbox-img"><img alt="img" src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}"></a>
                      <div class="aa-cartbox-info">
                        <h4><a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}">{{$sp->ten_san_pham}}</a></h4>
                        @if($sp->gia_sau_khi_giam!=0)
                        <span class="aa-product-price">{{number_format($sp->gia_sau_khi_giam)}}</span><span class="aa-product-price"><del>{{number_format($sp->don_gia)}}</del></span>
                        @else
                        <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                        @endif
                      </div>                    
                  </li>
                  @endforeach                                 
                </ul>
              </div>                            
            </div>
          </aside>
        </div>
       
      </div>
    </div>
  </section>