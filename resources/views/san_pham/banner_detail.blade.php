<!-- catg header banner section -->
<section id="aa-catg-head-banner">
    <img src="public/source/img/banner_product.jpg" alt="fashion img">
    <div class="aa-catg-head-banner-area">
      <div class="container">
       <div class="aa-catg-head-banner-content">
         <ol class="breadcrumb">
           <li><a href="{{URL('/')}}">Trang chủ</a></li>         
           @foreach ($dsDoiTuong as $dt)
               @if($dt->id==$san_pham->doi_tuong)
               <li ><a href="{{URL('san_pham/'.$dt->id)}}">{{$dt->ten_doi_tuong}}</a></li>
               @endif
           @endforeach
           <li><a href="{{URL('san_pham/loai/'.$loai_san_pham->ma_loai)}}">{{$loai_san_pham->ten_loai}}</a></li>
           <li class="active">{{$san_pham->ten_san_pham}}</li>
         
         </ol>
       </div>
      </div>
    </div>
   </section>
  <!-- / catg header banner section -->