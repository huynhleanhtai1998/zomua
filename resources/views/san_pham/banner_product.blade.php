<!-- catg header banner section -->
<section id="aa-catg-head-banner">
   <img src="public/source/img/banner_product.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <ol class="breadcrumb">
          <li><a href="{{URL('/')}}">Trang chủ</a></li> 
          @if(count($dsSanPham)>0)        
            @foreach ($dsDoiTuong as $dt)
                @if($dt->id==$dsSanPham[0]->doi_tuong)
                <li @if(!isset($loai_san_pham))class="active" @endif><a href="{{URL('san_pham/'.$dt->id)}}">{{$dt->ten_doi_tuong}}</a></li>
                @endif
            @endforeach
            @if(isset($loai_san_pham))
            <li class="active">{{$loai_san_pham->ten_loai}}</li>
            @endif
          @endif
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->