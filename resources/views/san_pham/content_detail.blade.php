<!-- product category -->
<section id="aa-product-details">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aa-product-details-area">
                        <div class="aa-product-details-content">
                            <div class="row">
                                <!-- Modal view slider -->
                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <div class="aa-product-view-slider">
                                        <div id="demo-1" class="simpleLens-gallery-container">
                                            <div class="simpleLens-container">
                                                <div class="simpleLens-big-image-container">
                                                    <a class="simpleLens-lens-image"><img style="width: 250px; height:300px" src="public/source/img/san_pham/{{$san_pham->ma_loai}}/{{$san_pham->ma_san_pham}}/{{$san_pham->hinh}}" class="simpleLens-big-image"></a>
                                                </div>
                                            </div>
                                            <div class="simpleLens-thumbnails-container">
                                                @foreach ($dsHinh as $hinh)
                                                    <a data-big-image="public/source/img/san_pham/{{$san_pham->ma_loai}}/{{$san_pham->ma_san_pham}}/{{$hinh->hinh_anh}}" data-lens-image="public/source/img/san_pham/{{$san_pham->ma_loai}}/{{$san_pham->ma_san_pham}}/{{$hinh->hinh_anh}}" class="simpleLens-thumbnail-wrapper" href="#">
                                                        <img style="width: 50px; height:70px" src="public/source/img/san_pham/{{$san_pham->ma_loai}}/{{$san_pham->ma_san_pham}}/{{$hinh->hinh_anh}}">
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal view content -->
                                <div class="col-md-7 col-sm-7 col-xs-12">
                                    <div class="aa-product-view-content">
                                    <h3>{{$san_pham->ten_san_pham}}</h3>
                                        <div class="aa-price-block">
                                            @if($san_pham->gia_sau_khi_giam!=0)
                                            <span class="aa-product-price">{{number_format($san_pham->gia_sau_khi_giam)}}</span> <span class="aa-product-price"><del>{{number_format($san_pham->don_gia)}}</del></span>
                                            @else
                                            <span class="aa-product-price">{{number_format($san_pham->don_gia)}}đ</span>
                                            @endif
                                            <p class="aa-product-avilability">Tình trạng: <span>@if($san_pham->so_luong>0)Còn hàng @else Hết hàng @endif</span></p>
                                        </div>
                                    <p>{{$san_pham->mo_ta_tom_tat}}</p>
                                        <h4>Size</h4>
            
                                        <div class="aa-prod-view-size">
                                            <input name="size" type="radio" checked="cheched" value="S"/>S
                                            <input name="size" type="radio" style="margin-left:10px" value="M"/>M
                                            <input name="size" type="radio" style="margin-left:10px" value="L"/>L
                                            <input name="size" type="radio" style="margin-left:10px" value="XL"/>XL
                                        </div>
                                        {{-- <h4>Color</h4>
                                        <div class="aa-color-tag">
                                            <a href="#" class="aa-color-green"></a>
                                            <a href="#" class="aa-color-yellow"></a>
                                            <a href="#" class="aa-color-pink"></a>
                                            <a href="#" class="aa-color-black"></a>
                                            <a href="#" class="aa-color-white"></a>
                                        </div> --}}
                                        <div class="aa-prod-quantity">
                                            <h4>Số lượng</h4>
                                            <input id="so_luong" name="so_luong" type="number" value="1">
                                            <p class="aa-prod-category">
                                            Loại: <a href="{{URL('san_pham/loai/'.$san_pham->ma_loai)}}">{{$loai_san_pham->ten_loai}}</a>
                                            </p>
                                        </div>
                                        <div class="aa-prod-view-bottom">
                                        <button id="btnThemVaoGioHang" name="{{$san_pham->ma_san_pham}}">Thêm vào giỏ hàng</button>
                                        <br><br>
                                        @if(Session::has('khach_hang'))
                                            @if($like_status==false)
                                                <span id="thong_bao">Thích</span>
                                                <input id="ma_khach_hang" type="hidden" value="{{Session::get('khach_hang')->ma_khach_hang}}">
                                                <input id="like_status"  type="hidden" value="dislike">
                                                <button name="{{$san_pham->ma_san_pham}}" id="btn_like"><img id="img_like_status" src="public/source/img/like.png"></button>
                                            @else
                                                <span id="thong_bao">Bỏ thích</span>
                                                <input id="ma_khach_hang" type="hidden" value="{{Session::get('khach_hang')->ma_khach_hang}}">
                                                <input id="like_status"  type="hidden" value="like">
                                                <button name="{{$san_pham->ma_san_pham}}" id="btn_like"><img id="img_like_status" src="public/source/img/dislike.png"></button>
                                            @endif
                                        @else
                                        <a href="" data-toggle="modal" data-target="#login-modal">Đăng nhập để thích</a>
                                        @endif
                                        </div>
                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="aa-product-details-bottom">
                            <ul class="nav nav-tabs" id="myTab2">
                                <li><a href="#description" data-toggle="tab">Mô tả</a></li>
                                <li><a href="#review" data-toggle="tab">Đánh giá</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="description">
                                    <?php
                                        echo htmlspecialchars_decode($san_pham->mo_ta);
                                    ?>
                                </div>
                                <div class="tab-pane fade " id="review">
                                    <div class="aa-product-review-area">
                                    <h4>Đánh giá ({{count($dsDanhGia)}})</h4>
                                        <ul class="aa-review-nav">
                                            @foreach ($dsDanhGia as $danh_gia)
                                            <li>
                                                <div class="media">
                                                    <div class="media-body">
                                                    <h4 class="media-heading"><strong>{{$danh_gia->tac_gia}}</strong> - <span>{{$danh_gia->created_at}}</span></h4>
                                                        <div class="aa-product-rating">
                                                            @for($i=1;$i<=$danh_gia->diem;$i++)
                                                            <span class="fa fa-star"></span>
                                                            @endfor
                                                            @for($i=5;$i>$danh_gia->diem;$i--)
                                                            <span class="fa fa-star-o"></span>
                                                            @endfor
                                                        </div>
                                                        <p>{{$danh_gia->noi_dung}}</p>
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                        <h4>Đánh giá của bạn</h4>
                                    <form id="commentform" method="post" action="san_pham/danh_gia/{{$san_pham->ma_san_pham}}">
                                        @csrf
                                        <input type="hidden" name="ma_san_pham" value="{{$san_pham->ma_san_pham}}">
                                        <div class="star-rating">
                                            <fieldset>
                                              <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Outstanding">5 stars</label>
                                              <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Very Good">4 stars</label>
                                              <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Good">3 stars</label>
                                              <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Poor">2 stars</label>
                                              <input type="radio" id="star1" name="rating" value="1" checked="checked"/><label for="star1" title="Very Poor">1 star</label>
                                            </fieldset>
                                          </div>
                                        <!-- review form -->
                                        @if(Session::has('khach_hang'))
                                        
                                        <input name="ma_tin_tuc" type="hidden" value="{{$san_pham->id}}">
                                            <p class="comment-form-comment">
                                            <textarea id="danh_gia" name="danh_gia" cols="45" rows="8" aria-required="true" required="required"></textarea>
                                            </p>
                                            <p class="form-submit">
                                            <input id="submit_danh_gia" type="submit" name="submit" class="aa-browse-btn" value="Nộp đánh giá">
                                            </p>        
                                        </form>
                                        @else
                                        <a href="" data-toggle="modal" data-target="#login-modal">Đăng nhập để đăng đánh giá của bạn về sản phẩm</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Related product -->
                        <div class="aa-product-related-item">
                            <h3>Sản phẩm liên quan</h3>
                            <ul class="aa-product-catg aa-related-item-slider">
                                @foreach ($san_pham_lien_quan as $sp)
                                    <!-- start single product item -->
                                    <li>
                                        <figure>
                                        <a class="aa-product-img" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><img src="public/source/img/san_pham/{{$sp->ma_loai}}/{{$sp->ma_san_pham}}/{{$sp->hinh}}" style="width:100%; height:300px;" alt="{{$sp->ten_san_pham}}"></a>
                                            <a class="aa-add-card-btn" href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}"><span class="fa fa-search"></span>Xem chi tiết</a>
                                            <figcaption>
                                            <h4 class="aa-product-title"><a href="{{URL('san_pham/chi_tiet/'.$sp->ma_san_pham)}}">{{$sp->ten_san_pham}}</a></h4>
                                            @if($sp->gia_sau_khi_giam!=0)
                                            <span class="aa-product-price">{{number_format($sp->gia_sau_khi_giam)}}</span> <span class="aa-product-price"><del>{{number_format($sp->don_gia)}}</del></span>
                                            @else
                                            <span class="aa-product-price">{{number_format($sp->don_gia)}}đ</span>
                                            @endif
                                            </figcaption>
                                        </figure>
                                        
                                        <!-- product badge -->
                                        @if($sp->so_luong<=0)
                                        <span class="aa-badge aa-sold-out" href="#">Hết hàng</span>
                                        @elseif($sp->like>10)
                                        <span class="aa-badge aa-hot" href="#">HOT</span>
                                        @else
                                        <span class="aa-badge aa-sale" href="#">Còn hàng</span>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @section('script')
        @parent
        <script type="text/javascript">

        $("#btnThemVaoGioHang").click(function(){
            var ma_san_pham=$("#btnThemVaoGioHang").attr('name');
            var sl=$("#so_luong").val();
            var s= $("input[name='size']:checked").val();
            if(sl<=0)
            {
                alert('Số lượng không hợp lệ');
                return false;
            }
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url:'{{URL('gio_hang/them')}}/'+ma_san_pham,
                data:{_token: '<?php echo csrf_token() ?>',so_luong:sl,size:s},
                success: function(data){
                    if(data.n==0)
                        alert('Thêm thành công');
                    else    
                    location.reload();
                },
                error: function(xhr,status,error){
                    alert(error);
                }

            });
        });

        $("#btn_like").click(function(){
          var ma_san_pham=$("#btn_like").attr('name');
            var ma_khach_hang=$("#ma_khach_hang").val();
            var like_status=document.getElementById("like_status").value;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url:'{{URL('san_pham/like')}}',
                data:{_token: '<?php echo csrf_token() ?>',ma_san_pham:ma_san_pham,ma_khach_hang:ma_khach_hang,like_status:like_status},
                success: function(data){
                    if(data.n==0)
                    {
                      document.getElementById("img_like_status").src = "public/source/img/like.png";
                      document.getElementById("like_status").value = "dislike";
                      document.getElementById("thong_bao").innerHTML="Thích";
                      
                    }
                    else    
                    {
                      document.getElementById("img_like_status").src = "public/source/img/dislike.png";
                      document.getElementById("like_status").value = "like";
                      document.getElementById("thong_bao").innerHTML="Đã thích";

                    }    
                },
                error: function(xhr,status,error){
                    alert(error);
                }

            });
        });
        </script>
    @endsection
    <!-- / product category -->