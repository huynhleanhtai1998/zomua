<?php
return [
    'home'=>'Trang chủ',
    'product'=>'Sản phẩm',
    'shop'=>'Cửa hàng',
    'sale'=>'Khuyến mãi',
    'news'=>'Tin thời trang',
    'login'=>'Đăng nhập',
    'logout'=>'Đăng xuất',
    'Nam'=>'Nam',
    'Nữ'=>'Nữ',
    'Trẻ em'=>'Trẻ em',
    'Thể thao'=>'Thể thao'
];
?>