<?php
return [
    'home'=>'Home',
    'product'=>'Product',
    'shop'=>'Shop',
    'sale'=>'Sale',
    'news'=>'News',
    'login'=>'Login',
    'logout'=>'Logout',
    'Nam'=>'Men',
    'Nữ'=>'Women',
    'Trẻ em'=>'Kid',
    'Thể thao'=>'Sport'
];
?>