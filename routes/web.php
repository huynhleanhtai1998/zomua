<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>'locale'],function(){
	Route::get('change_language/{language}','HomeController@ChangeLanguage');

	Route::get('/', 'SanPhamController@index');
	Route::get('/lien_he', 'LienHeController@index');

	Route::get('hoa_don/xoa/{id}', 'GioHangController@xoa_don_hang');
	Route::get('/quan_tri', 'QuanTriController@index');

	Route::get('/admin', 'QuanTriController@dang_nhap');
	Route::post('/admin', 'QuanTriController@xu_ly_dang_nhap');

	
	
	Route::prefix('doi_tuong')->group(function () {
		Route::get('/them', 'DoiTuongController@them');
		Route::get('/liet_ke', 'DoiTuongController@liet_ke');
		Route::post('/them', 'DoiTuongController@store');
		Route::get('/sua/{id}', 'DoiTuongController@sua');
		Route::get('/xoa/{id}', 'DoiTuongController@xoa');
		Route::post('/sua/{id}', 'DoiTuongController@edit');
	});

	
	Route::prefix('loai_san_pham')->group(function () {
		Route::get('/them', 'LoaiSanPhamController@them');
		Route::get('/liet_ke', 'LoaiSanPhamController@liet_ke');
		Route::post('/them', 'LoaiSanPhamController@store');
		Route::get('/xoa/{id}', 'LoaiSanPhamController@xoa');
		Route::get('/sua/{id}', 'LoaiSanPhamController@sua');
		Route::post('/sua/{id}', 'LoaiSanPhamController@edit');
	});

	

	Route::prefix('loai_tin_tuc')->group(function () {
		Route::get('/them', 'LoaiTinTucController@them');
		Route::get('/liet_ke', 'LoaiTinTucController@liet_ke');
		Route::post('/them', 'LoaiTinTucController@store');
		Route::get('/sua/{id}', 'LoaiTinTucController@sua');
		Route::get('/xoa/{id}', 'LoaiTinTucController@xoa');
		Route::post('/sua/{id}', 'LoaiTinTucController@edit');
	});

	Route::prefix('quan_tri')->group(function () {
		Route::get('/', 'QuanTriController@dang_nhap');
		Route::post('/', 'QuanTriController@xu_ly_dang_nhap');
		Route::get('/index', 'QuanTriController@index');
		Route::get('/dang_xuat', 'QuanTriController@dang_xuat');
		Route::get('/liet_ke_hoa_don_chua_giao', 'GioHangController@liet_ke_hoa_don_chua_giao');
		Route::get('/liet_ke_hoa_don_dang_giao', 'GioHangController@liet_ke_hoa_don_dang_giao');
		Route::get('/liet_ke_hoa_don_da_giao', 'GioHangController@liet_ke_hoa_don_da_giao');
		Route::get('/liet_ke_hoa_don_chua_chuyen_khoan', 'GioHangController@liet_ke_hoa_don_chua_chuyen_khoan');
		Route::get('/thong_ke_san_pham', 'QuanTriController@thong_ke_san_pham');
		Route::get('/thong_ke_tin_tuc', 'QuanTriController@thong_ke_tin_tuc');
		Route::get('/liet_ke_user', 'QuanTriController@liet_ke_user');
		Route::get('/them_user', 'QuanTriController@them_user');
		Route::post('/them_user', 'QuanTriController@luu_user');
		Route::get('/user/xoa/{id}', 'QuanTriController@xoa_user');
		Route::get('/user/sua/{id}', 'QuanTriController@sua_user');
		Route::post('/user/sua/{id}', 'QuanTriController@luu_sua_user');
		
	});

	Route::prefix('san_pham')->group(function () {
		Route::get('/', 'SanPhamController@index');
		Route::post('/like', 'SanPhamController@like');
		Route::get('/giam_gia', 'SanPhamController@san_pham_giam_gia');
		Route::post('/danh_gia/{id}', 'SanPhamController@danh_gia');
		Route::get('/giam_gia/{id}', 'SanPhamController@giam_gia');
		Route::post('/giam_gia/{id}', 'SanPhamController@store_giam_gia');
		Route::get('/loai/{id}', 'SanPhamController@loai_san_pham');
		Route::post('/loai/{id}', 'SanPhamController@loai_san_pham_sort');
		Route::get('/chi_tiet/{id}', 'SanPhamController@show');
		Route::get('/sua/{id}', 'SanPhamController@sua');
		Route::post('/sua/{id}', 'SanPhamController@edit');
		Route::get('/them', 'SanPhamController@create');
		Route::post('/them', 'SanPhamController@store');
		Route::get('/tim_kiem', 'SanPhamController@tim_kiem');
		Route::get('/xoa/{id}', 'SanPhamController@xoa');
		Route::get('/thong_ke_so_luong_san_pham_theo_loai', 'SanPhamController@thong_ke_so_luong_san_pham_theo_loai');
		Route::get('/liet_ke', 'SanPhamController@liet_ke');
		Route::get('/liet_ke_het_hang', 'SanPhamController@liet_ke_het_hang');
		Route::get('/thong_ke_doanh_thu', 'SanPhamController@thong_ke_doanh_thu');
		Route::get('/thong_ke_san_pham_ban_chay', 'SanPhamController@thong_ke_san_pham_ban_chay');
		Route::get('/thong_ke_san_pham_yeu_thich', 'SanPhamController@thong_ke_san_pham_yeu_thich');
		Route::get('/{id}', 'SanPhamController@doi_tuong');
		Route::post('/{id}', 'SanPhamController@doi_tuong_sort');
	});

	Route::prefix('tin_tuc')->group(function () {
		Route::get('/', 'TinTucController@index');
		Route::post('/comment', 'TinTucController@comment');
		Route::get('/moi', 'TinTucController@moi');
		Route::get('/loai/{id}', 'TinTucController@loai_tin_tuc');
		Route::get('/tac_gia/{id}', 'TinTucController@tim_kiem_tac_gia');
		Route::post('/like', 'TinTucController@like');
		Route::get('/chi_tiet/{id}', 'TinTucController@show');
		Route::get('/sua/{id}', 'TinTucController@sua');
		Route::get('/xoa/{id}', 'TinTucController@xoa');
		Route::post('/sua/{id}', 'TinTucController@edit');
		Route::get('/them', 'TinTucController@create');
		Route::get('/liet_ke', 'TinTucController@liet_ke');
		Route::get('/thong_ke_tin_tuc_xem_nhieu_nhat_thang', 'TinTucController@thong_ke_tin_tuc_xem_nhieu_nhat_thang');
		Route::get('/thong_ke_tin_tuc_yeu_thich', 'TinTucController@thong_ke_tin_tuc_yeu_thich');
		Route::get('/thong_ke_tin_tuc_nhieu_binh_luan', 'TinTucController@thong_ke_tin_tuc_nhieu_binh_luan');
		Route::post('/them', 'TinTucController@store');
		Route::get('/xoa', 'TinTucController@destroy');
	});


	Route::prefix('gio_hang')->group(function () {
		Route::get('/', 'GioHangController@index');
		Route::put('/cap_nhat', 'GioHangController@cap_nhat');
		Route::post('/them/{id}', 'GioHangController@them');
		Route::get('/xoa/{id}', 'GioHangController@xoa');
		Route::post('/dang_nhap', 'GioHangController@dang_nhap');
		


		Route::get('/thanh_toan', 'GioHangController@thanh_toan');
		Route::post('/luu_hoa_don', 'GioHangController@store');
		Route::get('/moi', 'TinTucController@moi');
		Route::get('/chi_tiet/{id}', 'SanPhamController@show');
		Route::get('/xoa', 'TinTucController@destroy');
		Route::get('/{id}', 'SanPhamController@loai_san_pham');
	});


	Route::prefix('khach_hang')->group(function () {
		Route::post('/dang_nhap', 'KhachHangController@xu_ly_dang_nhap');
		Route::post('/dang_ky', 'KhachHangController@xu_ly_dang_ky');
		Route::post('/danh_gia', 'KhachHangController@danh_gia');
		Route::get('/liet_ke', 'KhachHangController@liet_ke');
		Route::get('/cap_nhat', 'KhachHangController@cap_nhat');
		Route::get('/san_pham_yeu_thich', 'SanPhamController@san_pham_yeu_thich');
		Route::get('/tin_tuc_yeu_thich', 'TinTucController@tin_tuc_yeu_thich');
		Route::get('/tong_hop_hoa_don', 'GioHangController@tong_hop_hoa_don');
		Route::get('/hoa_don/{id}', 'GioHangController@chi_tiet_hoa_don');
		Route::post('/cap_nhat', 'KhachHangController@cap_nhat');
		Route::get('/dang_xuat', 'KhachHangController@dang_xuat');
		Route::get('/them', 'TinTucController@create');
		Route::post('/them', 'TinTucController@store');
		Route::get('/xoa', 'TinTucController@destroy');
	});
});